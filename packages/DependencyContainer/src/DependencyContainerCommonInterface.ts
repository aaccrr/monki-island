import { DispatcherInterface, TransporterInterface } from '@monki-island/transport';
import { Observer } from '@monki-island/observability';

export interface DependencyContainerCommonInterface {
  init?(): Promise<void>;

  getTransporter(): TransporterInterface;
  getDispatcher(): DispatcherInterface;
  getObserver(): Observer.Observer;
}
