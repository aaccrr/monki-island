export interface ConnectorInterface {
  init(): Promise<void>;
  getConnection(): any;
}
