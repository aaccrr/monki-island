import { Nullable, WithPagination } from '@monki-island/common';
import { StateCommandInterface } from './CommandInterface';
import { StateQueryInterface } from './QueryInterface';

export interface StateRepositoryInterface {
  exists(query: StateQueryInterface): Promise<boolean>;
  find<T extends any>(query: StateQueryInterface): Promise<Nullable<T>>;
  findAndUpdate<T extends any>(command: StateCommandInterface): Promise<Nullable<T>>;
  findAndDelete<T extends any>(command: StateQueryInterface): Promise<Nullable<T>>;
  create<T extends any>(command: StateCommandInterface): Promise<T>;
  createOrUpdate<T extends {} = {}>(command: StateCommandInterface): Promise<T>;
  createMany<T extends any>(command: StateCommandInterface): Promise<T[]>;
  update<T extends any>(command: StateCommandInterface): Promise<Partial<T>>;
  updateMany(command: StateCommandInterface[]): Promise<void>;
  updateBatch<T extends any>(command: StateCommandInterface): Promise<Partial<T>[]>;
  purge(command: StateCommandInterface): Promise<void>;
  delete(command: StateCommandInterface): Promise<void>;
  filter<T extends any>(query?: StateQueryInterface): Promise<T[]>;
  filterPaginate<T extends any>(query?: StateQueryInterface): Promise<WithPagination<T[]>>;
  aggregate<T extends any>(query?: StateQueryInterface): Promise<T[]>;
}
