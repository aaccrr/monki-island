export interface StateQueryInterface {
  select(): any;
  option?(): any;
  meta?(): any;
  postProcessor?(rawModel: any): any;
}
