export interface StateCommandInterface {
  select?(): any;
  insert?(): any;
  update?(): any;
  meta?(): any;
}
