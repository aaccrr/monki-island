import { MongoIndex } from './MongoIndex';

export interface MongoConfig {
  host: string;
  username: string;
  password: string;
  indexes: MongoIndex[];
}
