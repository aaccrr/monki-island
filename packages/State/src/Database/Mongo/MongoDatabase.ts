import { MongoClient } from 'mongodb';
import { MongoConfig } from './MongoConfig';
import { MongoInterface } from './MongoInterface';

export class MongoDatabase implements MongoInterface {
  private client: MongoClient;

  async init(config: MongoConfig): Promise<void> {
    this.client = await MongoClient.connect(`mongodb://${config.username}:${config.password}@${config.host}?authSource=admin`);

    for (const index of config.indexes) {
      await this.client.db(index.database).collection(index.collection).createIndex(index.specification, index.options);
    }
  }

  getClient(): MongoClient {
    return this.client;
  }
}
