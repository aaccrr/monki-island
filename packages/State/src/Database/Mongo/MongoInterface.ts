import { MongoClient } from 'mongodb';
import { MongoConfig } from './MongoConfig';

export interface MongoInterface {
  init(config: MongoConfig): Promise<void>;
  getClient(): MongoClient;
}
