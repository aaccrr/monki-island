import { IndexSpecification, CreateIndexesOptions } from 'mongodb';

export interface MongoIndex {
  database: string;
  collection: string;
  specification: IndexSpecification;
  options: CreateIndexesOptions;
}
