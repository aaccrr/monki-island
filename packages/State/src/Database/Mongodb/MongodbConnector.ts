import { Collection, CreateCollectionOptions, IndexSpecification, MongoClient } from 'mongodb';
import { ConnectorInterface } from '../../Interface';
import { MongodbConnectorConfiguration } from './Interface';

export class MongodbConnector implements ConnectorInterface {
  constructor(private config: MongodbConnectorConfiguration) {}

  private connection: Collection;

  async init(): Promise<void> {
    const client = await MongoClient.connect(`mongodb://${this.config.username}:${this.config.password}@${this.config.host}?authSource=admin`);

    const createCollectionOptions: Partial<CreateCollectionOptions> = {};

    if (this.config.timeseries) {
      createCollectionOptions.timeseries = {
        timeField: 'timestamp',
        metadata: 'metadata',
      };
    }

    const existingCollections = await client.db(this.config.database).listCollections().toArray();

    const isCollectionExists = existingCollections.find((collection) => collection.name === this.config.collection) !== undefined;

    if (isCollectionExists) {
      this.connection = client.db(this.config.database).collection(this.config.collection);
      return;
    }

    this.connection = await client.db(this.config.database).createCollection(this.config.collection, createCollectionOptions);

    if (Array.isArray(this.config.index)) {
      for (const index of this.config.index) {
        await this.connection.createIndex({ [index]: 1 });
      }
    }
  }

  getConnection(): Collection {
    return this.connection;
  }
}
