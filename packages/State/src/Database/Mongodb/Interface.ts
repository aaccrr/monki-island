export interface MongodbConnectorConfiguration {
  host: string;
  username: string;
  password: string;
  database: string;
  collection: string;
  timeseries?: boolean;
  index?: string[];
}
