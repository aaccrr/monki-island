import { Collection, Filter } from 'mongodb';
import { Nullable, WithPagination } from '@monki-island/common';
import { ConnectorInterface, StateCommandInterface, StateQueryInterface, StateRepositoryInterface } from '../..';

export class MongodbRepository implements StateRepositoryInterface {
  constructor(private connector: ConnectorInterface) {}

  private connection: Collection;

  async init(): Promise<void> {
    await this.connector.init();
    this.connection = this.connector.getConnection();
  }

  async exists(query: StateQueryInterface): Promise<boolean> {
    const model = await this.connection.findOne(query.select() as Filter<any>);

    if (!model) {
      return false;
    }

    return true;
  }

  async find<T extends any>(query: StateQueryInterface): Promise<Nullable<T>> {
    const model = await this.connection.findOne(query.select() as Filter<T>);

    if (!model) {
      return null;
    }

    return model as T;
  }

  async findAndUpdate<T extends any>(command: StateCommandInterface): Promise<Nullable<T>> {
    const updatedModel = await this.connection.findOneAndUpdate(command.select() as Filter<T>, command.insert(), { returnDocument: 'after' });
    return updatedModel.value as T;
  }

  async findAndDelete<T extends any>(command: StateQueryInterface): Promise<Nullable<T>> {
    return null;
  }

  async create<T extends any>(command: StateCommandInterface): Promise<T> {
    try {
      const insertion = command.insert();
      await this.connection.insertOne(insertion);
      return insertion;
    } catch (e) {}
  }

  async createOrUpdate<T extends {} = {}>(command: StateCommandInterface): Promise<T> {
    const model = await this.connection.findOne(command.select() as Filter<T>);

    if (model) {
      await this.connection.updateOne(command.select(), command.update());
      return;
    }

    await this.connection.insertOne(command.insert());
  }

  async createMany<T extends any>(command: StateCommandInterface): Promise<T[]> {
    try {
      const insertion = command.insert();
      await this.connection.insertMany(insertion, command.meta());
      return insertion;
    } catch (e) {}
  }

  async update<T extends any>(command: StateCommandInterface): Promise<Partial<T>> {
    const update = command.insert();
    await this.connection.updateOne(command.select(), update);
    return update;
  }

  async updateMany(command: StateCommandInterface[]): Promise<void> {
    for (const com of command) {
      await this.connection.updateOne(com.select(), com.insert());
    }
  }

  async updateBatch<T extends any>(command: StateCommandInterface): Promise<Partial<T>[]> {
    const insertion = command.insert();
    await this.connection.updateMany(command.select(), insertion);
    return insertion;
  }

  async purge(command: StateCommandInterface): Promise<void> {
    await this.connection.deleteMany(command.select());
  }

  async delete(command: StateCommandInterface): Promise<void> {
    await this.connection.deleteOne(command.select());
  }

  filter<T extends any>(query?: StateQueryInterface): Promise<T[]> {
    const options = typeof query.option === 'function' ? query.option() : {};
    return this.connection.find<T>(query.select(), options).toArray();
  }

  async filterPaginate<T extends any>(query?: StateQueryInterface): Promise<WithPagination<T[]>> {
    return {
      offset: 0,
      total: 0,
      items: [],
    };
  }

  async aggregate<T extends any>(query?: StateQueryInterface): Promise<T[]> {
    const aggregation = await this.connection.aggregate(query.select()).toArray();
    return aggregation as T[];
  }
}
