export interface DatabaseInterface {
  getClient<T>(): T;
}
