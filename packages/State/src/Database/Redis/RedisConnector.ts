import { createNodeRedisClient, WrappedNodeRedisClient } from 'handy-redis';
import { ConnectorInterface } from '../../Interface';
import { RedisConnectorConfiguration } from './Interface';

export class RedisConnector implements ConnectorInterface {
  constructor(private config: RedisConnectorConfiguration) {}

  private connection: WrappedNodeRedisClient;

  async init(): Promise<void> {
    const [host, port] = this.config.host.split(':');

    this.connection = createNodeRedisClient({
      host,
      port: parseInt(port),
      password: this.config.password,
    });
  }

  getConnection(): WrappedNodeRedisClient {
    return this.connection;
  }
}
