import { Nullable, WithPagination } from '@monki-island/common';
import { WrappedNodeRedisClient } from 'handy-redis';
import { ConnectorInterface, StateCommandInterface, StateQueryInterface, StateRepositoryInterface } from '../../Interface';
import { RedisActionMeta } from './Interface';

const objectToTuple = (object: object): Array<[field: string, value: string]> => Object.keys(object).map((key) => [key, object[key]]);

export class RedisRepository implements StateRepositoryInterface {
  constructor(private connector: ConnectorInterface) {}

  private connection: WrappedNodeRedisClient;

  async init(): Promise<void> {
    await this.connector.init();
    this.connection = this.connector.getConnection();
  }

  async exists(query: StateQueryInterface): Promise<boolean> {
    return false;
  }

  async find<T extends any>(query: StateQueryInterface): Promise<Nullable<T>> {
    const meta = query.meta() as RedisActionMeta;
    const key = query.select() as string;

    // hgetall
    const model = await this.connection[meta.method](key);

    if (!model) {
      return null;
    }

    if (typeof query.postProcessor === 'function') {
      return query.postProcessor(model);
    }

    return model as T;
  }

  async findAndUpdate<T extends any>(command: StateCommandInterface): Promise<Nullable<T>> {
    return null;
  }

  async findAndDelete<T extends any>(command: StateQueryInterface): Promise<Nullable<T>> {
    return null;
  }

  async create<T extends any>(command: StateCommandInterface): Promise<T> {
    const meta = command.meta() as RedisActionMeta;
    const key = command.select() as string;
    const insertion = command.insert();

    // hset
    await this.connection[meta.method](key, ...objectToTuple(insertion));

    return insertion;
  }

  async createOrUpdate<T extends {} = {}>(command: StateCommandInterface): Promise<T> {
    return {} as any;
  }

  async createMany<T extends any>(command: StateCommandInterface): Promise<T[]> {
    return [];
  }

  async update<T extends any>(command: StateCommandInterface): Promise<Partial<T>> {
    const meta = command.meta() as RedisActionMeta;
    const key = command.select() as string;
    const insertion = command.insert();

    await this.connection[meta.method](key, ...objectToTuple(insertion));

    return insertion;
  }

  async updateMany(command: StateCommandInterface[]): Promise<void> {}

  async updateBatch<T extends any>(command: StateCommandInterface): Promise<Partial<T>[]> {
    return [];
  }

  async purge(command: StateCommandInterface): Promise<void> {
    return;
  }

  async delete(command: StateCommandInterface): Promise<void> {
    return;
  }

  async filter<T extends any>(query?: StateQueryInterface): Promise<T[]> {
    return [];
  }

  async filterPaginate<T extends any>(query?: StateQueryInterface): Promise<WithPagination<T[]>> {
    return {
      offset: 0,
      total: 0,
      items: [],
    };
  }

  async aggregate<T extends any>(query?: StateQueryInterface): Promise<T[]> {
    return [] as any;
  }
}
