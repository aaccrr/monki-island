export interface RedisConnectorConfiguration {
  host: string;
  password: string;
}

export interface RedisActionMeta {
  method: 'hgetall' | 'hset';
}
