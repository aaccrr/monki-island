import * as Elasticseach from '@elastic/elasticsearch';
import { ElasticsearchDatabaseInterface } from './ElasticsearchDatabaseInterface';
import { ElasticsearchConfiguration } from './ElasticsearchConfiguration';

export class ElasticsearchDatabase implements ElasticsearchDatabaseInterface {
  private client: Elasticseach.Client;

  async init(configuration: ElasticsearchConfiguration): Promise<void> {
    this.client = new Elasticseach.Client({
      node: configuration.host,
      auth: {
        username: configuration.username,
        password: configuration.password,
      },
    });
  }

  getClient(): Elasticseach.Client {
    return this.client;
  }
}
