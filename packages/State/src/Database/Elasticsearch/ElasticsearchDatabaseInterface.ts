import * as Elasticseach from '@elastic/elasticsearch';
import { ElasticsearchConfiguration } from './ElasticsearchConfiguration';

export interface ElasticsearchDatabaseInterface {
  init(configuration: ElasticsearchConfiguration): Promise<void>;
  getClient(): Elasticseach.Client;
}
