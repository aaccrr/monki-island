export interface ElasticsearchConfiguration {
  host: string;
  username: string;
  password: string;
}
