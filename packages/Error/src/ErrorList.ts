interface IError {
  [code: string]: IErrorStruct;
}

interface IErrorStruct {
  httpCode: number;
  message: IErrorMessageStruct;
}

interface IErrorMessageStruct {
  [key: string]: (params?: any) => string;
}

export const errors: IError = {
  // --- commons
  INTERNAL_ERROR: {
    httpCode: 500,
    message: {
      ru: () => 'Ошибка на сервере.',
    },
  },
  RPC_SERVER_UNAVAILABLE: {
    httpCode: 500,
    message: {
      ru: () => 'Ну удалось подключиться к серверу по рписи.',
    },
  },
  UNAUTHORIZED: {
    httpCode: 401,
    message: {
      ru: () => 'Необходима авторизация.',
    },
  },

  INVALID_SECRET_CODE: {
    httpCode: 403,
    message: {
      ru: () => 'Недействительный API токен.',
    },
  },

  // --- refunds
  PURCHASE_FOR_REFUND_IS_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => '<p>Билет или заказ не найден.<br />Введите корректный номер билета или заказа.</p>',
    },
  },
  PURCHASE_FOR_REFUND_IS_ALREADY_PASSED: {
    httpCode: 400,
    message: {
      ru: () => 'Билет уже погашен контролером.',
    },
  },
  PURCHASE_FOR_REFUND_IS_NOT_REFUNDABLE: {
    httpCode: 400,
    message: {
      ru: () => 'Не подлежит возврату (по условиям договора оферты).',
    },
  },
  PURCHASE_FOR_REFUND_ALREADY_IN_REFUND_PROCESS: {
    httpCode: 409,
    message: {
      ru: () => 'На билет или заказ уже был оформлен возврат.',
    },
  },
  REFUND_INITIATOR_AND_PURCHASE_CUSTOMER_ARE_NOT_THE_SAME: {
    httpCode: 400,
    message: {
      ru: () => 'Данные не совпадают с указанными при покупке билета',
    },
  },
  CLIENT_LOGIN_ATTEMPTS_EXCEEDED: {
    httpCode: 429,
    message: {
      ru: ({ timeout = 30 }) => `<p>Вы исчерпали количество попыток ввода данных. <span>Попробуйте войти через ${timeout} минут.</span></p>`,
    },
  },

  REFUND_AVAILABILITY_CHECK_ATTEMPTS_EXCEEDED: {
    httpCode: 429,
    message: {
      ru: ({ timeout = 30 }) => `Вы исчерпали количество попыток ввода билетов, попробуйте через ${timeout} минут.`,
    },
  },
  REFUND_INITIATOR_PROFILE_CHECK_ATTEMPTS_EXCEEDED: {
    httpCode: 429,
    message: {
      ru: ({ timeout = 30 }) => `Вы исчерпали количество попыток возврата покупки, попробуйте через ${timeout} минут.`,
    },
  },
  REFUND_CREATION_ATTEMPTS_EXCEEDED: {
    httpCode: 429,
    message: {
      ru: ({ timeout = 30 }) => `Вы исчерпали количество попыток возврата покупки, попробуйте через ${timeout} минут.`,
    },
  },

  // --- account
  ACCOUNT_TO_CHANGE_PASSWORD_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт для изменения пароля не найден.',
    },
  },
  ACCOUNT_TO_SET_NEW_PASSWORD_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт для установки пароля не найден.',
    },
  },
  CONFIRMATION_CODE_IS_INVALID: {
    httpCode: 400,
    message: {
      ru: () => 'Код подтверждения недействителен.',
    },
  },
  ACCOUNT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт не найден.',
    },
  },
  CONFIRMED_ACCOUNT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Подтверждаемый аккаунт не найден.',
    },
  },
  WRONG_LOGIN_OR_PASSWORD: {
    httpCode: 401,
    message: {
      ru: () => 'Неверный логин или пароль.',
    },
  },
  VIEWING_CLIENT_ACCOUNT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт не найден.',
    },
  },
  ACCOUNT_TO_UPDATE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт не найден.',
    },
  },
  ACCOUNT_CAN_BE_UPDATED_ONCE_A_DAY: {
    httpCode: 400,
    message: {
      ru: () => 'Обновление аккаунта возможно только один раз в день.',
    },
  },

  // --- ordering
  MUSEUM_TO_WHICH_TICKET_PURCHASED_NOT_FOUND: {
    httpCode: 400,
    message: {
      ru: () => 'Музей в который покупается билет не найден.',
    },
  },
  PURCHASED_TICKET_NOT_FOUND: {
    httpCode: 400,
    message: {
      ru: () => 'Покупаемый билет не найден.',
    },
  },
  PURCHASED_ABONEMENT_NOT_FOUND: {
    httpCode: 400,
    message: {
      ru: () => 'Покупаемый абонемент не найден.',
    },
  },
  PURCHASED_EVENT_NOT_FOUND: {
    httpCode: 400,
    message: {
      ru: () => 'Покупаемое мероприятие не найдено.',
    },
  },
  SELECTED_VISIT_DATE_IS_NOT_VALID: {
    httpCode: 400,
    message: {
      ru: () => 'Выбрана некорректная дата посещения.',
    },
  },
  SELECTED_VISIT_INTERVAL_IS_NOT_VALID: {
    httpCode: 400,
    message: {
      ru: () => 'Выбран некорректный интервал посещения.',
    },
  },
  COUNT_BY_CLIENT_LIMIT_EXCEEDED: {
    httpCode: 400,
    message: {
      ru: () => 'Превышен лимит количества в одни руки.',
    },
  },
  UNABLE_TO_RESERVE_PURCHASE: {
    httpCode: 400,
    message: {
      ru: () => 'Не удалось зарезервировать купленные билеты.',
    },
  },
  PAYMENT_SYSTEM_CONNECT_ERROR: {
    httpCode: 500,
    message: {
      ru: () => 'Ошибка при обращении к платежной системе.',
    },
  },
  SELECTED_CLIENT_CATEGORY_IS_INVALID: {
    httpCode: 400,
    message: {
      ru: () => 'Выбрана несуществуюшая категория клиента.',
    },
  },
  OWN_ORDER_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Заказ не найден.',
    },
  },
  SELECTED_BY_CLIENT_MUSEUM_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Музей не найден.',
    },
  },
  MUSEUM_ALREADY_ADDED_TO_FAVORITES: {
    httpCode: 409,
    message: {
      ru: () => 'Музей уже был добавлен в избранное.',
    },
  },
  MUSEUM_NOT_IN_FAVORITES_YET: {
    httpCode: 400,
    message: {
      ru: () => 'Музей пока не был добавлен в избранное.',
    },
  },
  ADDED_TO_FAVORITES_MUSEUM_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Музей не найден.',
    },
  },
  REMOVED_FROM_FAVORITES_MUSEUM_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Музей не найден.',
    },
  },
  SELLING_TICKET_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Билет не найден.',
    },
  },
  SELLING_ABONEMENT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Абонемент не найден.',
    },
  },
  SELLING_EVENT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Мероприятие не найдено.',
    },
  },
  ADDED_TO_FAVORITES_EVENT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Мероприятие не найдено.',
    },
  },
  REMOVED_FROM_FAVORITES_EVENT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Мероприятие не найдено.',
    },
  },
  EVENT_ALREADY_ADDED_TO_FAVORITES: {
    httpCode: 409,
    message: {
      ru: () => 'Мероприятие уже было добавлено в избранное.',
    },
  },
  EVENT_NOT_IN_FAVORITES_YET: {
    httpCode: 400,
    message: {
      ru: () => 'Мероприятие пока не было добавлено в избранное.',
    },
  },

  // --- nomenclature
  MODERATING_ENTRANCE_TICKET_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Билет не найден.',
    },
  },
  ENTRANCE_TICKET_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Билет не найден.',
    },
  },
  ORGANIZATION_REQUIRED: {
    httpCode: 403,
    message: {
      ru: () => 'Необходимо зарегистрировать организацию.',
    },
  },
  ORGANIZATION_NOT_CONFIRMED: {
    httpCode: 403,
    message: {
      ru: () => 'Необходимо подтвердить организацию.',
    },
  },
  AT_LEAST_ONE_ACCOUNTABLE_MUSEUM_REQUIRED: {
    httpCode: 403,
    message: {
      ru: () => 'Необходимо привязать к организации музей.',
    },
  },
  CONTROLLER_IN_MUSEUM_REQUIRED: {
    httpCode: 403,
    message: {
      ru: () => 'К музею должен быть прикреплен контроллер.',
    },
  },
  PRIMARY_MANAGER_ACCOUNT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт главного администратора не найден.',
    },
  },
  EMPLOYING_ORGANZATION_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Организация, создающая контроллера не найдена.',
    },
  },
  DELETED_EMPLOYEE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Сотрудник не найден.',
    },
  },
  CANT_DELETE_LAST_CONTROLLER: {
    httpCode: 400,
    message: {
      ru: () => 'Невозможно удалить единственного контроллера.',
    },
  },
  CANT_DELETE_LAST_GUIDE: {
    httpCode: 400,
    message: {
      ru: () => 'Невозможно удалить единственного экскурсовода.',
    },
  },
  UPDATED_EMPLOYEE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Сотрудник не найден.',
    },
  },
  VIEWED_EMPLOYEE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Сотрудник не найден.',
    },
  },
  INVALID_REFRESH_TOKEN: {
    httpCode: 401,
    message: {
      ru: () => 'Передан испорченный токен обновления сессиии.',
    },
  },
  OWN_ACCOUNT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт не найден.',
    },
  },
  ONLY_PRIMARY_MANAGER_CAN_ADD_ACCOUNT: {
    httpCode: 403,
    message: {
      ru: () => 'Для добавления пользователя нужны права доступа главного администратора.',
    },
  },
  ONLY_PRIMARY_MANAGER_CAN_DELETE_ACCOUNT: {
    httpCode: 403,
    message: {
      ru: () => 'Для удаления пользователя нужны права доступа главного администратора.',
    },
  },
  ONLY_MANAGER_CAN_BE_GRANTED_PRIMARY_ACCESS_LEVEL: {
    httpCode: 403,
    message: {
      ru: () => 'Только администратору можно передать права главного администратора.',
    },
  },
  SUBORDINATE_ACCOUNT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт не найден.',
    },
  },
  CAN_NOT_DELETE_PRIMARY_MANAGER_ACCOUNT: {
    httpCode: 403,
    message: {
      ru: () => 'Аккаунт главного администратора невозможно удалить.',
    },
  },
  ADMIN_ACCOUNT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Аккаунт не найден.',
    },
  },
  RESENDED_CODE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Код не найден.',
    },
  },

  // --- SEASON TICKET
  SEASON_TICKET_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Абонемент не найден.',
    },
  },
  ONGOING_EVENT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Мероприятие не найдено.',
    },
  },
  MODERATING_SEASON_TICKET_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Модерируемый абонемент не найден.',
    },
  },
  MODERATING_EVENT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Модерируемое мероприятие не найдено.',
    },
  },
  ACCOUNTABLE_MUSEUM_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Подотчетный музей не найден.',
    },
  },
  SELLING_EVENT_MUSEUMS_NOT_FOUND: {
    httpCode: 400,
    message: {
      ru: () => 'Музеи продающие билеты на мероприятия не найдены.',
    },
  },
  ACCOUNTABLE_MUSEUMS_NOT_FOUND: {
    httpCode: 400,
    message: {
      ru: () => 'Подотчетные музеи не найдены.',
    },
  },
  MODERATING_MUSEUM_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Модерируемый музей не найден.',
    },
  },
  CANT_DELETE_LAST_ACCOUNTABLE_MUSEUM: {
    httpCode: 400,
    message: {
      ru: () => 'Нельзя удалить последний музей.',
    },
  },
  ACCOUNTABLE_ORGANIZATION_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Организация не найдена.',
    },
  },

  RESENDABLE_PURCHASE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Покупка не найдена.',
    },
  },

  REVIEWED_PURCHASE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Покупка не найдена.',
    },
  },
  APPROVED_REFUND_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Возврат не найден.',
    },
  },

  // --- billing
  UNABLE_TO_AUTHORIZE_IN_PAYMENT_SYSTEM: {
    httpCode: 401,
    message: {
      ru: () => 'Не удаётся авторизоваться в платежной системе.',
    },
  },
  PAYMENT_SIGN_IN_NOT_VALID: {
    httpCode: 400,
    message: {
      ru: () => 'Подпись испорчена.',
    },
  },
  PAID_INVOICE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Оплаченный счет не найден.',
    },
  },
  PAYMENT_AMOUNT_DOES_NOT_EQUAL_INVOICE_AMOUNT: {
    httpCode: 400,
    message: {
      ru: () => 'Сумма платежа не равна сумме счета.',
    },
  },

  ACCOUNTABLE_ORGANIZATION_REQUIRED: {
    httpCode: 403,
    message: {
      ru: () => 'Необходимо иметь подотчетную организцию.',
    },
  },

  UNABLE_TO_DETERMINE_SEASON_TICKET_TYPE: {
    httpCode: 400,
    message: {
      ru: () => 'Не удается определить тип абонемента.',
    },
  },

  REJECTED_REFUND_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Возврат не найден.',
    },
  },

  ONLY_ONE_FREE_CATEGORY_IN_ORDER: {
    httpCode: 400,
    message: {
      ru: () => 'В заказе может присутствовать только одна бесплатная категория.',
    },
  },
  FREE_CATEGORY_ONLY_WITH_PAID: {
    httpCode: 400,
    message: {
      ru: () => 'Для покупки бесплатной категории необходимо добавить в заказ платную категорию.',
    },
  },
  SALES_NOT_STARTED_YET: {
    httpCode: 400,
    message: {
      ru: () => 'Продажи билетов на мероприятие еще не начались.',
    },
  },
  CANNOT_CREATE_MORE_THAN_ONE_ORGANIZATION: {
    httpCode: 400,
    message: {
      ru: () => 'Нельзя создать больше одной организации.',
    },
  },
  MUST_CONFIRM_EMAIL_BEFORE_CREATE_ORGANIZATION: {
    httpCode: 403,
    message: {
      ru: () => 'Перед созданием организации нужно подтвердить электронную почту, привязанную к аккаунту.',
    },
  },
  PROCESSED_REFUND_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Обработанный возврат не найден.',
    },
  },
  CANCELED_PURCHASE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Покупка не найдена.',
    },
  },
  PUBLISHED_MUSEUM_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Публикуемый музей не найден.',
    },
  },
  CANCELED_EVENT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Отменяемое мероприятие не найдено.',
    },
  },
  EVENT_ORGANIZATOR_REFUND_CONTACTS_REQUIRED: {
    httpCode: 400,
    message: {
      ru: () => 'Необходимо указать контакты по вопросам возврата.',
    },
  },
  PUBLISHED_SEASON_TICKET_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Публикуемый абонемент не найден.',
    },
  },
  UPDATED_ENTRANCE_TICKET_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Обновленный входной билет не найден.',
    },
  },
  PUBLISHED_EVENT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Публикуемое мероприятие не найдено.',
    },
  },

  // ОШИБКИ СЧИТЫВАТЕЛЯ ПРИ ЛОГИНЕ
  FAILED_CONTROLLER_ORGANIZATION_LOGIN_ATTEMPT: {
    httpCode: 429,
    message: {
      ru: ({ timeout }) => `Попытки ввода исчерпаны. Обратитесь к администратору или попробуйте войти через ${timeout} минут.`,
    },
  },
  CONTROLLER_LOGIN_ATTEMPTS_EXCEEDED: {
    httpCode: 429,
    message: {
      ru: ({ timeout }) => `Попытки ввода исчерпаны. Обратитесь к администратору или попробуйте войти через ${timeout} минут.`,
    },
  },
  CONTROLLER_ACCOUNT_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Сотрудник не найден. Обратитесь к администратору.',
    },
  },
  CONTROLLER_ACTIVE_SESSION_ALREADY_EXISTS: {
    httpCode: 409,
    message: {
      ru: () => 'Под этим номером уже был выполнен вход с другого устройства. Введите свой номер сотрудника или выйдите из другого аккаунта.',
    },
  },

  // ОШИБКИ ДЛЯ СЧИТЫВАТЕЛЯ ПРИ ПРОБИТИИ БИЛЕТА
  VISIT_PERIOD_HAS_NOT_STARTED_YET: {
    httpCode: 400,
    message: {
      ru: () => 'Срок действия билета еще не наступил.',
    },
  },
  PURCHASE_EXPIRED: {
    httpCode: 400,
    message: {
      ru: () => 'Срок действия билета истек.',
    },
  },
  PASSED_PURCHASE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Билет не найден.',
    },
  },
  PURCHASE_ALREADY_PASSED: {
    httpCode: 400,
    message: {
      ru: () => 'Билет уже погашен.',
    },
  },
  INVALID_PURCHASE_ID_FORMAT: {
    httpCode: 400,
    message: {
      ru: () => 'Неправильный формат номера билета.',
    },
  },

  ACCESS_DENIED: {
    httpCode: 403,
    message: {
      ru: () => 'Доступ запрещен.',
    },
  },

  // ошибки модуля виджетов
  WIDGET_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Виджет не найден.',
    },
  },
  WIDGET_DOES_NOT_INCLUDES_SELECTED_MUSEUM: {
    httpCode: 403,
    message: {
      ru: () => 'Данный виджет не имеет доступа к выбранному музею.',
    },
  },
  WIDGET_PLACEMENT_ADDRESS_IS_NOT_CORRECT: {
    httpCode: 403,
    message: {
      ru: () => 'Фактический адрес виджета не совпадает с указанным при создании.',
    },
  },
  WIDGET_DOES_NOT_INCLUDES_PURCHASED_TICKET: {
    httpCode: 403,
    message: {
      ru: () => 'Данный виджет не имеет доступа к выбранному билету.',
    },
  },

  MUSEUMS_NOT_AFFILIATED_WITH_ORGANIZATION: {
    httpCode: 400,
    message: {
      ru: () => 'Выбранные музеи не привязаны к организации.',
    },
  },

  PURCHASE_NOT_FOUND: {
    httpCode: 404,
    message: {
      ru: () => 'Покупка не найдена.',
    },
  },
};
