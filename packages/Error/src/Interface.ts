export interface BusinessErrorInterface {
  type: string;
  message: string;
  code: string;
  stacktrace?: string;
  locale?: string;
  params?: any;
}
