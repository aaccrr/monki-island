import { errors } from './ErrorList';
import { BusinessErrorInterface } from './Interface';

type LocaleEnum = 'ru';

const DEFAULT_ERROR = {
  httpCode: 500,
  message: {
    ru: () => '',
  },
};

export class BusinessError extends Error {
  type = 'BUSINESS_ERROR';
  httpCode: number;
  message: string;
  code: string;
  params: any;
  locale: LocaleEnum = 'ru';

  constructor(locale: LocaleEnum, code: string, params?: any) {
    super();

    const errorStruct = errors[code] || DEFAULT_ERROR;

    this.message = errorStruct.message[locale](params);
    this.httpCode = errorStruct.httpCode;
    this.code = code;
    this.locale = locale;

    if (params) this.params = params;
  }

  format(): BusinessErrorInterface {
    const errorView: BusinessErrorInterface = {
      type: this.type,
      message: this.message,
      code: this.code,
    };

    if (this.params) errorView.params = this.params;

    return errorView;
  }

  formatWithLocale(): BusinessErrorInterface {
    const errorView: BusinessErrorInterface = {
      locale: this.locale,
      type: this.type,
      message: this.message,
      code: this.code,
      stacktrace: this.stack,
    };

    if (this.params) errorView.params = this.params;

    return errorView;
  }
}
