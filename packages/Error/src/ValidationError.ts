type LocaleEnum = 'ru';

export class ValidationError extends Error {
  type = 'VALIDATION_ERROR';
  errors: any[];
  httpCode: number;
  locale: LocaleEnum = 'ru';
  isPublic = true;

  constructor(locale: LocaleEnum, errors: any[], httpCode?: number, isPublic?: boolean) {
    super();

    this.errors = errors;
    this.httpCode = httpCode || 400;
    this.locale = locale;
    if (typeof isPublic === 'boolean') this.isPublic = isPublic;
  }

  format(): any {
    return {
      type: this.type,
      errors: this.errors,
    };
  }

  formatWithLocale(): any {
    return {
      locale: this.locale,
      type: this.type,
      errors: this.errors,
      isPublic: this.isPublic,
      stacktrace: this.stack,
    };
  }
}
