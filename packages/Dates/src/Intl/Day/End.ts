import * as Luxon from 'luxon';

export const End = (intl: string, zone: string = 'Europe/Moscow'): Date => {
  return Luxon.DateTime.fromFormat(intl, 'yyyy-MM-dd', { zone }).endOf('day').toJSDate();
};
