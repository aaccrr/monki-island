import * as Luxon from 'luxon';

export const Start = (intl: string, zone: string = 'Europe/Moscow'): Date => {
  return Luxon.DateTime.fromFormat(intl, 'yyyy-MM-dd', { zone }).startOf('day').toJSDate();
};
