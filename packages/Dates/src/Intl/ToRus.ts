export const ToRus = (intl: string): string => {
  return intl.split('-').reverse().join('.');
};
