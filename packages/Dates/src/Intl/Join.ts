export const Join = (intl: string): string => {
  return intl.replace(/-/g, '');
};
