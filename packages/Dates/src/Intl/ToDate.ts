import * as Luxon from 'luxon';

export const ToDate = (intl: string, zone: string = 'Europe/Moscow'): Date => {
  return Luxon.DateTime.fromFormat(intl, 'yyyy-MM-dd', { zone }).toJSDate();
};
