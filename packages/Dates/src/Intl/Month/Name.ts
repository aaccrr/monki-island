import * as Luxon from 'luxon';

export const Name = (intl: string, zone: string = 'Europe/Moscow'): string => {
  return Luxon.DateTime.fromFormat(intl, 'yyyy-MM-dd', { zone }).setLocale('ru').toFormat('LLLL');
};
