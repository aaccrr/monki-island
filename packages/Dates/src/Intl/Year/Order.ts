import * as Luxon from 'luxon';

export const Order = (intl: string, zone: string = 'Europe/Moscow'): number => {
  return Luxon.DateTime.fromFormat(intl, 'yyyy-MM-dd', { zone }).get('year');
};
