import * as Luxon from 'luxon';

export const Subtract = (date: Date, zone: string = 'Europe/Moscow'): Date => {
  return Luxon.DateTime.fromJSDate(date, { zone }).minus({ days: 1 }).toJSDate();
};
