import * as Luxon from 'luxon';

export const ToOfficial = (date: Date, zone: string = 'Europe/Moscow'): string => {
  return Luxon.DateTime.fromJSDate(date, { zone }).toFormat('dd.MM.yyyy');
};
