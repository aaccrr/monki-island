import * as Luxon from 'luxon';

export const ToIntl = (date: Date, zone: string = 'Europe/Moscow'): string => {
  return Luxon.DateTime.fromJSDate(date, { zone }).toFormat('yyyy-MM-dd');
};
