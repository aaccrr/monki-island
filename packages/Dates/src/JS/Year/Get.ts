import * as Luxon from 'luxon';

export const Get = (date: Date, zone: string = 'Europe/Moscow'): number => {
  return Luxon.DateTime.fromJSDate(date, { zone }).get('year');
};
