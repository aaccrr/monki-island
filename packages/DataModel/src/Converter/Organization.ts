import { PartialDeep } from 'type-fest';
import { IOrganizationModel, OrganizationStatusEnum } from '../LegacyModel';
import { OrganizationModel, OrganizationStateEnum, OrganizationForm } from '../Model';

export const Exists = (...fields: any[]): boolean => fields.some((field) => field !== undefined);

export const OrganizationModelConverter = (model: Partial<IOrganizationModel>): OrganizationModel => {
  const organizationModel: PartialDeep<OrganizationModel> = {};

  if (Exists(model.id)) {
    organizationModel.id = model.id;
  }

  if (Exists(model.form)) {
    if (model.form === 'budgetary') {
      organizationModel.form = OrganizationForm.budgetary;
      organizationModel.undisputedFeeRetention = false;
    }

    if (model.form === 'commercial') {
      organizationModel.form = OrganizationForm.commercial;
      organizationModel.undisputedFeeRetention = true;
    }
  }

  if (Exists(model.undisputedFeeRetention)) {
    organizationModel.undisputedFeeRetention = model.undisputedFeeRetention;
  }

  if (Exists(model.name, model.shortName, model.fullName)) {
    organizationModel.name = {};

    if (Exists(model.name)) {
      organizationModel.name.value = model.name;
    }

    if (Exists(model.shortName)) {
      organizationModel.name.short = model.shortName;
    }

    if (Exists(model.fullName)) {
      organizationModel.name.full = model.fullName;
    }
  }

  if (Exists(model.legalAddress, model.mailingAddress, model.location)) {
    organizationModel.address = {};

    if (Exists(model.legalAddress)) {
      organizationModel.address.legal = model.legalAddress;
    }

    if (Exists(model.mailingAddress)) {
      organizationModel.address.mailing = model.mailingAddress;
    }

    if (Exists(model.location)) {
      organizationModel.address.place = model.location;
    }
  }

  if (Exists(model.docs)) {
    organizationModel.document = {};

    if (Exists(model.docs.creation)) {
      organizationModel.document.creation = model.docs.creation?.src || '';
    }

    if (Exists(model.docs.egurl)) {
      organizationModel.document.egurl = model.docs.egurl?.src || '';
    }

    if (Exists(model.docs.manager)) {
      organizationModel.document.manager = model.docs.manager?.src || '';
    }

    if (Exists(model.docs.nalog)) {
      organizationModel.document.nalog = model.docs.nalog?.src || '';
    }

    if (Exists(model.docs.registration)) {
      organizationModel.document.registration = model.docs.registration?.src || '';
    }
  }

  if (Exists(model.settings)) {
    organizationModel.setting = {};

    if (Exists(model.settings.automaticRefundApprovalEnabled)) {
      organizationModel.setting.automaticRefundApprovalEnabled = model.settings.automaticRefundApprovalEnabled;
    }

    if (Exists(model.settings.isOfflineSalesEnabled)) {
      organizationModel.setting.offlineSalesEnabled = model.settings.isOfflineSalesEnabled;
    }
  }

  if (Exists(model.id, model.contract, model.creation)) {
    organizationModel.contract = {};

    if (Exists(model.id)) {
      organizationModel.contract.number = model.id;
    }

    if (Exists(model.creation)) {
      if (Exists(model.creation.createdAt)) {
        organizationModel.contract.signDate = model.creation.createdAt as any;
      }
    }

    if (Exists(model.contract)) {
      if (Exists(model.contract.number)) {
        if (model.contract.number) {
          organizationModel.contract.number = model.contract.number;
        }
      }

      if (Exists(model.contract.signedAt)) {
        if (model.contract.signedAt !== null) {
          organizationModel.contract.signDate = model.contract.signedAt as any;
        }
      }
    }
  }

  if (Exists(model.inn, model.ogrn, model.kpp, model.okato, model.okogu, model.okved, model.okpo)) {
    organizationModel.identifier = {};

    if (Exists(model.inn)) {
      organizationModel.identifier.inn = model.inn;
    }

    if (Exists(model.ogrn)) {
      organizationModel.identifier.ogrn = model.ogrn;
    }

    if (Exists(model.kpp)) {
      organizationModel.identifier.kpp = model.kpp;
    }

    if (Exists(model.okato)) {
      organizationModel.identifier.okato = model.okato;
    }

    if (Exists(model.okogu)) {
      organizationModel.identifier.okogu = model.okogu;
    }

    if (Exists(model.okved)) {
      organizationModel.identifier.okved = model.okved;
    }

    if (Exists(model.okpo)) {
      organizationModel.identifier.okpo = model.okpo;
    }
  }

  if (Exists(model.bank)) {
    organizationModel.bank = {};

    if (Exists(model.bank.bankBIK)) {
      organizationModel.bank.bik = model.bank.bankBIK;
    }

    if (Exists(model.bank.bankKS)) {
      organizationModel.bank.correspondentAccount = model.bank.bankKS;
    }

    if (Exists(model.bank.bankName)) {
      organizationModel.bank.name = model.bank.bankName;
    }

    if (Exists(model.bank.bill)) {
      organizationModel.bank.checkingAccount = model.bank.bill;
    }

    if (Exists(model.bank.kbk)) {
      organizationModel.bank.kbk = model.bank.kbk;
    }

    if (Exists(model.bank.paymentsRecipient)) {
      organizationModel.bank.paymentRecipient = model.bank.paymentsRecipient;
    }
  }

  if (Exists(model.contactEmail, model.contactPhone)) {
    organizationModel.contact = {};

    if (Exists(model.contactEmail)) {
      organizationModel.contact.email = model.contactEmail;
    }

    if (Exists(model.contactPhone)) {
      organizationModel.contact.phone = model.contactPhone;
    }
  }

  if (Exists(model.director)) {
    if (Exists(model.director.fio)) {
      organizationModel.signer = {
        fio: model.director.fio,
      };
    }
  }

  if (Exists(model.status)) {
    organizationModel.state = OrganizationStateConverter(model);
  }

  if (Exists(model.pushkinCardRealizationProgramContract)) {
    organizationModel.pushkinCardContract = {};

    if (Exists(model.pushkinCardRealizationProgramContract.number)) {
      organizationModel.pushkinCardContract.number = model.pushkinCardRealizationProgramContract.number;
    }

    if (Exists(model.pushkinCardRealizationProgramContract.signDate)) {
      organizationModel.pushkinCardContract.signDate = model.pushkinCardRealizationProgramContract.signDate;
    }
  }

  return organizationModel as OrganizationModel;
};

export const OrganizationStateConverter = (model: Partial<IOrganizationModel>): OrganizationStateEnum => {
  if (model.status === OrganizationStatusEnum.PREVIEW) {
    return 'loaded';
  }

  if (model.status === OrganizationStatusEnum.WAITING_FOR_CONFIRMATION) {
    return 'registered';
  }

  if (model.status === OrganizationStatusEnum.CONFIRMED) {
    return 'signed';
  }

  return 'deleted';
};
