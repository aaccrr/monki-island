import { Nullable } from '@monki-island/common';
import { ILocationModel, IOrganizedEventClientCategoryPriceModel, IOrganizedEventDayModel, IOrganizedEventModel } from '../LegacyModel/Event';
import {
  EventClientCategoryModel,
  EventClientCategoryPriceModel,
  EventIntegrationModel,
  EventLocationModel,
  EventModel,
  EventOrganizatorContactModel,
  EventOrganizatorModel,
  EventScheduleDateIntervalModel,
  EventScheduleDateModel,
  EventScheduleLongPeriodModel,
  EventScheduleModel,
  EventScheduleTypeModel,
  EventStatusEnum,
  EventScheduleUnionTicketModel,
} from '../Model/OrganizationManagement';
import * as Legacy from '../LegacyModel/Event';
import { EventPositionTypeEnum } from '../';

export const HasLongPeriod = (model: IOrganizedEventModel): boolean => {
  return typeof model?.longPeriod?.start === 'string';
};

export const HasUnionTicket = (model: IOrganizedEventModel): boolean => {
  return Array.isArray(model?.unionTicket?.dates);
};

export const EventModelConverter = (model: IOrganizedEventModel): EventModel => {
  const location = EventLocationConverter(model);

  return {
    id: model.id,
    slug: model.slug,
    organizationId: model.organizationId,
    museumId: model.museums[0].id,
    title: model.title,
    description: model.description,
    category: {
      custom: typeof model.category.id !== 'number',
      id: typeof model.category.id === 'number' ? model.category.id : null,
      name: typeof model.category.id !== 'number' ? model.category.name : null,
    },
    clientCategory: EventClientCategoryConverter(model),
    content: {
      gallery: model.images,
      previewImage: model.previewImage,
    },
    condition: {
      perOrderLimit: model.countByClient,
      ageCensor: model.ageCensor,
    },
    schedule: EventScheduleConverter(model, location),
    organizator: EventOrganizatorConverter(model),
    integration: EventIntegrationConverter(model),
    location,
    state: EventStateConverter(model),
    lastChangedAt: model.update?.updatedAt || null,
    createdAt: model.creation?.createdAt || model.update?.updatedAt || new Date(),
    deletedAt: model.deletion?.deletedAt || null,
  };
};

export const EventStateConverter = (model: IOrganizedEventModel): EventStatusEnum => {
  if (model.status === Legacy.OrganizedEventStatusEnum.PENDING) {
    return 'created';
  }

  if (model.status === Legacy.OrganizedEventStatusEnum.APPROVED) {
    return 'created';
  }

  if (model.status === Legacy.OrganizedEventStatusEnum.DELETED) {
    return 'deleted';
  }

  if (model.status === Legacy.OrganizedEventStatusEnum.CANCELED) {
    return 'canceled';
  }

  if (model.status === Legacy.OrganizedEventStatusEnum.FINISHED) {
    return 'finished';
  }

  return 'requires_addition';
};

export const EventClientCategoryConverter = (model: IOrganizedEventModel): EventClientCategoryModel[] => {
  const isLongPeriod = HasLongPeriod(model);

  return model.clientCategories.map((clientCategory) => {
    return {
      id: clientCategory.id,
      name: clientCategory.name,
      price: clientCategory.prices.map((price) => EventClientCategoryPriceConverter(price, isLongPeriod)),
      group: clientCategory.groups,
    };
  });
};

export const EventClientCategoryPriceConverter = (
  price: IOrganizedEventClientCategoryPriceModel,
  isLongPeriod: boolean
): EventClientCategoryPriceModel => {
  const categoryPrice: Partial<EventClientCategoryPriceModel> = {
    online: price.price.online,
    offline: price.price.offline,
  };

  if (isLongPeriod) {
    categoryPrice.ticketType = 'long_period';
  } else {
    categoryPrice.ticketType = Array.isArray(price.date) ? 'union' : price.date;
  }

  return categoryPrice as EventClientCategoryPriceModel;
};

export const EventScheduleConverter = (model: IOrganizedEventModel, location: EventLocationModel[]): EventScheduleModel => {
  return {
    type: EventScheduleTypeConverter(model),
    day: EventScheduleDayConverter(model, location),
    longPeriod: EventScheduleLongPeriodConverter(model, location),
    unionTicket: EventScheduleUnionTicketConverter(model),
  };
};

export const EventScheduleLongPeriodConverter = (
  model: IOrganizedEventModel,
  location: EventLocationModel[]
): Nullable<EventScheduleLongPeriodModel> => {
  if (!HasLongPeriod(model)) {
    return null;
  }

  return {
    legacyPositionId: 1,
    start: model.longPeriod.start,
    finish: model.longPeriod.finish,
    saleStart: model.longPeriod.saleStart,
    location: EventScheduleLocationConverter(model.longPeriod.address, location),
  };
};

export const EventScheduleUnionTicketConverter = (model: IOrganizedEventModel): Nullable<EventScheduleUnionTicketModel> => {
  if (!HasUnionTicket(model)) {
    return null;
  }

  return {
    legacyPositionId: model.days.length + 1,
    include: model.unionTicket.dates,
    saleStart: model.unionTicket.salesStartsAt,
    online: model.unionTicket.tickets.online || null,
    offline: model.unionTicket.tickets.offline || null,
  };
};

export const EventScheduleTypeConverter = (model: IOrganizedEventModel): EventScheduleTypeModel => {
  if (model.days.length > 0) {
    return 'daily';
  }

  return 'long_period';
};

export const EventScheduleDayConverter = (model: IOrganizedEventModel, location: EventLocationModel[]): EventScheduleDateModel[] => {
  return model.days.map((day, index) => {
    return {
      legacyPositionId: index + 1,
      date: day.date,
      isFree: day.isFree,
      atNight: day.duration.atNight,
      interval: [EventScheduleDayIntervalConverter(day, EventScheduleLocationConverter(day.address, location))],
      timeline: day.timeline,
    };
  });
};

export const EventScheduleLocationConverter = (scheduleLocation: ILocationModel, location: EventLocationModel[]): number => {
  const locationMatching = location.find(
    (location) => location.location.lat === scheduleLocation.location.lat && location.location.lon === scheduleLocation.location.lon
  );

  return locationMatching.id;
};

export const EventScheduleDayIntervalConverter = (day: IOrganizedEventDayModel, locationId: number): EventScheduleDateIntervalModel => {
  return {
    location: locationId,
    from: day.duration.from,
    to: day.duration.to,
    saleStart: day.salesStartsAt,
    online: day.tickets.online || null,
    offline: day.tickets.offline || null,
  };
};

export const EventOrganizatorConverter = (model: IOrganizedEventModel): EventOrganizatorModel => {
  const contact: EventOrganizatorContactModel[] = model.contacts.map((contact) => {
    return {
      type: contact.isPublic ? 'feedback' : 'private',
      name: contact.name,
      phone: contact.phone || '',
      email: contact.email || '',
    };
  });

  const contactRefund = model.contacts.find((contact) => contact.name === 'По вопросам возврата');

  if (contactRefund !== undefined) {
    contact.push({
      type: 'refund',
      name: 'По вопросам возврата',
      phone: contactRefund.phone,
      email: contactRefund.email,
    });
  }

  return {
    contact,
  };
};

export const EventIntegrationConverter = (model: IOrganizedEventModel): EventIntegrationModel => {
  return {
    proCultureId: model.proCultureId || null,
  };
};

export const EventLocationConverter = (model: IOrganizedEventModel): EventLocationModel[] => {
  return model.positions
    .filter((position) => position.type !== EventPositionTypeEnum.UNION_TICKET)
    .reduce((locations, day) => {
      day.dates.forEach((date) => {
        locations.push({
          id: day.id,
          ...EventLocationModelConverter(date.location),
        } as EventLocationModel);
      });

      return locations;
    }, [] as EventLocationModel[]);
};

export const EventLocationModelConverter = (location: ILocationModel): Partial<EventLocationModel> => {
  return {
    localityId: location.localityId,
    provinceId: location.provinceId,
    regionId: location.regionId,
    timezone: location.timezone,
    location: location.location,
  };
};
