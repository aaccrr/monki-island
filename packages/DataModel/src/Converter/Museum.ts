import { IAdministeredMuseumModel, MuseumStatusEnum } from '../LegacyModel/Museum';
import {
  MuseumFeedbackContactModel,
  MuseumLocationModel,
  MuseumModel,
  MuseumStateModel,
  MuseumTypeModel,
  MuseumWorkingScheduleDayModel,
} from '../Model';

export const MuseumModelConverter = (model: IAdministeredMuseumModel): MuseumModel => {
  return {
    id: model.id,
    slug: model.slug,
    organizationId: model.organizationId,
    name: model.name,
    description: model.description,
    type: MuseumTypeConverter(model),
    location: MuseumLocationModelConverter(model),
    category: model.categories.map((category) => category.id),
    content: {
      gallery: model.images,
      previewImage: model.previewImage,
    },
    feedback: {
      contact: MuseumFeedbackContactModelConverter(model),
      website: model.officialSite,
    },
    workingSchedule: {
      schedule: MuseumWorkingScheduleScheduleConverter(model),
      addition: model.schedule.additionalInfo,
      sanitaryDay: model.sanitaryDays,
    },
    visit: {
      rules: {
        source: model.rules.src,
        lastUpdatedAt: model.rules.uploadedAt as any,
      },
      isFree: model.isFree,
      service: model.services,
      freeDayAnnounce: model.freeDays.additionalInfo,
      covidRestriction: model.implementsCovidRestriction || false,
    },
    state: MuseumStateModelConverter(model),
    lastChangeAt: model.update.updatedAt,
    createdAt: model.creation.createdAt,
    deletedAt: model.delete.deletedAt || null,
  };
};

export const MuseumTypeConverter = (model: IAdministeredMuseumModel): MuseumTypeModel => {
  return model.type.toLowerCase() as MuseumTypeModel;
};

export const MuseumWorkingScheduleScheduleConverter = (model: IAdministeredMuseumModel): MuseumWorkingScheduleDayModel[] => {
  return model.schedule.openingHours.map((scheduleDay) => {
    return {
      weekDay: scheduleDay.weekDay,
      openingHour: {
        from: scheduleDay.from,
        to: scheduleDay.to,
      },
      break: {
        from: scheduleDay.breakFrom,
        to: scheduleDay.breakTo,
      },
    };
  });
};

export const MuseumLocationModelConverter = (model: IAdministeredMuseumModel): MuseumLocationModel => {
  return {
    locality: model.location.locality,
    localityId: model.location.localityId,
    provinceId: model.location.provinceId,
    region: model.location.region,
    regionId: model.location.regionId,
    address: model.location.address,
    location: model.location.location,
    timezone: model.location.timezone,
    subjectCenter: !!model.location.isSubjectCenter,
  };
};

export const MuseumFeedbackContactModelConverter = (model: IAdministeredMuseumModel): MuseumFeedbackContactModel[] => {
  const contact: MuseumFeedbackContactModel[] = model.contacts.map((contact) => {
    return {
      type: contact.isPublic ? 'feedback' : 'private',
      name: contact.name,
      phone: contact.phone || '',
      email: contact.email || '',
    };
  });

  const contactRefund = model.contacts.find((contact) => contact.name === 'По вопросам возврата');

  if (contactRefund !== undefined) {
    contact.push({
      type: 'refund',
      name: 'По вопросам возврата',
      phone: contactRefund.phone,
      email: contactRefund.email,
    });
  }

  return contact;
};

export const MuseumStateModelConverter = (model: IAdministeredMuseumModel): MuseumStateModel => {
  if (model.status === MuseumStatusEnum.ACTIVE) {
    return 'created';
  }

  if (model.status === MuseumStatusEnum.REQUIRES_ADDITION) {
    return 'addition_required';
  }

  if (model.status === MuseumStatusEnum.DELETED) {
    return 'deleted';
  }

  return 'loaded';
};
