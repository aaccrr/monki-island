import { EntranceTicketStatusEnum, IEntranceTicketModel, TicketSaleScheduleModel, VisitConditionModel, VisitConditionTypeEnum } from '../../..';
import { IntervalModel } from '../../../Model/OrganizationManagement/VisitCondition/Values/Interval';
import { EntranceTicketVisitConditionId } from '../EntranceTicketVisitConditionId';
import { EntranceTicket } from './EntranceTicket';

export class EntranceTicketToTime extends EntranceTicket {
  constructor(entranceTicket: IEntranceTicketModel) {
    super(entranceTicket);

    this.salesSchedule = this.packSalesSchedule();
    this.visitConditions = this.packVisitConditions();
  }

  packVisitConditions(): VisitConditionModel[] {
    return this.entranceTicket.salesSchedule.toTime.reduce((visitConditions, toTimeDate) => {
      toTimeDate.intervals.forEach((interval) => {
        const visitConditionValue: IntervalModel = {
          from: {
            date: toTimeDate.date,
            time: interval.from,
          },
          to: {
            date: toTimeDate.date,
            time: interval.to,
          },
        };

        visitConditions.push({
          id: EntranceTicketVisitConditionId(this.id, VisitConditionTypeEnum.Interval, visitConditionValue),
          type: VisitConditionTypeEnum.Interval,
          value: visitConditionValue,
          limit: interval.online,
        });
      });

      return visitConditions;
    }, [] as VisitConditionModel[]);
  }

  packSalesSchedule(): TicketSaleScheduleModel {
    return {
      state: this.entranceTicket.status === EntranceTicketStatusEnum.SUSPENDED ? 'suspended' : 'opened',
      type: 'to_time',
      dayOffSales: this.entranceTicket.dayOffSales || false,
      toTime: this.entranceTicket.salesSchedule.toTime.map((toTimeDate) => {
        return {
          date: toTimeDate.date,
          interval: toTimeDate.intervals,
        };
      }),
      suspendedAt: null,
    };
  }
}
