import { ClientCategoryModel, EntranceTicketModel, IEntranceTicketModel, TicketSaleScheduleModel, TicketStateEnum } from '../../../';
import { VisitorsCategoryModel } from '../../../Model/OrganizationManagement/VisitorsCategory';
import { EntranceTicketId } from '../EntranceTicketId';
import { VisitorsCategoryId } from '../../VisitorsCategoryId';
import { VisitConditionModel } from '../../../Model';
import { EntranceTicketStatusEnum } from '../../../LegacyModel';

export class EntranceTicket {
  constructor(protected entranceTicket: IEntranceTicketModel) {
    this.id = EntranceTicketId(this.entranceTicket.id);
  }

  protected id: string;
  protected salesSchedule: TicketSaleScheduleModel;
  protected visitConditions: VisitConditionModel[] = [];

  pack(): EntranceTicketModel {
    return {
      id: this.id,
      publicId: this.entranceTicket.id,
      organizationId: this.entranceTicket.organizationId,
      museumId: this.entranceTicket.museum.id,
      title: this.entranceTicket.title,
      type: 'ticket',
      visit: {
        includedInPrice: this.entranceTicket.includedInPrice,
      },
      clientCategory: this.packCategories(),
      condition: {
        visitLimitNumber: this.entranceTicket.passLimitNumber,
        ageCensor: this.entranceTicket.ageCensor,
        canBeTransferred: this.entranceTicket.canBeTransferred,
        isPersonal: this.entranceTicket.isPersonal,
        validityPeriod: this.entranceTicket.expiresAt,
        perOrderLimit: this.entranceTicket.countByClient,
      },
      saleSchedule: this.salesSchedule,
      location: {
        timezone: this.entranceTicket.timezone,
      } as any,
      state: this.packStatus(),
      visitConditions: this.visitConditions,
      visitorsCategories: this.packVisitorsCategories(),
      lastChangedAt: this.entranceTicket.update.updatedAt,
      createdAt: this.entranceTicket.creation.createdAt,
      deletedAt: this.entranceTicket.deletion.deletedAt || null,
    };
  }

  private packStatus(): TicketStateEnum {
    if (this.entranceTicket.status == EntranceTicketStatusEnum.APPROVED) {
      return 'created';
    }

    if (this.entranceTicket.status == EntranceTicketStatusEnum.PENDING) {
      return 'created';
    }

    if (this.entranceTicket.status == EntranceTicketStatusEnum.SUSPENDED) {
      return 'suspended';
    }

    if (this.entranceTicket.status == EntranceTicketStatusEnum.DELETED) {
      return 'deleted';
    }
  }

  protected packCategories(): ClientCategoryModel[] {
    return this.entranceTicket.clientCategories.map((clientCategory) => {
      return {
        id: clientCategory.id,
        name: clientCategory.name,
        price: clientCategory.price.online,
        group: clientCategory.groups,
      };
    });
  }

  protected packVisitorsCategories(): VisitorsCategoryModel[] {
    return this.entranceTicket.clientCategories.map((clientCategory) => {
      return {
        legacyId: clientCategory.id,
        id: VisitorsCategoryId(this.id, clientCategory.name, clientCategory.price.online),
        name: clientCategory.name,
        price: clientCategory.price.online,
        groups: clientCategory.groups,
      };
    });
  }
}
