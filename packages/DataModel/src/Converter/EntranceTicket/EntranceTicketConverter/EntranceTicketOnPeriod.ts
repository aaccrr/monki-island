import { EntranceTicketStatusEnum, IEntranceTicketModel, TicketSaleScheduleModel } from '../../../';
import { VisitConditionModel, VisitConditionTypeEnum } from '../../../Model';
import { VisitsCountModel } from '../../../Model/OrganizationManagement/VisitCondition/Values/VisitsCount';
import { EntranceTicketVisitConditionId } from '../EntranceTicketVisitConditionId';
import { EntranceTicket } from './EntranceTicket';

export class EntranceTicketOnPeriod extends EntranceTicket {
  constructor(entranceTicket: IEntranceTicketModel) {
    super(entranceTicket);

    this.salesSchedule = this.packSalesSchedule();
    this.visitConditions = this.packVisitConditions();
  }

  packVisitConditions(): VisitConditionModel[] {
    const visitsCount: VisitsCountModel = {
      visits: this.entranceTicket.passLimitNumber,
      period: this.entranceTicket.expiresAt,
    };

    return [
      {
        id: EntranceTicketVisitConditionId(this.id, VisitConditionTypeEnum.VisitsCount, visitsCount),
        type: VisitConditionTypeEnum.VisitsCount,
        value: visitsCount,
        limit: null,
      },
    ];
  }

  packSalesSchedule(): TicketSaleScheduleModel {
    return {
      state: this.entranceTicket.status === EntranceTicketStatusEnum.SUSPENDED ? 'suspended' : 'opened',
      type: 'none',
      dayOffSales: this.entranceTicket.dayOffSales || false,
      toTime: [],
      suspendedAt: null,
    };
  }
}
