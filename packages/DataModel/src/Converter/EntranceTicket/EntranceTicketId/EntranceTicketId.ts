import { createHash } from 'crypto';

export const HashString = (string: string): string => {
  return createHash('sha256').update(string).digest('hex');
};

export const EntranceTicketId = (id: string): string => {
  return HashString(id);
};
