import { VisitConditionTypeEnum, VisitConditionValueModel } from '../../..';
import { VisitsCountModel, IntervalModel } from '../../../Model/OrganizationManagement/VisitCondition';
import { HashString } from '../EntranceTicketId';

export const EntranceTicketVisitConditionId = (nomenclatureId: string, type: VisitConditionTypeEnum, value: VisitConditionValueModel): string => {
  if (type === VisitConditionTypeEnum.VisitsCount) {
    const validityPeriodValue = value as VisitsCountModel;
    return HashString(`${nomenclatureId}|none|${validityPeriodValue.visits}`);
  }

  const intervalValue = value as IntervalModel;
  return HashString(`${nomenclatureId}|to_time|${intervalValue.from.date}:${intervalValue.from.time}:${intervalValue.to.time}`);
};
