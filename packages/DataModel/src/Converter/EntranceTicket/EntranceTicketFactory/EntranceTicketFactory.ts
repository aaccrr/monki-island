import { EntranceTicketModel, IEntranceTicketModel } from '../../../';
import { EntranceTicketOnPeriod, EntranceTicketToTime } from '../EntranceTicketConverter';

export class EntranceTicketFactory {
  static create(entranceTicket: IEntranceTicketModel): EntranceTicketModel {
    if (entranceTicket.salesSchedule.toTime.length > 0) {
      return new EntranceTicketToTime(entranceTicket).pack();
    }

    return new EntranceTicketOnPeriod(entranceTicket).pack();
  }
}
