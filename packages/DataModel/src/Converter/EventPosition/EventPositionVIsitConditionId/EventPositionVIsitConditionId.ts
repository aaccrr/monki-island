import { IntervalModel, MultipleIntervalsModel, VisitConditionTypeEnum, VisitConditionValueModel } from '../../..';
import { HashString } from '../EventPositionId';

export const EventPositionVisitConditionId = (nomenclatureId: string, type: VisitConditionTypeEnum, value: VisitConditionValueModel): string => {
  if (type === VisitConditionTypeEnum.LongInterval) {
    const longIntervalValue = value as IntervalModel;
    const dna = [nomenclatureId, 'long_period', `${longIntervalValue.from.date}:${longIntervalValue.to.date}`].join('|');
    return HashString(dna);
  }

  if (type === VisitConditionTypeEnum.Interval) {
    const intervalValue = value as IntervalModel;
    const dna = [nomenclatureId, 'date', intervalValue.from.date].join('|');
    return HashString(dna);
  }

  if (type === VisitConditionTypeEnum.MultipleIntervals) {
    const multipleIntervalsValue = value as MultipleIntervalsModel;
    const dna = [nomenclatureId, 'union_ticket', multipleIntervalsValue.intervals.map((interval) => interval.from.date).join(':')].join('|');
    return HashString(dna);
  }
};
