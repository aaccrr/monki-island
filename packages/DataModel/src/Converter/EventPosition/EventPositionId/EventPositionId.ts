import { createHash } from 'crypto';

export const HashString = (string: string): string => {
  return createHash('sha256').update(string).digest('hex');
};

export const EventPositionId = (eventId: string, type: string, date: string[]): string => {
  return HashString(`${eventId}:${type}:${date.join(':')}`);
};
