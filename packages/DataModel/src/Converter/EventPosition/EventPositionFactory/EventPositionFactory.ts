import { EventModel, EventPositionModel } from '../../..';
import { LongPeriod, EventDate, UnionTicket } from '../EventPositionConverter';

export class EventPositionsFactory {
  static create(event: EventModel): EventPositionModel[] {
    if (!event) {
      return [];
    }

    if (event.schedule.type === 'daily') {
      const dailyPositions = event.schedule.day.map((day) => {
        return new EventDate(event, day.date).pack();
      });

      if (event.schedule.unionTicket !== null) {
        dailyPositions.push(new UnionTicket(event).pack());
      }

      return dailyPositions;
    }

    const longPeriodPosition = new LongPeriod(event);
    return [longPeriodPosition.pack()];
  }
}
