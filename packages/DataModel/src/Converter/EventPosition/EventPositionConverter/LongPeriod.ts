import {
  EventModel,
  EventPositionSaleScheduleModel,
  EventPositionScheduleModel,
  IntervalModel,
  VisitConditionModel,
  VisitConditionTypeEnum,
} from '../../..';
import { EventPositionId } from '../EventPositionId';
import { EventPositionVisitConditionId } from '../EventPositionVIsitConditionId';
import { EventPosition } from './EventPosition';

const BeutifyDate = (date: string): string => {
  const [year, month, day] = date.split('-');
  return [day, month, year].join('.');
};

export class LongPeriod extends EventPosition {
  constructor(event: EventModel) {
    super(event);

    this.type = 'long_period';
    this.legacyPositionId = this.event.schedule.longPeriod.legacyPositionId;

    this.start = this.event.schedule.longPeriod.start;
    this.finish = this.event.schedule.longPeriod.finish;

    this.id = this.packId();
    this.title = this.packTitle();

    this.schedule = this.packSchedule();
    this.saleSchedule = this.packSalesSchedule();
    this.visitConditions = this.packVisitConditions();
  }

  private start: string;
  private finish: string;

  packId(): string {
    return EventPositionId(this.event.id, this.type, [this.start, this.finish]);
  }

  packTitle(): string {
    return `${this.event.title}: ${BeutifyDate(this.start)} - ${BeutifyDate(this.finish)}`;
  }

  packSchedule(): EventPositionScheduleModel {
    return {
      type: 'long_period',
      start: this.event.schedule.longPeriod.start,
      finish: this.event.schedule.longPeriod.finish,
    };
  }

  packSalesSchedule(): EventPositionSaleScheduleModel {
    return {
      start: this.event.schedule.longPeriod.saleStart,
    };
  }

  packVisitConditions(): VisitConditionModel[] {
    const value: IntervalModel = {
      from: {
        date: this.start,
        time: '00:00',
      },
      to: {
        date: this.finish,
        time: '23:59',
      },
    };

    const visitCondition: VisitConditionModel = {
      id: EventPositionVisitConditionId(this.id, VisitConditionTypeEnum.LongInterval, value),
      type: VisitConditionTypeEnum.LongInterval,
      value,
      limit: null,
    };

    return [visitCondition];
  }
}
