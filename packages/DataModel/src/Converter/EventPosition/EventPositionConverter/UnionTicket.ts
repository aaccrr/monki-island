import { VmuzeyTimezonedDateHelper } from 'monki-island';
import { EventPositionId } from '../EventPositionId';
import {
  EventModel,
  EventPositionSaleScheduleModel,
  EventPositionScheduleModel,
  MultipleIntervalsModel,
  VisitConditionModel,
  VisitConditionTypeEnum,
  VisitorsCategoryModel,
} from '../../..';
import { EventPosition } from './EventPosition';
import { EventPositionVisitConditionId } from '../EventPositionVIsitConditionId';

const BeutifyDate = (date: string): string => {
  const [year, month, day] = date.split('-');
  return [day, month, year].join('.');
};

const SortDate = (dateUnsorted: string[]): string[] => {
  return dateUnsorted
    .map((date) => {
      const timestamp = new VmuzeyTimezonedDateHelper(date).getUnixTimestamp();

      return {
        timestamp,
        date,
      };
    })
    .sort((a, b) => a.timestamp - b.timestamp)
    .map((dateSorted) => dateSorted.date);
};

export class UnionTicket extends EventPosition {
  constructor(event: EventModel) {
    super(event);

    this.type = 'union';
    this.combinedDates = SortDate(this.event.schedule.unionTicket.include);
    this.legacyPositionId = this.event.schedule.unionTicket.legacyPositionId;

    this.id = this.packId();
    this.title = this.packTitle();

    this.schedule = this.packSchedule();
    this.saleSchedule = this.packSalesSchedule();
    this.visitConditions = this.packVisitConditions();
  }

  private combinedDates: string[] = [];

  packId(): string {
    return EventPositionId(this.event.id, 'union_ticket', this.combinedDates);
  }

  packTitle(): string {
    const dates = this.combinedDates.map((combinedDate) => BeutifyDate(combinedDate));
    return `${this.event.title}: ${dates.join(', ')}`;
  }

  packSchedule(): EventPositionScheduleModel {
    return {
      type: 'union_ticket',
      include: this.event.schedule.day.filter((day) => this.combinedDates.includes(day.date)),
    };
  }

  packSalesSchedule(): EventPositionSaleScheduleModel {
    return {
      start: this.event.schedule.unionTicket.saleStart,
    };
  }

  packVisitConditions(): VisitConditionModel[] {
    const value: MultipleIntervalsModel = {
      intervals: this.combinedDates.map((combinedDate) => {
        const day = this.event.schedule.day.find((day) => day.date === combinedDate);

        return {
          from: {
            date: day.date,
            time: day.interval[0].from,
          },
          to: {
            date: day.date,
            time: day.interval[0].to,
          },
        };
      }),
    };

    const visitCondition: VisitConditionModel = {
      id: EventPositionVisitConditionId(this.id, VisitConditionTypeEnum.MultipleIntervals, value),
      type: VisitConditionTypeEnum.MultipleIntervals,
      value,
      limit: this.event.schedule.unionTicket.online,
    };

    return [visitCondition];
  }
}
