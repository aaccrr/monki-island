import { VisitorsCategoryId } from '../..';
import { ClientCategoryModel, EventModel, EventPositionModel, EventPositionSaleScheduleModel, EventPositionScheduleModel } from '../../../';
import { VisitConditionModel } from '../../../Model';
import { VisitorsCategoryModel } from '../../../Model/OrganizationManagement/VisitorsCategory';

export abstract class EventPosition {
  constructor(protected event: EventModel) {}

  protected id: string;
  protected title: string;
  protected type: 'union' | 'long_period' | string;
  protected legacyPositionId: number;

  protected schedule: EventPositionScheduleModel;
  protected saleSchedule: EventPositionSaleScheduleModel;
  protected visitConditions: VisitConditionModel[] = [];

  pack(): EventPositionModel {
    return {
      id: this.id,
      legacyPositionId: this.legacyPositionId,
      publicId: this.event.id,
      organizationId: this.event.organizationId,
      museumId: this.event.museumId,
      type: 'event',
      title: this.title,
      schedule: this.schedule,
      saleSchedule: this.saleSchedule,
      clientCategory: this.packClientCategories(),
      location: [], // this.event.location,
      sellerLocation: this.event.location[0],
      pushkinCardProgram: typeof this.event.integration.proCultureId === 'number',
      visitConditions: this.visitConditions,
      visitorsCategories: this.packVisitorsCategories(),
      createdAt: this.event.createdAt,
      lastChangedAt: this.event.lastChangedAt,
      deletedAt: this.event.deletedAt,
    };
  }

  protected packClientCategories(): ClientCategoryModel[] {
    return this.event.clientCategory.map((clientCategory) => {
      const price = clientCategory.price.find((price) => price.ticketType === this.type);

      return {
        id: clientCategory.id,
        name: clientCategory.name,
        group: clientCategory.group,
        price: price?.online || 0,
      };
    });
  }

  protected packVisitorsCategories(): VisitorsCategoryModel[] {
    return this.event.clientCategory.map((clientCategory) => {
      const price = clientCategory.price.find((price) => price.ticketType === this.type);

      return {
        legacyId: clientCategory.id,
        id: VisitorsCategoryId(this.id, clientCategory.name, price?.online || 0),
        name: clientCategory.name,
        groups: clientCategory.group,
        price: price?.online || 0,
      };
    });
  }
}
