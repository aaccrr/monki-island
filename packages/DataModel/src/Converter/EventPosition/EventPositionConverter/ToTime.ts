import { EventPositionId } from '../EventPositionId';
import {
  EventModel,
  EventPositionSaleScheduleModel,
  EventPositionScheduleModel,
  EventScheduleDateModel,
  IntervalModel,
  VisitConditionModel,
  VisitConditionTypeEnum,
} from '../../..';
import { EventPosition } from './EventPosition';
import { EventPositionVisitConditionId } from '../EventPositionVIsitConditionId';

const BeutifyDate = (date: string): string => {
  const [year, month, day] = date.split('-');
  return [day, month, year].join('.');
};

export class EventDate extends EventPosition {
  constructor(event: EventModel, private date: string) {
    super(event);

    this.type = this.date;
    this.day = this.event.schedule.day.find((day) => day.date === this.date);
    this.legacyPositionId = this.day.legacyPositionId;

    this.id = this.packId();
    this.title = this.packTitle();

    this.schedule = this.packSchedule();
    this.saleSchedule = this.packSalesSchedule();
    this.visitConditions = this.packVisitConditions();
  }

  private day: EventScheduleDateModel;

  packId(): string {
    return EventPositionId(this.event.id, 'date', [this.day.date]);
  }

  packTitle(): string {
    return `${this.event.title}: ${BeutifyDate(this.day.date)}`;
  }

  packSchedule(): EventPositionScheduleModel {
    return {
      type: 'date',
      date: this.day.date,
      interval: this.day.interval,
      timeline: [],
    };
  }

  packSalesSchedule(): EventPositionSaleScheduleModel {
    return {
      start: this.day.interval[0].saleStart,
    };
  }

  packVisitConditions(): VisitConditionModel[] {
    const value: IntervalModel = {
      from: {
        date: this.day.date,
        time: this.day.interval[0].from,
      },
      to: {
        date: this.day.date,
        time: this.day.interval[0].to,
      },
    };

    const visitCondition: VisitConditionModel = {
      id: EventPositionVisitConditionId(this.id, VisitConditionTypeEnum.Interval, value),
      type: VisitConditionTypeEnum.Interval,
      value,
      limit: this.day.interval[0].online || null,
    };

    return [visitCondition];
  }
}
