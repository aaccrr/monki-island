import { createHash } from 'crypto';

export const HashString = (string: string): string => {
  return createHash('sha256').update(string).digest('hex');
};

export const VisitorsCategoryId = (nomenclatureId: string, name: string, price: number): string => {
  return HashString(`${nomenclatureId}|${name}|${price}`);
};
