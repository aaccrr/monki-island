import { ISeasonTicketModel, VisitConditionModel, VisitConditionTypeEnum, VisitsCountModel } from '../../..';
import { SeasonTicketVisitConditionId } from '../SeasonTicketVisitConditionId';
import { SeasonTicket } from './SeasonTicket';

export class SeasonTicketOnVisits extends SeasonTicket {
  constructor(seasonTicket: ISeasonTicketModel) {
    super(seasonTicket);

    this.visitConditions = this.packVisitConditions();
  }

  packVisitConditions(): VisitConditionModel[] {
    const value: VisitsCountModel = {
      visits: this.seasonTicket.patterns.visits,
      period: this.seasonTicket.patterns.period || 90,
    };

    return [
      {
        id: SeasonTicketVisitConditionId(this.id, VisitConditionTypeEnum.VisitsCount, value),
        type: VisitConditionTypeEnum.VisitsCount,
        value,
        limit: this.seasonTicket.salesLimit.online,
      },
    ];
  }
}
