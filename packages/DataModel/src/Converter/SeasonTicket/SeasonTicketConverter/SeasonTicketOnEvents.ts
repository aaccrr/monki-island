import { VmuzeyTimezonedDateHelper } from 'monki-island';
import { ISeasonTicketModel, MultipleIntervalsModel, SeasonTicketConditionEventModel, VisitConditionModel, VisitConditionTypeEnum } from '../../..';
import { SeasonTicketVisitConditionId } from '../SeasonTicketVisitConditionId';
import { SeasonTicket } from './SeasonTicket';

export class SeasonTicketOnEvents extends SeasonTicket {
  constructor(seasonTicket: ISeasonTicketModel) {
    super(seasonTicket);

    this.visitConditions = this.packVisitConditions();
    this.events = this.packEvents();
  }

  packVisitConditions(): VisitConditionModel[] {
    const multipleIntervalsValue: MultipleIntervalsModel = {
      intervals: this.seasonTicket.patterns.events.map((event) => {
        const startDate = new VmuzeyTimezonedDateHelper(event.startsAt, { timezone: this.seasonTicket.timezone, subTimezone: false });
        const finishDate = new VmuzeyTimezonedDateHelper(event.startsAt, { timezone: this.seasonTicket.timezone, subTimezone: false }).addHours(10);

        return {
          from: {
            date: startDate.toFormattedString('YYYY-MM-DD'),
            time: startDate.toFormattedString('HH:mm'),
          },
          to: {
            date: finishDate.toFormattedString('YYYY-MM-DD'),
            time: finishDate.toFormattedString('HH:mm'),
          },
        };
      }),
    };

    return [
      {
        id: SeasonTicketVisitConditionId(this.id, VisitConditionTypeEnum.MultipleIntervals, multipleIntervalsValue),
        type: VisitConditionTypeEnum.MultipleIntervals,
        value: multipleIntervalsValue,
        limit: this.seasonTicket.salesLimit.online,
      },
    ];
  }

  packEvents(): SeasonTicketConditionEventModel[] {
    if (!Array.isArray(this.seasonTicket.patterns.events)) {
      return [];
    }

    return this.seasonTicket.patterns.events.map((event) => {
      return {
        name: event.name,
        start: event.startsAt as any,
      };
    });
  }
}
