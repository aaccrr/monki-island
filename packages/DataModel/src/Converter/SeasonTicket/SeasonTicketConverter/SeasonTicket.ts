import { ClientCategoryModel, ISeasonTicketModel, SeasonTicketConditionEventModel, SeasonTicketModel } from '../../../';
import { VisitorsCategoryModel } from '../../../Model/OrganizationManagement/VisitorsCategory';
import { SeasonTicketId } from '../SeasonTicketId';
import { SeasonTicketStateModel } from '../../../Model/OrganizationManagement/Nomenclature/SeasonTicket';
import { VisitorsCategoryId } from '../../VisitorsCategoryId';
import { VisitConditionModel } from '../../../Model';
import * as Legacy from '../../../LegacyModel';

export class SeasonTicket {
  constructor(protected seasonTicket: ISeasonTicketModel) {
    this.id = SeasonTicketId(this.seasonTicket.id);
  }

  protected id: string;
  protected visitConditions: VisitConditionModel[] = [];
  protected events: SeasonTicketConditionEventModel[] = [];

  pack(): SeasonTicketModel {
    return {
      id: this.id,
      publicId: this.seasonTicket.id,
      organizationId: this.seasonTicket.organizationId,
      museumId: this.seasonTicket.museums[0].id,
      title: this.seasonTicket.title,
      type: 'season_ticket',
      clientCategory: this.packCategories(),
      saleSchedule: {
        state: 'opened',
        limit: {
          online: this.seasonTicket.salesLimit.online || null,
          offline: this.seasonTicket.salesLimit.offline || null,
        },
        suspendScheduledDate: this.seasonTicket.salesEndDate as any,
      },
      condition: {
        perOrderLimit: this.seasonTicket.countByClient,
        ageCensor: this.seasonTicket.ageCensor,
        visitLimitNumber: this.seasonTicket.patterns.visits || null,
        validityPeriod: this.seasonTicket.patterns.period || 90,
        event: this.events,
      },
      location: {
        timezone: this.seasonTicket.timezone,
      } as any,
      state: this.packState(),
      visitConditions: this.visitConditions,
      visitorsCategories: this.packVisitorsCategories(),
      lastChangedAt: this.seasonTicket.update.updatedAt,
      createdAt: this.seasonTicket.creation.createdAt,
      deletedAt: this.seasonTicket.deletion.deletedAt || null,
    };
  }

  protected packState(): SeasonTicketStateModel {
    if (this.seasonTicket.status === Legacy.SeasonTicketStatusEnum.PENDING) {
      return 'created';
    }

    if (this.seasonTicket.status === Legacy.SeasonTicketStatusEnum.APPROVED) {
      return 'created';
    }

    if (this.seasonTicket.status === Legacy.SeasonTicketStatusEnum.CANCELED) {
      return 'canceled';
    }

    if (this.seasonTicket.status === Legacy.SeasonTicketStatusEnum.DELETED) {
      return 'deleted';
    }
  }

  protected packCategories(): ClientCategoryModel[] {
    return this.seasonTicket.clientCategories.map((clientCategory) => {
      return {
        id: clientCategory.id,
        name: clientCategory.name,
        price: clientCategory.price.online,
        group: clientCategory.groups,
      };
    });
  }

  protected packVisitorsCategories(): VisitorsCategoryModel[] {
    return this.seasonTicket.clientCategories.map((clientCategory) => {
      return {
        legacyId: clientCategory.id,
        id: VisitorsCategoryId(this.id, clientCategory.name, clientCategory.price.online),
        name: clientCategory.name,
        price: clientCategory.price.online,
        groups: clientCategory.groups,
      };
    });
  }
}
