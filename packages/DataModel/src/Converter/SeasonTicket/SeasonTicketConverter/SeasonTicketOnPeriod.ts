import { ISeasonTicketModel, ValidityPeriodModel, VisitConditionModel, VisitConditionTypeEnum } from '../../..';
import { SeasonTicketVisitConditionId } from '../SeasonTicketVisitConditionId';
import { SeasonTicket } from './SeasonTicket';

export class SeasonTicketOnPeriod extends SeasonTicket {
  constructor(seasonTicket: ISeasonTicketModel) {
    super(seasonTicket);

    this.visitConditions = this.packVisitConditions();
  }

  packVisitConditions(): VisitConditionModel[] {
    const validityPeriodValue: ValidityPeriodModel = {
      period: this.seasonTicket.patterns.period,
    };

    return [
      {
        id: SeasonTicketVisitConditionId(this.id, VisitConditionTypeEnum.ValidityPeriod, validityPeriodValue),
        type: VisitConditionTypeEnum.ValidityPeriod,
        value: validityPeriodValue,
        limit: this.seasonTicket.salesLimit.online,
      },
    ];
  }
}
