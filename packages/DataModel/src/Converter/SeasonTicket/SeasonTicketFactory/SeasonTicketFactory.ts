import { ISeasonTicketModel, SeasonTicketModel, SeasonTicketPatternType } from '../../..';
import { SeasonTicketOnPeriod, SeasonTicketOnEvents, SeasonTicketOnVisits } from '../SeasonTicketConverter';

export class SeasonTicketFactory {
  static create(seasonTicket: ISeasonTicketModel): SeasonTicketModel {
    if (seasonTicket.patterns.type === SeasonTicketPatternType.PERIOD) {
      return new SeasonTicketOnPeriod(seasonTicket).pack();
    }

    if (seasonTicket.patterns.type === SeasonTicketPatternType.VISITS || seasonTicket.patterns.type === SeasonTicketPatternType.UNIVERSAL) {
      return new SeasonTicketOnVisits(seasonTicket).pack();
    }

    if (seasonTicket.patterns.type === SeasonTicketPatternType.EVENTS) {
      return new SeasonTicketOnEvents(seasonTicket).pack();
    }
  }
}
