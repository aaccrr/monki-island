import { MultipleIntervalsModel, ValidityPeriodModel, VisitConditionTypeEnum, VisitConditionValueModel, VisitsCountModel } from '../../..';
import { HashString } from '../SeasonTicketId';

export const SeasonTicketVisitConditionId = (nomenclatureId: string, type: VisitConditionTypeEnum, value: VisitConditionValueModel): string => {
  if (type === VisitConditionTypeEnum.VisitsCount) {
    const visitsCountValue = value as VisitsCountModel;
    return HashString(`${nomenclatureId}|visit|${visitsCountValue.period}:${visitsCountValue.visits}`);
  }

  if (type === VisitConditionTypeEnum.ValidityPeriod) {
    const validityPeriodValue = value as ValidityPeriodModel;
    return HashString(`${nomenclatureId}|period|${validityPeriodValue.period}`);
  }

  const multipleIntervalsValue = value as MultipleIntervalsModel;
  return HashString(`${nomenclatureId}|event|${multipleIntervalsValue.intervals.map((interval) => interval.from.date).join(':')}`);
};
