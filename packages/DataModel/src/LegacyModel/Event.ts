import { Nullable, Timezone } from '@monki-island/common';
import { EventConditionAgeCensorModel } from '../Model/OrganizationManagement';
import { ClientCategoryModel } from '../Model/OrganizationManagement/ClientCategory';
import { IClientCategoryModel, ICreationLogModel, IDeletionLogModel, IEntranceTicketMuseumModel, IUpdateLogModel, LastChangeEnum } from './Shared';

export interface IOrganizedEventModel {
  id: string;
  title: string;
  slug: string;
  description: string;
  museums: IEntranceTicketMuseumModel[];
  category: IEventCategoryModel;
  organizationId: string;
  organizationName: string;

  previewImage: ICroppedPreviewImageModel;
  images: ICroppedGalleryImageModel[];

  ageCensor: EventConditionAgeCensorModel | null;

  positions: IEventPositionModel[];

  sellerTimezone: Timezone;

  clientCategories: IOrganizedEventClientCategoryModel[];
  days: IOrganizedEventDayModel[];
  longPeriod: EventLongPeriodView;
  unionTicket: IOrganizedEventUnionTicketModel;
  contacts: IEventOrganizatorContactModel[];

  countByClient: number;
  isFree: boolean;
  status: OrganizedEventStatusEnum;
  canEdit: boolean;
  latestExpirationDateOfPurchase: Date | null;
  isAddedToCatalog: boolean;
  proCultureId: Nullable<number>;

  places?: EventPlaceModel[];

  lastChange: LastChangeEnum;
  creation: ICreationLogModel;
  update: IUpdateLogModel;
  deletion: IDeletionLogModel;
}

export interface EventPlaceModel {
  id: string;
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  address: string;
  location: IGeoPoint;
  timezone: Timezone;
  timezoneOffset: string;
  isSubjectCenter: boolean | number;
}

export interface IEventPositionModel {
  id: number;
  type: EventPositionTypeEnum;
  dates: IEventPositionDateModel[];
  clientCategories: IClientCategoryModel[];
  salesStartDate: Date;
  finishDate: Date;
  salesLimit: IEventSalesLimitModel;
  minPrice: number;
  maxPrice: number;
  isFree: boolean;
  heldAtNight: boolean;
  heldAtNightUntil: string | null;
}

export enum EventPositionTypeEnum {
  DAY = 'DAY',
  UNION_TICKET = 'UNION_TICKET',
}

export interface IEventPositionDateModel {
  id: number;
  startsAt: Date;
  finishAt: Date;
  museum: any;
  timeline: IEventTimelinePointModel[];
  location: ILocationModel;
}

export interface ICropBounds {
  width: number;
  height: number;
  x: number;
  y: number;
}

export interface ICroppedGalleryImageModel {
  order: number;
  crop: ICropBounds;
  src: string;
}

export interface ICroppedPreviewImageModel {
  crop: ICropBounds;
  src: string;
}

export interface IOrganizedEventClientCategoryModel {
  id: number;
  name: string;
  prices: IOrganizedEventClientCategoryPriceModel[];
  groups: string[];
}

export interface IOrganizedEventClientCategoryPriceModel {
  date: string | string[];
  price: {
    online: number;
    offline: number;
  };
}

export interface IEventOrganizatorContactModel {
  name: string;
  phone: string;
  email: string;
  isPublic: boolean;
}

export enum OrganizedEventStatusEnum {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  DELETED = 'DELETED',
  CANCELED = 'CANCELED',
  FINISHED = 'FINISHED',
}

export interface IEventCategoryModel {
  id: number | null;
  name: string;
}

export interface IOrganizedEventDayModel {
  date: string;
  duration: IEventDurationModel;
  address: ILocationModel;
  salesStartsAt: string;
  timeline: IEventTimelinePointModel[];
  tickets: IEventSalesLimitModel;
  isFree: boolean;
  placeId?: string;
}

export interface IEventDurationModel {
  from: string;
  to: string;
  atNight: boolean;
}

export interface ILocationModel {
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  address: string;
  location: IGeoPoint;
  timezone: Timezone;
  timezoneOffset: string;
  isSubjectCenter: boolean | number;
}

export interface IGeoPoint {
  lat: number;
  lon: number;
}

export interface IEventTimelinePointModel {
  from: string;
  to: string;
  description: string;
}

export interface IEventSalesLimitModel {
  online: number | null;
  offline: number | null;
}

export interface IOrganizedEventUnionTicketModel {
  dates: string[];
  salesStartsAt: string;
  tickets: IEventSalesLimitModel;
  isFree: boolean;
}

export interface EventLongPeriodView {
  start: string;
  finish: string;
  saleStart: string;
  address: ILocationModel;
  placeId?: string;
}
