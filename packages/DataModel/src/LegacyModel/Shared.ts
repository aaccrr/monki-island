export interface IEntranceTicketMuseumModel {
  id: string;
  name: string;
}

export enum LastChangeEnum {
  'CREATE' = 'CREATE',
  'UPDATE' = 'UPDATE',
  'APPROVE' = 'APPROVE',
  'DELETE' = 'DELETE',
}

export interface ICreationLogModel {
  createdAt: Date;
  createdBy: string;
  approvedAt: Date | null;
  approvedBy: string | null;
}

export interface IUpdateLogModel {
  updatedAt: Date;
  updatedBy: string;
  updatedFields: string[];
  approvedAt: Date | null;
  approvedBy: string | null;
}

export interface IDeletionLogModel {
  deletedAt: Date;
  deletedBy: string;
}

export interface IClientCategoryModel {
  id: number;
  name: string;
  price: IClientCategoryPriceModel;
  groups: string[];
}

export interface IClientCategoryPriceModel {
  online: number;
  offline: number;
}
