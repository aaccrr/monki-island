import { Timezone } from '@monki-island/common';

export interface IOrganizationModel {
  id: string;
  name: string;
  address: string;
  inn: string;
  ogrn: string;
  kpp: string;
  director: IOrganizationDirectorModel;
  mailingAddress: string;
  contactPhone: string[];
  contactEmail: string;
  bank: IOrganizationBankModel;
  booker: string;
  shortName: string;
  fullName: string;
  legalAddress: string;
  okato: string;
  okogu: string;
  okved: string;
  okpo: string;
  registrationDate: string;
  organizationStatus: string;
  location: IOrganizationLocationModel;

  status: OrganizationStatusEnum;

  pushkinCardProgram: boolean;
  hasMuseum: boolean;
  hasController: boolean;
  hasGuide: boolean;

  settings: IOrganizationSettingsModel;
  docs: IOrganizationDocumentsModel;

  contract: IOrganizationContractModel;
  creation: IOrganizationCreationModel;

  pushkinCardRealizationProgramContract: {
    number: string;
    signDate: string;
  };

  form: 'budgetary' | 'commercial';
  undisputedFeeRetention: boolean;
}

export interface IOrganizationCreationModel {
  createdBy: string;
  createdByRole: any;
  createdAt: Date;
}

export enum OrganizationStatusEnum {
  PREVIEW = 'PREVIEW',
  WAITING_FOR_CONFIRMATION = 'WAITING_FOR_CONFIRMATION',
  CONFIRMED = 'CONFIRMED',
}

export interface IOrganizationSettingsModel {
  isOfflineSalesEnabled: boolean;
  automaticRefundApprovalEnabled: boolean;
  reportsMailingList: string[];
}

export interface IOrganizationBankModel {
  bill: string;
  bankName: string;
  bankBIK: string;
  bankKS: string;
  paymentsRecipient: string;
  kbk: string;
}

export interface IOrganizationDirectorModel {
  fio: string;
  inn: string;
}

export interface IOrganizationContractModel {
  number: string;
  signedAt: Date;
}

export interface IOrganizationDocumentsModel {
  registration: IOrganizationDocumentModel;
  nalog: IOrganizationDocumentModel;
  manager: IOrganizationDocumentModel;
  creation: IOrganizationDocumentModel;
  egurl: IOrganizationDocumentModel;
}

export interface IOrganizationDocumentModel {
  src: string;
  uploadedAt: string;
}

export interface IOrganizationLocationModel {
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  timezone: Timezone;
}
