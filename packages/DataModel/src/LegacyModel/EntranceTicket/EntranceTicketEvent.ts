import { IBaseItemModel, IPurchasePassModel } from './EntranceTicketBase';
import { NomenclatureTypeEnum } from './NomenclatureType';
import { LocationModel } from '../..';
import { Timezone } from '@monki-island/common';

export interface IEventItemModel extends IBaseItemModel {
  nomenclature: IPurchasedEventModel;
  condition: IEventConditionModel;
}

export interface IEventConditionModel {
  dates: IEventConditionDateModel[];
}

export interface IEventConditionDateModel {
  id: number;
  from: Date;
  to: Date;
  timezone: Timezone;
  status: EventConditionDateStatusEnum;
  pass: IPurchasePassModel;
}

export enum EventConditionDateStatusEnum {
  ACTIVE = 'ACTIVE',
  PASSED = 'PASSED',
}

export interface IPurchasedEventModel {
  id: string;
  title: string;
  type: NomenclatureTypeEnum;
  slug: string;
  category: IEventCategoryModel;
  schedule: IPurchasedEventSchedulePointModel[];
}

export interface IPurchasedEventSchedulePointModel {
  id: number;
  startsAt: Date;
  finishAt: Date;
  timeline: IEventTimelinePointModel[];
  location: LocationModel;
}

interface IEventCategoryModel {
  id: number | null;
  name: string;
}

interface IEventTimelinePointModel {
  from: string;
  to: string;
  description: string;
}
