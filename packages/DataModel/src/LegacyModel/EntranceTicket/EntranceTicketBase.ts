import { Timezone } from '@monki-island/common';

export interface IBaseItemModel {
  id: string;
  orderId: string;
  type: PurchaseTypeEnum;
  status: PurchaseStatusEnum;
  museums: string[];
  price: string;
  serviceProvider: IServiceProviderModel;
  props: IPurchasePropsModel;
  customer: ICustomerModel;
  customerCategory: ICustomerCategoryModel;
  customerLocation: ICustomerLocationModel;

  isFree: boolean;
  isPersonal: boolean;

  refund: {
    id: null | string;
    refundedAt: null | Date;
  };
  payment: IPurchasePaymentModel;
  pass: IPurchasePassModel;

  createdAt: Date;

  widgetId?: string;

  nomenclatureId?: string;
  visitConditionId?: string;
  visitorCategoryId?: string;
}

export interface ICustomerModel {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}

export interface ICustomerCategoryModel {
  id: number;
  name: string;
  groups: string[];
}

export interface IServiceProviderModel {
  id: string;
  inn: string;
  name: string;
  shortName: string;
  address: string;
  contacts: IPublicContactModel[];
  refundContacts: IRefundContactView;
  site: string;
}

export interface IPublicContactModel {
  name: string;
  phone: string;
  email: string;
}

export interface IRefundContactView {
  phone: string;
  email?: string;
}

export interface IPurchasePropsModel {
  positionId: number;
  date: string;
  time: string;
}

export interface IPurchasePaymentModel {
  id: string;
  invoiceId: string;
  paidAt: Date | null;
  pushkinCard: boolean;
  rrn?: string;
}

export enum PurchaseStatusEnum {
  CREATED = 'CREATED', // заказ создан и клиенты выставлен счет
  UNPAID = 'UNPAID', // заказ не был оплачен в отведенный срок
  ACTIVE = 'ACTIVE', // заказ оплачен
  IN_REFUND_PROCESS = 'IN_REFUND_PROCESS', // на заказ был создан возврат
  CLOSED_BY_REFUND = 'CLOSED_BY_REFUND', // заказ закрыт после одобрения заявки на возврат
  REFUNDED = 'REFUNDED', // деньги за заказ были возвращены клиенту
  PASSED = 'PASSED', // клиент прошел по билету
  EXPIRED = 'EXPIRED', // у заказа закончился срок действия и он не бы использован
}

export enum PurchaseTypeEnum {
  ORDER = 'ORDER',
  ORDER_ITEM = 'ORDER_ITEM',
}

export interface IPurchasePassModel {
  passedAt: Date | null;
  allowedBy: string | null;
}

export interface ICustomerLocationModel {
  localityId: string;
  timezone: Timezone;
}
