import { IBaseItemModel } from './EntranceTicketBase';
import { NomenclatureTypeEnum } from './NomenclatureType';
import { IVisitedMuseumModel } from './VisitedMuseum';

export interface ISeasonTicketItemModel extends IBaseItemModel {
  condition: ISeasonTicketConditionModel;
  visitedMuseum: IVisitedMuseumModel;
  nomenclature: IPurchasedSeasonTicketModel;
}

export interface IPurchasedSeasonTicketModel {
  id: string;
  title: string;
  type: NomenclatureTypeEnum;
}

export enum SeasonTicketConditionTypeEnum {
  EVENTS = 'EVENTS',
  VISITS = 'VISITS',
  PERIOD = 'PERIOD',
  UNIVERSAL = 'UNIVERSAL',
}

export interface IPurchasePassModel {
  passedAt: Date | null;
  allowedBy: string | null;
}

export interface ISeasonTicketConditionModel {
  type: SeasonTicketConditionTypeEnum;
  events?: ISeasonTicketConditionEventModel[];
  total?: number;
  left?: number;
  from?: Date;
  to?: Date;
}

export interface ISeasonTicketConditionEventModel {
  id: number;
  title: string;
  from: Date;
  to: Date;
  status: string;
  pass: IPurchasePassModel;
}
