import { IEntranceTicketItemModel } from './EntranceTicketTicket';
import { ISeasonTicketItemModel } from './EntranceTicketSeasonTicket';
import { IEventItemModel } from './EntranceTicketEvent';

export type IItemModel = IEntranceTicketItemModel | ISeasonTicketItemModel | IEventItemModel;
