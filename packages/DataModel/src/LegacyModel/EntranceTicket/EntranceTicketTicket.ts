import { IBaseItemModel } from './EntranceTicketBase';
import { NomenclatureTypeEnum } from './NomenclatureType';
import { IVisitedMuseumModel } from './VisitedMuseum';

export interface IEntranceTicketItemModel extends IBaseItemModel {
  nomenclature: IPurchasedEntranceTicketModel;
  visitedMuseum: IVisitedMuseumModel;
  condition: IEntranceTicketConditionModel;
}

export interface IPurchasedEntranceTicketModel {
  id: string;
  title: string;
  type: NomenclatureTypeEnum;
  includedInPrice: string[];
}

export enum EntranceTicketConditionTypeEnum {
  PERIOD = 'PERIOD',
  TIME = 'TIME',
}

export interface IEntranceTicketConditionModel {
  type: EntranceTicketConditionTypeEnum;
  passLimitNumberTotal: number;
  passLimitNumberLeft: number;
  from: Date;
  to: Date;
}
