import { LocationModel } from '../..';

export interface IVisitedMuseumModel {
  id: string;
  name: string;
  slug: string;
  location: LocationModel;
  schedule: IMuseumScheduleDayModel[];
  scheduleComment: string;
  site: string;
}

interface IMuseumScheduleDayModel {
  weekDay: number;
  from: string | null;
  to: string | null;
  breakFrom: string | null;
  breakTo: string | null;
}
