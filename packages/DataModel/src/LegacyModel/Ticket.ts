import { Timezone } from '@monki-island/common';
import { TicketConditionAgeCensorModel } from '../Model/OrganizationManagement';
import { IClientCategoryModel, ICreationLogModel, IDeletionLogModel, IEntranceTicketMuseumModel, IUpdateLogModel, LastChangeEnum } from './Shared';

export interface IEntranceTicketModel {
  id: string;
  title: string;
  organizationId: string;
  organizationName: string;
  museum: IEntranceTicketMuseumModel;
  timezone: Timezone;
  status: EntranceTicketStatusEnum;
  salesSchedule: IEntranceTicketSalesScheduleModel;
  countByClient: number;
  expiresAt: number;
  isPersonal: boolean;
  clientCategories: IClientCategoryModel[];
  minPrice: number;
  maxPrice: number;
  dayOffSales: boolean;
  canBeTransferred: boolean;
  ageCensor: TicketConditionAgeCensorModel;

  canEdit: boolean;
  lastChange: LastChangeEnum;
  isAddedToCatalog: boolean;
  latestExpirationDateOfPurchase: Date | null;
  includedInPrice: string[];
  passLimitNumber: number;

  creation: ICreationLogModel;
  update: IUpdateLogModel;
  deletion: IDeletionLogModel;
}

export enum EntranceTicketStatusEnum {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  SUSPENDED = 'SUSPENDED',
  DELETED = 'DELETED',
}

export interface IEntranceTicketSalesScheduleModel {
  weekly: IEntranceTicketSalesWeeklyModel[];
  free: IEntranceTicketSalesFreeModel[];
  toTime: IEntranceTicketSalesToTimeModel[];
}

export interface IEntranceTicketSalesWeeklyModel {
  weekDayOrder: number;
  online: number | null;
  offline: number | null;
}

export interface IEntranceTicketSalesFreeModel {
  date: string;
  online: number | null;
  offline: number | null;
}

export interface IEntranceTicketSalesToTimeModel {
  date: string;
  intervals: IEntranceTicketToTimeIntervalModel[];
}

export interface IEntranceTicketToTimeIntervalModel {
  from: string;
  to: string;
  online: number | null;
  offline: number | null;
}
