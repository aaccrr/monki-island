import { Timezone } from '@monki-island/common';
import { SeasonTicketConditionAgeCensorModel } from '../Model/OrganizationManagement';
import { IClientCategoryModel, ICreationLogModel, IDeletionLogModel, IEntranceTicketMuseumModel, IUpdateLogModel, LastChangeEnum } from './Shared';

export interface ISeasonTicketModel {
  id: string;
  title: string;
  organizationId: string;
  organizationName: string;
  timezone: Timezone;
  museums: IEntranceTicketMuseumModel[];
  clientCategories: IClientCategoryModel[];
  patterns: ISeasonTicketPatternModel;
  salesLimit: ISalesLimitModel;
  salesEndDate: Date;
  minPrice: number;
  maxPrice: number;
  countByClient: number;
  status: SeasonTicketStatusEnum;
  ageCensor: SeasonTicketConditionAgeCensorModel;

  lastChange: LastChangeEnum;
  canEdit: boolean;
  latestExpirationDateOfPurchase: Date | null;
  isAddedToCatalog: boolean;

  creation: ICreationLogModel;
  update: IUpdateLogModel;
  deletion: IDeletionLogModel;
  cancellation: ICancellationLogModel;
}

export interface ISalesLimitModel {
  online: number | null;
  offline: number | null;
}

export interface ICancellationLogModel {
  canceledAt: Date;
  canceledBy: string;
}

export enum SeasonTicketStatusEnum {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  CANCELED = 'CANCELED',
  DELETED = 'DELETED',
}

export enum SeasonTicketPatternType {
  PERIOD = 'PERIOD',
  VISITS = 'VISITS',
  UNIVERSAL = 'UNIVERSAL',
  EVENTS = 'EVENTS',
}

export interface ISeasonTicketEventModel {
  name: string;
  startsAt: Date;
}

export interface ISeasonTicketPatternModel {
  type: SeasonTicketPatternType;
  visits?: number;
  period?: number;
  events?: ISeasonTicketEventModel[];
}
