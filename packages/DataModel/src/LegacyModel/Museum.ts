import {
  ICreationLogModel,
  ICroppedGalleryImageModel,
  ICroppedPreviewImageModel,
  IDeletionLogModel,
  ILocationModel,
  IUpdateLogModel,
  LastChangeEnum,
} from '.';

export interface IAdministeredMuseumModel {
  id: string;
  slug: string;
  organizationId: string;
  organizationName: string;
  name: string;
  description: string;
  location: ILocationModel;
  type: MuseumTypeEnum;
  categories: IMuseumCategoryModel[];
  contacts: IContactModel[];
  refundContacts: IMuseumRefundContactModel;
  officialSite: string;
  schedule: IMuseumScheduleModel;
  services: IMuseumServiceModel[];
  freeDays: IMuseumFreeDaysModel;
  previewImage: ICroppedPreviewImageModel;
  images: ICroppedGalleryImageModel[];
  rules: IMuseumRulesModel;
  status: MuseumStatusEnum;
  sanitaryDays: string[];
  isFree: boolean;
  implementsCovidRestriction: boolean;

  isAddedToCatalog: boolean;
  canContentManagerView: boolean;
  lastChange: LastChangeEnum;

  creation: ICreationLogModel;
  update: IUpdateLogModel;
  delete: IDeletionLogModel;
}

export enum MuseumStatusEnum {
  IDLE = 'IDLE',
  PENDING = 'PENDING',
  REQUIRES_ADDITION = 'REQUIRES_ADDITION',
  ACTIVE = 'ACTIVE',
  DELETED = 'DELETED',
}

export enum MuseumTypeEnum {
  MUSEUM = 'MUSEUM',
  GALLERY = 'GALLERY',
  EXHIBITION_CENTER = 'EXHIBITION_CENTER',
  EXHIBITION_HALL = 'EXHIBITION_HALL',
  PARK = 'PARK',
}

export interface IContactModel {
  name: string;
  phone: string;
  email: string;
  isPublic: boolean;
}

export interface IMuseumRulesModel {
  src: string;
  uploadedAt: Date;
}

export interface IMuseumFreeDaysModel {
  patterns: IMuseumFreeDayPatternModel[];
  dates: IMuseumFreeDayDateModel[];
  additionalInfo: string;
}

export interface IMuseumFreeDayPatternModel {
  inMonthOrder: number;
  weekDay: number;
  toString?: string;
}

export interface IMuseumFreeDayDateModel {
  date: string;
  name: string;
}

export interface IMuseumServiceModel {
  name: string;
  slug: string;
  order: number | null;
}

export interface IMuseumRefundContactModel {
  phone: string;
  email: string;
}

export interface IMuseumCategoryModel {
  id: number;
  name: string;
}

export interface IMuseumScheduleModel {
  openingHours: IMuseumScheduleDayModel[];
  additionalInfo: string;
}

export interface IMuseumScheduleDayModel {
  weekDay: number;
  from: string | null;
  to: string | null;
  breakFrom: string | null;
  breakTo: string | null;
}
