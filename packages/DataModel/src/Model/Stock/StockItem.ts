import { Nullable } from '@monki-island/common';
import { VisitConditionTypeEnum, VisitConditionValueModel } from '../OrganizationManagement';

export interface StockItem {
  _id: string;
  productId: string;
  nomenclatureId: string;
  type: VisitConditionTypeEnum;
  value: VisitConditionValueModel;
  limit: Nullable<number>;
  outDate: Nullable<Date>;
  availableStock: Nullable<number>;
}
