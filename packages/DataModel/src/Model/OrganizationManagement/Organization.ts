import { Timezone } from '@monki-island/common';

export interface OrganizationModel {
  id: string;
  name: OrganizationNameModel;
  form: OrganizationForm;
  address: OrganizationAddressModel;
  document: OrganizationDocumentModel;
  setting: OrganizationSettingModel;
  contract: OrganizationContractModel;
  pushkinCardContract: OrganizationContractModel;
  identifier: OrganizationIdentifierModel;
  bank: OrganizationBankModel;
  contact: OrganizationContactModel;
  signer: OrganizationSignerModel;
  state: OrganizationStateEnum;
  undisputedFeeRetention: boolean;
}

export enum OrganizationForm {
  budgetary = 'budgetary',
  commercial = 'commercial',
}

export interface OrganizationNameModel {
  value: string;
  short: string;
  full: string;
}

export interface OrganizationAddressModel {
  legal: string;
  mailing: string;
  place: OrganizationAddressPlaceModel;
}

export interface OrganizationAddressPlaceModel {
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  timezone: Timezone;
}

export interface OrganizationDocumentModel {
  registration: string;
  nalog: string;
  manager: string;
  creation: string;
  egurl: string;
}

export interface OrganizationSettingModel {
  offlineSalesEnabled: boolean;
  automaticRefundApprovalEnabled: boolean;
}

export interface OrganizationContractModel {
  number: string;
  signDate: string;
}

export interface OrganizationIdentifierModel {
  inn: string;
  ogrn: string;
  kpp: string;
  okato: string;
  okogu: string;
  okved: string;
  okpo: string;
}

export interface OrganizationBankModel {
  checkingAccount: string;
  name: string;
  bik: string;
  correspondentAccount: string;
  paymentRecipient: string;
  kbk: string;
}

export interface OrganizationContactModel {
  phone: string[];
  email: string;
}

export interface OrganizationSignerModel {
  fio: string;
}

export type OrganizationStateEnum = 'loaded' | 'registered' | 'signed' | 'deleted';
