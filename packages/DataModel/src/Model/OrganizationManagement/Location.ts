import { GeoPoint, Timezone } from '@monki-island/common';

export interface LocationModel {
  localityId: string;
  provinceId: number;
  regionId: number;
  timezone: Timezone;
  location: GeoPoint;
}
