import { CropBoundary, GeoPoint, Nullable, Timezone } from '@monki-island/common';

export interface MuseumModel {
  id: string;
  slug: string;
  organizationId: string;
  name: string;
  description: string;
  type: MuseumTypeModel;
  location: MuseumLocationModel;
  category: number[];
  content: MuseumContentModel;
  feedback: MuseumFeedbackModel;
  workingSchedule: MuseumWorkingScheduleModel;
  visit: MuseumVisitModel;
  state: MuseumStateModel;
  lastChangeAt: Date;
  createdAt: Date;
  deletedAt: Nullable<Date>;
}

export type MuseumStateModel = 'loaded' | 'created' | 'addition_required' | 'deleted';

export type MuseumTypeModel = 'museum' | 'gallery' | 'exhibition_center' | 'exhibition_hall' | 'park';

export interface MuseumContentModel {
  gallery: MuseumContentGalleryItemModel[];
  previewImage: MuseumContentPreviewImageModel;
}

export interface MuseumContentGalleryItemModel {
  order: number;
  crop: CropBoundary;
  src: string;
}

export interface MuseumContentPreviewImageModel {
  crop: CropBoundary;
  src: string;
}

export interface MuseumFeedbackModel {
  contact: MuseumFeedbackContactModel[];
  website: string;
}

export interface MuseumFeedbackContactModel {
  type: 'refund' | 'feedback' | 'private';
  name: string;
  email: string;
  phone: string;
}

export interface MuseumVisitModel {
  rules: MuseumVisitRulesModel;
  isFree: boolean;
  service: MuseumVisitServiceModel[];
  freeDayAnnounce: string;
  covidRestriction: boolean;
}

export interface MuseumVisitRulesModel {
  source: string;
  lastUpdatedAt: string;
}

export interface MuseumVisitServiceModel {
  name: string;
  slug: string;
  order: Nullable<number>;
}

export interface MuseumWorkingScheduleModel {
  schedule: MuseumWorkingScheduleDayModel[];
  addition: string;
  sanitaryDay: string[];
}

export interface MuseumWorkingScheduleDayModel {
  weekDay: number;
  openingHour: MuseumWorkingScheduleDayOpeningHourModel;
  break: MuseumWorkingScheduleDayBreakModel;
}

export interface MuseumWorkingScheduleDayOpeningHourModel {
  from: string;
  to: string;
}

export interface MuseumWorkingScheduleDayBreakModel {
  from: string;
  to: string;
}

export interface MuseumLocationModel {
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  address: string;
  location: GeoPoint;
  timezone: Timezone;
  subjectCenter: boolean;
}
