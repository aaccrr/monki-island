import { EntranceTicketModel } from './EntranceTicket';
import { EventPositionModel } from './EventPosition';
import { SeasonTicketModel } from './SeasonTicket';

export type NomenclatureModel = EntranceTicketModel | SeasonTicketModel | EventPositionModel;
