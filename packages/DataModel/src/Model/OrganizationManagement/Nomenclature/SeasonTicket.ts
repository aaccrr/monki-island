import { Nullable, Timezone } from '@monki-island/common';
import { ClientCategoryModel } from '..';
import { VisitConditionModel } from '../VisitCondition';
import { VisitorsCategoryModel } from '../VisitorsCategory';

export interface SeasonTicketModel {
  id: string;
  publicId: string;
  organizationId: string;
  museumId: string;
  title: string;
  type: 'season_ticket';
  clientCategory: ClientCategoryModel[];
  saleSchedule: SeasonTicketSaleScheduleModel;
  condition: SeasonTicketConditionModel;
  location: SeasonTicketLocationModel;
  state: SeasonTicketStateModel;
  visitConditions: VisitConditionModel[];
  visitorsCategories: VisitorsCategoryModel[];
  lastChangedAt: Date;
  createdAt: Date;
  deletedAt: Nullable<Date>;
}

export interface SeasonTicketSaleScheduleModel {
  state: 'opened' | 'closed';
  limit: SeasonTicketSaleScheduleLimitModel;
  suspendScheduledDate: Nullable<Date>;
}

export interface SeasonTicketSaleScheduleLimitModel {
  online: Nullable<number>;
  offline: Nullable<number>;
}

export interface SeasonTicketConditionModel {
  perOrderLimit: number;
  ageCensor: SeasonTicketConditionAgeCensorModel;
  visitLimitNumber: Nullable<number>;
  validityPeriod: Nullable<number>;
  event: SeasonTicketConditionEventModel[];
}

export interface SeasonTicketConditionEventModel {
  name: string;
  start: string;
}

export type SeasonTicketConditionAgeCensorModel = 0 | 6 | 12 | 16 | 18;

export type SeasonTicketStateModel = 'created' | 'canceled' | 'deleted';

export interface SeasonTicketLocationModel {
  localityId: string;
  provinceId: number;
  regionId: number;
  timezone: Timezone;
}
