import { ClientCategoryModel } from '../ClientCategory';
import { EventScheduleDateIntervalModel, EventScheduleDateTimelinePointModel } from '../Event/EventSchedule';
import { LocationModel } from '../Location';
import { EventLocationModel } from '..';
import { VisitorsCategoryModel } from '../VisitorsCategory';
import { VisitConditionModel } from '../VisitCondition';

export interface EventPositionModel {
  id: string;
  legacyPositionId: number;
  publicId: string;
  organizationId: string;
  museumId: string;
  type: 'event';
  title: string;
  schedule: EventPositionScheduleModel;
  saleSchedule: EventPositionSaleScheduleModel;
  clientCategory: ClientCategoryModel[];
  location: EventLocationModel[];
  sellerLocation: LocationModel;
  pushkinCardProgram: boolean;
  visitConditions: VisitConditionModel[];
  visitorsCategories: VisitorsCategoryModel[];
  createdAt: Date;
  lastChangedAt: Date;
  deletedAt: Date;
}

export interface EventPositionSaleScheduleModel {
  start: string;
}

export type EventPositionScheduleModel =
  | EventPositionScheduleDateModel
  | EventPositionScheduleLongPeriodModel
  | EventPositionScheduleUnionTicketModel;

export interface EventPositionScheduleLongPeriodModel {
  type: 'long_period';
  start: string;
  finish: string;
}

export interface EventPositionScheduleUnionTicketModel {
  type: 'union_ticket';
  include: EventPositionScheduleUnionTicketIncludeModel[];
}

export interface EventPositionScheduleUnionTicketIncludeModel {
  date: string;
  interval: EventScheduleDateIntervalModel[];
  timeline: EventScheduleDateTimelinePointModel[];
}

export interface EventPositionScheduleDateModel {
  type: 'date';
  date: string;
  interval: EventScheduleDateIntervalModel[];
  timeline: EventScheduleDateTimelinePointModel[];
}

export interface EventPositionIdPayload {
  type: 'date' | 'union_ticket' | 'long_period';
  date: string[];
}
