import * as Common from '@monki-island/common';
import { ClientCategoryModel } from '../ClientCategory';
import { LocationModel } from '../Location';
import { VisitConditionModel } from '../VisitCondition';
import { VisitorsCategoryModel } from '../VisitorsCategory';

export interface EntranceTicketModel {
  id: string;
  publicId: string;
  organizationId: string;
  museumId: string;
  title: string;
  type: 'ticket';
  visit: TicketVisitModel;
  clientCategory: ClientCategoryModel[];
  condition: TicketConditionModel;
  saleSchedule: TicketSaleScheduleModel;
  location: LocationModel;
  state: TicketStateEnum;
  visitConditions: VisitConditionModel[];
  visitorsCategories: VisitorsCategoryModel[];
  lastChangedAt: Date;
  createdAt: Date;
  deletedAt: Common.Nullable<Date>;
}

export type TicketStateEnum = 'created' | 'suspended' | 'deleted';

export interface TicketVisitModel {
  includedInPrice: string[];
}

export interface TicketSaleScheduleModel {
  state: TicketSaleScheduleStateModel;
  type: TicketSaleScheduleTypeModel;
  toTime: TicketSaleScheduleToTimeItemModel[];
  expiration?: Common.Nullable<Date>;
  dayOffSales: boolean;
  suspendedAt: Common.Nullable<Date>;
}

export type TicketSaleScheduleTypeModel = 'none' | 'to_time';

export type TicketSaleScheduleStateModel = 'opened' | 'suspended';

export interface TicketSaleScheduleToTimeItemModel {
  date: string;
  interval: TicketSaleScheduleToTimeItemIntervalModel[];
}

export interface TicketSaleScheduleToTimeItemIntervalModel {
  from: string;
  to: string;
  online: Common.Nullable<number>;
  offline: Common.Nullable<number>;
}

export interface TicketConditionModel {
  visitLimitNumber: number;
  ageCensor: TicketConditionAgeCensorModel;
  canBeTransferred: boolean;
  isPersonal: boolean;
  validityPeriod: number;
  perOrderLimit: number;
}

export type TicketConditionAgeCensorModel = 0 | 6 | 12 | 16 | 18;
