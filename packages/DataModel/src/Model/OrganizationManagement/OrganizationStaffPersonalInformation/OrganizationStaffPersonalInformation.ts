export interface OrganizationStaffPersonalInformationModel {
  lastName: string;
  firstName: string;
  patronymic: string;
  email: string;
  phone: string;
}
