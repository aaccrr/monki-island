export interface ClientCategoryModel {
  id: number;
  name: string;
  price: number;
  group: string[];
}
