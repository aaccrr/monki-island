import { IntervalModel } from './Interval';

export interface MultipleIntervalsModel {
  intervals: IntervalModel[];
}
