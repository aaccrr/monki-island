export interface VisitsCountModel {
  visits: number;
  period: number;
}
