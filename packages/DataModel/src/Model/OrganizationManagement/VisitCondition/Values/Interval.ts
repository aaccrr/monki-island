export interface IntervalLimitModel {
  date: string;
  time: string;
}

export interface IntervalModel {
  from: IntervalLimitModel;
  to: IntervalLimitModel;
}
