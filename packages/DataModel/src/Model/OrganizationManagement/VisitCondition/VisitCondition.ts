import { VisitConditionValueModel } from './VisitConditionValue';
import { VisitConditionTypeEnum } from './VisitConditionType';
import { VisitConditionLimitModel } from './VisitConditionLimit';

export interface VisitConditionModel {
  id: string;
  type: VisitConditionTypeEnum;
  value: VisitConditionValueModel;
  limit: VisitConditionLimitModel;
}
