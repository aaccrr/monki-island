import { Nullable } from '@monki-island/common';

export type VisitConditionLimitModel = Nullable<number>;
