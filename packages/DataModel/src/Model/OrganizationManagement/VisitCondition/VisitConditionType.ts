export enum VisitConditionTypeEnum {
  Interval = 'interval',
  MultipleIntervals = 'multiple_intervals',
  LongInterval = 'long_interval',
  VisitsCount = 'visits_count',
  ValidityPeriod = 'validity_period',
}
