import { IntervalModel } from './Values/Interval';
import { MultipleIntervalsModel } from './Values/MultipleIntervals';
import { ValidityPeriodModel } from './Values/ValidityPeriod';
import { VisitsCountModel } from './Values/VisitsCount';

export type VisitConditionValueModel = IntervalModel | MultipleIntervalsModel | ValidityPeriodModel | VisitsCountModel;
