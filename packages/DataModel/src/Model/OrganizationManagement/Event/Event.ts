import { CropBoundary, GeoPoint, Timezone, Nullable } from '@monki-island/common';
import { LocationModel } from '../Location';
import { EventScheduleModel } from './EventSchedule';

export interface EventModel {
  id: string;
  slug: string;
  organizationId: string;
  museumId: string;
  title: string;
  description: string;
  category: EventCategoryModel;
  clientCategory: EventClientCategoryModel[];
  content: EventContentModel;
  condition: EventConditionModel;
  schedule: EventScheduleModel;
  organizator: EventOrganizatorModel;
  integration: EventIntegrationModel;
  location: EventLocationModel[];
  state: EventStatusEnum;
  salesStart?: Date;
  lastChangedAt: Date;
  createdAt: Date;
  deletedAt: Nullable<Date>;
}

export type EventStatusEnum = 'created' | 'deleted' | 'canceled' | 'finished' | 'requires_addition';

export interface EventLocationModel extends LocationModel {
  id: number;
}

export interface EventCategoryModel {
  custom: boolean;
  id: Nullable<number>;
  name: Nullable<string>;
}

export interface EventOrganizatorModel {
  contact: EventOrganizatorContactModel[];
}

export interface EventOrganizatorContactModel {
  type: 'refund' | 'feedback' | 'private';
  name: string;
  email: string;
  phone: string;
}

export interface EventIntegrationModel {
  proCultureId: Nullable<number>;
}

export interface EventClientCategoryModel {
  id: number;
  name: string;
  price: EventClientCategoryPriceModel[];
  group: string[];
}

export interface EventClientCategoryPriceModel {
  ticketType: string | 'union' | 'long_period';
  online: number;
  offline: number;
}

export interface EventContentModel {
  gallery: EventContentGalleryItemModel[];
  previewImage: EventContentPreviewImageModel;
}

export interface EventContentGalleryItemModel {
  order: number;
  crop: CropBoundary;
  src: string;
}

export interface EventContentPreviewImageModel {
  crop: CropBoundary;
  src: string;
}

export interface EventConditionModel {
  perOrderLimit: number;
  ageCensor: EventConditionAgeCensorModel;
}

export type EventConditionAgeCensorModel = 0 | 6 | 12 | 16 | 18;
