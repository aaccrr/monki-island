import { Nullable } from '@monki-island/common';

export interface EventScheduleModel {
  type: EventScheduleTypeModel;
  day: EventScheduleDateModel[];
  longPeriod: Nullable<EventScheduleLongPeriodModel>;
  unionTicket: Nullable<EventScheduleUnionTicketModel>;
  start?: Date;
  completion?: Date;
}

export type EventScheduleTypeModel = 'daily' | 'long_period' | 'interval';

export interface EventScheduleLongPeriodModel {
  legacyPositionId: number;
  start: string;
  finish: string;
  saleStart: string;
  location: number;
}

export interface EventScheduleDateModel {
  legacyPositionId: number;
  date: string;
  isFree: boolean;
  atNight: boolean;
  interval: EventScheduleDateIntervalModel[];
  timeline: EventScheduleDateTimelinePointModel[];
}

export interface EventTicketSaleModel {
  saleStart: string;
  online: Nullable<number>;
  offline: Nullable<number>;
}

export interface EventScheduleDateIntervalModel extends EventTicketSaleModel {
  location: number;
  from: string;
  to: string;
}

export interface EventScheduleUnionTicketModel extends EventTicketSaleModel {
  legacyPositionId: number;
  include: string[];
}

export interface EventScheduleDateTimelinePointModel {
  from: string;
  to: string;
  description: string;
}
