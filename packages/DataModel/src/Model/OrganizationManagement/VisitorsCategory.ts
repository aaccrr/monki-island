export interface VisitorsCategoryModel {
  legacyId: number;
  id: string;
  name: string;
  price: number;
  groups: string[];
}
