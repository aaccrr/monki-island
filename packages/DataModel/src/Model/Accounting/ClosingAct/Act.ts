import * as Contragent from '../Contragent';
import { Period } from '../Period';
import { Reports } from './Reports';
import { Assets } from './Assets';
import { Documents } from './Documents';
import { Stage } from './Stage';

export interface Act {
  _id: string;
  contragentId: string;
  period: Period;
  reports?: Reports;
  assets?: Assets;
  requisites?: Contragent.Requisites;
  documents?: Documents;
  stage?: Stage;
}
