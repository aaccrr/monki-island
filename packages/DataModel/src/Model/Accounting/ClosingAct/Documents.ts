import * as Common from '@monki-island/common';
import * as Document from '../Document';

export interface Documents {
  gratuitousBasis: Common.Nullable<Document.Closing>;
  pushkinCard: Common.Nullable<Document.Closing>;
}
