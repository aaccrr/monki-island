import * as Report from '../Report';

export interface Reports {
  gratuitousBasis: Report.GratuitousBasis.Report;
  pushkinCard: Report.PushkinCard.Report;
}
