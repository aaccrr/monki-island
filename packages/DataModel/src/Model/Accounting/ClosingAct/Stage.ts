export enum Stage {
  Prepared = 'prepared',
  Skipped = 'skipped',
  ReportsCalculated = 'reports_calculated',
  AssetsFetched = 'assets_fetched',
  RequisitesFetched = 'requisites_fetched',
  DocumentsPrinted = 'documents_printed',
  DocumentsSended = 'documents_sended',
}
