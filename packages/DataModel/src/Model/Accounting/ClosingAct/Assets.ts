import * as Asset from '../Asset';

export interface Assets {
  visitObjects: Asset.VisitObject[];
  nomenclature: Asset.Nomenclature[];
  visitorsCategories: Asset.VisitorsCategory[];
}
