export interface VisitObject {
  id: string;
  name: string;
}
