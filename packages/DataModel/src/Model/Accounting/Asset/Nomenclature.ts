export interface Nomenclature {
  id: string;
  type: 'entrance_ticket' | 'season_ticket' | 'event';
  title: string;
  labels?: {
    type: 'long_period' | 'daily';
    dates: string[];
  };
}
