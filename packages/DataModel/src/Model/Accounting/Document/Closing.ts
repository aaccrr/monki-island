import * as Contragent from '../Contragent';
import { Period } from '../Period';

export interface Closing {
  contragentId: string;
  period: Period;
  contract: Contragent.Contract.Type;
}
