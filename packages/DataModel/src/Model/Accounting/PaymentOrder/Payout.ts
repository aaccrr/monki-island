import * as Contragent from '../Contragent';
import { Period } from '../Period';
import { Amount } from './Amount';

export interface Payout {
  period: Period;
  requisites: Contragent.Requisites;
  amount: Amount;
}
