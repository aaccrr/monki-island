export interface Payer {
  inn: string;
  name: string;
  checkingAccount: string;
  correspondentAccount: string;
  kpp: string;
  bik: string;
  bank: string;
  bankLocality: string;
}
