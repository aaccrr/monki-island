export interface Recipient {
  name: string;
  inn: string;
  kpp: string;
  checkingAccount: string;
  correspondentAccount: string;
  bik: string;
  bank: string;
  bankLocality: string;
  subsidyCode: string;
  status: string;
  kbk: string;
  okato: string;
}
