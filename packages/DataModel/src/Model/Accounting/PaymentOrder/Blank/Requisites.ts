import { Payer } from './Payer';
import { Recipient } from './Recipient';

export interface Requisites {
  number: string;
  date: string;
  amount: string;
  payer: Payer;
  recipient: Recipient;
  purpose: string;
}
