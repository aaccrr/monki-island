export interface Amount {
  contragentId: string;
  fee: number;
  pushkinCardAmount: number;
  gratuitousBasisAmount: number;
}
