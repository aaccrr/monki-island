export interface Sheet {
  contragentId: string;
  name: string;
  inn: string;
  pushkinCardProfit: number;
  pushkinCardFee: number;
  gratuitousBasisProfit: number;
  gratuitousBasisServiceCharge: number;
  retention: boolean;
}
