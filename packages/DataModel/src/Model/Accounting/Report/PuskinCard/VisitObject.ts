import { Indicators } from '../Indicators';

export interface VisitObject {
  id: string;
  indicators: Indicators;
  refundsFee: number;
  fee: number;
}
