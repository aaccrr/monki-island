import { VisitObject } from './VisitObject';
import { Nomenclature } from './Nomenclature';
import { VisitorsCategory } from './VisitorsCategory';

export interface Report {
  fee: number;
  profit: number;
  visitObjects: VisitObject[];
  nomenclature: Nomenclature[];
  visitorsCategories: VisitorsCategory[];
}
