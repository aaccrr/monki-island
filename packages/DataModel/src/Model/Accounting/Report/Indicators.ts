export interface Indicators {
  sale: number;
  refund: number;
  profit: number;
}
