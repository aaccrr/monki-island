import { Indicators } from '../Indicators';

export interface Nomenclature {
  id: string;
  visitObjectId: string;
  indicators: Indicators;
}
