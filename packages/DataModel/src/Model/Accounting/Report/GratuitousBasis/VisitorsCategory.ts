import { Indicators } from '../Indicators';

export interface VisitorsCategory {
  id: string;
  nomenclatureId: string;
  indicators: Indicators;
}
