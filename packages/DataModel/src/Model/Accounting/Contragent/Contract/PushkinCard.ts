export interface PushkinCard {
  number: string;
  date: string;
  feeRetention: boolean;
}
