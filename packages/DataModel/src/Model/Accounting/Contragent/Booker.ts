export interface Booker {
  _id: string;
  firstName: string;
  lastName: string;
  patronymic: string;
  email: string;
  phone: string;
}
