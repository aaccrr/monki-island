import { Contracts } from './Contracts';
import { OwnershipType } from './OwnershipType';

export interface Requisites {
  _id: string;
  name: string;
  ownershipType: OwnershipType;
  inn: string;
  kpp: string;
  ogrn: string;
  oktmo: string;
  okpo: string;
  checkingAccount: string;
  bank: string;
  bik: string;
  correspondentAccount: string;
  paymentsRecipient: string;
  kbk: string;
  signer: string;
  mailingAddress: string;
  contracts: Contracts;
  subsidyCode: string;
}
