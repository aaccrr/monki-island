import * as Contract from './Contract';

export interface Contracts {
  gratuitousBasis: Contract.GratuitousBasis;
  pushkinCard: Contract.PushkinCard;
}
