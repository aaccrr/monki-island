export enum OwnershipType {
  State = 'state',
  Private = 'private',
}
