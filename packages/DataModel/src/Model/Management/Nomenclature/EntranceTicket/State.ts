export enum State {
  Created = 'created',
  Suspended = 'suspended',
  Deleted = 'deleted',
}
