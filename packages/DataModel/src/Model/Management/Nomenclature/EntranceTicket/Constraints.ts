export interface Constraints {
  visits: number;
  age: number;
  transfer: boolean;
  personal: boolean;
  validityPeriod: number;
  perOrder: number;
  dayOffSales: boolean;
}
