import * as Geography from '../../../Geography';
import * as VisitCondition from '../../VisitCondition';
import * as VisitorsCategory from '../../VisitorsCategory';
import { Constraints } from './Constraints';
import { State } from './State';

export interface EntranceTicket {
  id: string;
  label: string;
  organizationId: string;
  visitObjectId: string;
  title: string;
  visitorsCategories: VisitorsCategory.VisitorsCategory[];
  schedule: VisitCondition.VisitCondition[];
  overviewList: string[];
  constraints: Constraints;
  place: Geography.Place;
  state: State;
}
