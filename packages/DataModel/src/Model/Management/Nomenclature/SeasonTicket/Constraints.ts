import * as Common from '@monki-island/common';

export interface Constraints {
  perOrder: number;
  age: number;
  visits: Common.Nullable<number>;
  validityPeriod: Common.Nullable<number>;
  salesPeriod: Common.Nullable<Date>;
  sales: Common.Nullable<number>;
}
