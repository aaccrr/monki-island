export enum State {
  Created = 'created',
  Deleted = 'deleted',
  Canceled = 'canceled',
  Finished = 'finished',
  RequiresAddition = 'requires_addition',
}
