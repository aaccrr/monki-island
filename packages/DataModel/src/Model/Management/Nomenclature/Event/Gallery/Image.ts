import { Type } from './Type';

export interface Image {
  type: Type;
  src: string;
  crop: any;
  order?: number;
}
