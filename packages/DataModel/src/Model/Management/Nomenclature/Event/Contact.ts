export interface Contact {
  type: 'refund' | 'feedback' | 'private';
  name: string;
  email: string;
  phone: string;
}
