import * as VisitCondition from '../../../VisitCondition';
import * as VisitorsCategory from '../../../VisitorsCategory';
import { Type } from './Type';

export interface Item {
  id: string;
  type: Type;
  visitConditions: VisitCondition.VisitCondition[];
  visitorsCategories: VisitorsCategory.VisitorsCategory[];
  salesStart: string;
}
