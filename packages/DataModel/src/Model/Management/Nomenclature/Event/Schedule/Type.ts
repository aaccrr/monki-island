export enum Type {
  Day = 'day',
  LongPeriod = 'long_period',
  UnionTicket = 'union_ticket',
}
