import * as Schedule from './Schedule';
import * as Gallery from './Gallery';
import { Category } from './Category';
import { Contact } from './Contact';
import { Integrations } from './Integrations';
import { Constraints } from './Constraints';
import { State } from './State';

export interface Event {
  id: string;
  slug: string;
  organizationId: string;
  visitObjectId: string;
  categories: Category[];
  title: string;
  description: string;
  gallery: Gallery.Image[];
  schedule: Schedule.Item[];
  contacts: Contact[];
  constraints: Constraints;
  integration: Integrations;
  state: State;
}
