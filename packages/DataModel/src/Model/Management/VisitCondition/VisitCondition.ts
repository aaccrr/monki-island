import * as Value from './Value';
import { Limit } from './Limit';
import { Type } from './Type';

export interface VisitCondition {
  id: string;
  type: Type;
  value: Value.Interval | Value.MultiInterval | Value.Period | Value.Visits;
  limit: Limit;
}
