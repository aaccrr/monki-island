import * as Common from '@monki-island/common';

export type Limit = Common.Nullable<number>;
