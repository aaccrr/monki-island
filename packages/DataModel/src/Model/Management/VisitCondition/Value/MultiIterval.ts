import { Interval } from './Interval';

export type MultiInterval = Interval[];
