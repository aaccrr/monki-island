export interface VisitorsCategory {
  id: string;
  name: string;
  price: number;
  groups: string[];
}
