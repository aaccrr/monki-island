export interface File {
  hash: string;
  ext: string;
}
