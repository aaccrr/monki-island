export enum Type {
  PreviewImage = 'PreviewImage',
  GalleryImage = 'GalleryImage',
  Avatar = 'Avatar',
  Rules = 'Rules',
  RegistrationCertificate = 'RegistrationCertificate',
  TaxRegistrationCertificate = 'TaxRegistrationCertificate',
  ManagerAppointmentCertificate = 'ManagerAppointmentCertificate',
  CreationCertificate = 'CreationCertificate',
  ExtractFromEgrul = 'ExtractFromEgrul',
}
