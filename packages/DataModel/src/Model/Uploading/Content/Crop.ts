export interface Crop {
  left: number;
  top: number;
  width: number;
  height: number;
}
