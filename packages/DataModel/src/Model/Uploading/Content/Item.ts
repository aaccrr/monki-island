import { Crop } from './Crop';
import { File } from './File';

export interface Item {
  id: string;
  key: string;
  namespace: string;
  type: string;
  file: File;
  storage: 'local' | 's3';
  crop?: Crop;
  order?: number;
}
