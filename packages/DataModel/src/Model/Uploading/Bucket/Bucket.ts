import { Type } from './Type';

export interface Bucket {
  namespace: string;
  type: Type;
  owner: string;
}
