export enum Type {
  Account = 'account',
  Contragent = 'organization',
  Event = 'event',
  VisitObject = 'museum',
  Employee = 'employee',
}
