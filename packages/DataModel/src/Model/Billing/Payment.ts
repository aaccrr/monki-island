export interface PaymentModel {
  id: string;
  rnn: string;
  date: Date;
}
