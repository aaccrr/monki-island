export interface Price {
  id: string;
  legacyId: number;
  price: number;
  category: string;
  groups: string[];
}
