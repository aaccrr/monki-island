export interface Constraints {
  personal: boolean;
  transfer: boolean;
  age: number;
  perOrder: number;
}
