import { Price } from './Price';
import { Visibility } from './Visibility';
import { Type } from './Type';
import { Constraints } from './Constraints';

export interface ProductModel {
  _id: string;
  showcaseId: string;
  serviceProviderId: string;
  visitObjectId: string;
  title: string;
  type: Type;
  prices: Price[];
  constraints: Constraints;
  visibility: Visibility;
  score: number;
  schedule: boolean;
}
