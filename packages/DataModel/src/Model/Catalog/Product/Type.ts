export enum Type {
  EntranceTicket = 'entrance_ticket',
  SeasonTicket = 'season_ticket',
  EventPosition = 'event_position',
}
