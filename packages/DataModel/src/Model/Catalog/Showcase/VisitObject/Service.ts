export interface Service {
  name: string;
  slug: string;
  order: number;
}
