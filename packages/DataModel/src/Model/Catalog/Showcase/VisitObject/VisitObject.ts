import { Showcase } from '../Showcase';
import { Service } from './Service';
import { WorkingSchedule } from './WorkingSchedule';
import { Constraints } from './Constraints';

export interface VisitObject extends Showcase {
  constraints: Constraints;
  services: Service[];
  workingSchedule: WorkingSchedule;
  freeVisit: string;
  rules: string;
}
