export interface WorkingSchedule {
  daily: WorkingScheduleDay[];
  comment: string;
  sanitaryDays: string[];
}

export interface WorkingScheduleDay {
  weekDay: number;
  opening: [string, string];
  break: [string, string];
}
