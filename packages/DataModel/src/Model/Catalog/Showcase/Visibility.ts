export enum Visibility {
  Visible = 'visible',
  VisibleByLink = 'visible_by_link',
  Hidden = 'hidden',
}
