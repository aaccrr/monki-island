import { Contact } from './Contact';
import { Visibility } from './Visibility';
import { Place } from './Place';

export interface Showcase {
  id: string;
  slug: string;
  serviceProviderId: string;
  visitObjectId: string;
  title: string;
  description: string;
  gallery: string[];
  contacts: Contact[];
  visibility: Visibility;
  places: Place[];
}
