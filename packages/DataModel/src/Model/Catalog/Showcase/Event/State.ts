export enum State {
  Upcoming = 'upcoming',
  Ongoing = 'ongoing',
  Finished = 'finished',
}
