import { Showcase } from '../Showcase';
import { Category } from './Category';
import { Integrations } from './Integrations';
import { State } from './State';
import { Constraints } from './Constraints';

export interface Event extends Showcase {
  constraints: Constraints;
  category: Category;
  integrations: Integrations;
  state: State;
}
