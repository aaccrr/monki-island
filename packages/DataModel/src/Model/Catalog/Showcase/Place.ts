import { Timezone } from '@monki-island/common';

export interface Place {
  id: string;
  localityId: string;
  provinceId: number;
  regionId: number;
  timezone: Timezone;
  location: string;
}
