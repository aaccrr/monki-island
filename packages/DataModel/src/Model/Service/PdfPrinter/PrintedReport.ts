export interface PrintedReport {
  type: 'counterfoils_sheet' | 'main_closing_report' | 'closing_report_by_pushkin_card' | 'sales_report' | 'sales_detalization';
  publicUrl: string;
  internalPath: string;
}
