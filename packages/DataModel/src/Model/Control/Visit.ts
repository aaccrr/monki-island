export interface VisitModel {
  entranceTicketId: string;
  timestamp: number;
}
