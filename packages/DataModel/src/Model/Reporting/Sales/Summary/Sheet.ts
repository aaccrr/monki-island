export interface Sheet {
  nomenclatureType: string;
  salesTotal: number;
  profitTotal: number;
}
