import * as Report from './Report';
import * as Detalization from './Detalization';
import * as Summary from './Summary';

export { Report, Detalization, Summary };
