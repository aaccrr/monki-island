import * as Subjects from './Subjects';
import { ReportingPeriod } from '../../ReportingPeriod';

export interface Sheet {
  reportingPeriod: ReportingPeriod;
  visitObjects: Subjects.VisitObject[];
  totals: [number, number, number, number];
}
