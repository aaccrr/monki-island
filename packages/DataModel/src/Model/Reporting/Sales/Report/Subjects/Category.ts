export interface Category {
  title: string;
  report: [number, number, number, number];
}
