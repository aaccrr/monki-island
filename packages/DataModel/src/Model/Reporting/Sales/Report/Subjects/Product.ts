import { Category } from './Category';

export interface Product {
  id: string;
  title: string;
  report: [number, number, number, number];
  detalization: boolean;
  categories: Category[];
}
