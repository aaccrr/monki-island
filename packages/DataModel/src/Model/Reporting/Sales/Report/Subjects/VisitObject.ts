import { Product } from './Product';

export interface VisitObject {
  id: string;
  title: string;
  report: [number, number, number, number];
  tickets: Product[];
  seasonTickets: Product[];
  events: Product[];
}
