import { Product } from './Product';
import { ReportingPeriod } from '../../ReportingPeriod';

export interface Sheet {
  product: Product;
  visitPeriod: ReportingPeriod;
}
