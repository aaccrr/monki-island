import { Condition } from './Condition';

export interface Product {
  id: string;
  title: string;
  visitObjectId: string;
  report: [number, number, number, number];
  conditions: Condition[];
}
