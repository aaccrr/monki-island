import { Time } from './Time';

export interface Condition {
  date: string[];
  report: [number, number, number, number];
  times: Time[];
}
