import { Counterfoil } from '../Counterfoil';

export interface Product {
  title: string;
  type: string;
  counterfoils: Counterfoil[];
}
