import { Product } from './Product';

export interface VisitObject {
  name: string;
  products: Product[];
}
