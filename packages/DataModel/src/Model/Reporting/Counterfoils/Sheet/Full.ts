import { ReportingPeriod } from '../../ReportingPeriod';
import { VisitObject } from './VisitObject';

export interface Full {
  reportingPeriod: ReportingPeriod;
  visitObjects: VisitObject[];
  profitTotal: number;
  refundsAmountTotal: number;
}
