export interface Counterfoil {
  ticketNumber: string;
  refunded: boolean;
  price: number;
}
