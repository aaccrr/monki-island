import * as ReportEntity from '../../ReportEntity';

export interface Detailed {
  branchOffices: Partial<ReportEntity.BranchOffice>[];
}
