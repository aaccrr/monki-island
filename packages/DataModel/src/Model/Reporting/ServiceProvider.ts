import { Timezone } from '@monki-island/common';

export interface ServiceProvider {
  _id: string;
  name: string;
  identifier: ContragentIdentifierModel;
  paymentDetail: ContragentPaymentDetailModel;
  signer: ContragentSignerModel;
  address: ContragentAddressModel;
  location: ContragentLocationModel;
  contract: ContragentContractModel;
  contact: ContragentContactModel;
  pushkinCardContract: ContragentContractModel;
}

export interface ContragentIdentifierModel {
  inn: string;
  kpp: string;
  ogrn: string;
}

export interface ContragentPaymentDetailModel {
  checkingAccount: string;
  name: string;
  bik: string;
  correspondentAccount: string;
  paymentRecipient: string;
  kbk: string;
}

export interface ContragentAddressModel {
  mailing: string;
}

export interface ContragentSignerModel {
  fio: string;
}

export interface ContragentLocationModel {
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  timezone: Timezone;
}

export interface ContragentContractModel {
  number: string;
  signDate: string;
}

export interface ContragentContactModel {
  email: string;
  phone: string[];
}
