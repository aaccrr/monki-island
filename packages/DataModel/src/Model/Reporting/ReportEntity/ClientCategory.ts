import { Turnover } from './Turnover';

export interface ClientCategory extends Turnover {
  id: string;
  name: string;
  price: number;
}
