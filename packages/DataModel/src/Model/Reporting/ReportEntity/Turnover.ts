export interface Turnover {
  sale: number;
  passage: number;
  refund: number;
  profit: number;
  refundAmount: number;
}
