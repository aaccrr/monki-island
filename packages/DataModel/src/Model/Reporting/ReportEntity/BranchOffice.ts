import { Nomenclature } from './Nomenclature';
import { Turnover } from './Turnover';

export interface BranchOffice extends Turnover {
  id: string;
  name: string;
  tickets: Nomenclature[];
  seasonTickets: Nomenclature[];
  events: Nomenclature[];
  compensationForRefundsTotal: number;
  feeTotal: number;
}
