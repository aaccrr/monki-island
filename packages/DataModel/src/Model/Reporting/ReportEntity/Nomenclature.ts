import { ClientCategory } from './ClientCategory';
import { Turnover } from './Turnover';

export interface Nomenclature extends Turnover {
  id: string;
  title: string;
  clientCategory: ClientCategory[];
}
