import { Nomenclature } from './Nomenclature';

export interface VisitObject {
  id: string;
  name: string;
  tickets: Nomenclature[];
  seasonTickets: Nomenclature[];
  events: Nomenclature[];
  report: [number, number, number, number, number];
  compensationForRefundsTotal: number;
  feeTotal: number;
}
