export interface Category {
  id: string;
  name: string;
  price: number;
  report: [number, number, number, number, number];
}
