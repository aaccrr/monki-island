import { Category } from './Category';

// report: [sales, refunds, passages, profitTotal, refundsAmountTotal]

export interface Nomenclature {
  id: string;
  title: string;
  categories: Category[];
  report: [number, number, number, number, number];
}
