import * as Sheet from '../Sheet';

export interface PushkinCardProgramRealization {
  visitObjects: Sheet.PerformedWork.VisitObject[];
  profitTotal: number;
  feeTotal: number;
}
