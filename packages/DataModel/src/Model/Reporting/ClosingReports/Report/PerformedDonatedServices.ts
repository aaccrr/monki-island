import * as Sheet from '../Sheet';

export interface PerformedDonatedServices {
  visitObjects: Sheet.PerformedWork.VisitObject[];
  profitTotal: number;
}
