export interface ReportingPeriod {
  from: Date;
  to: Date;
}
