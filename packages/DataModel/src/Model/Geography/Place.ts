import * as Common from '@monki-island/common';

export interface Place {
  id: string;
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  address: string;
  geohash: string;
  timezone: Common.Timezone;
}
