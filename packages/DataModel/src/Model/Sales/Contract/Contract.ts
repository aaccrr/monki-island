import { State } from './State';

export interface Contract {
  _id: string;
  name: string;
  inn: string;
  state: State;
}
