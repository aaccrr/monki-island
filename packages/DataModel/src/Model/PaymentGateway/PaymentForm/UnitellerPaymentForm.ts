export interface UnitellerPaymentForm {
  paymentPlatformType: 'uniteller';
  shopId: string;
  orderId: string;
  amount: string;
  orderLifetime: string;
  meanType: string;
  lifeTime: string;
  email: string;
  successfulPaymentUrl: string;
  failedPaymentUrl: string;
  callbackFormat: string;
  callbackFields: string[];
  signature: string;
}
