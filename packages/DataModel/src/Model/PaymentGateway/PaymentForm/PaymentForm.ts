import { PaykeeperPaymentForm } from './PaykeeperPaymentForm';
import { UnitellerPaymentForm } from './UnitellerPaymentForm';

export type PaymentForm = PaykeeperPaymentForm | UnitellerPaymentForm;
