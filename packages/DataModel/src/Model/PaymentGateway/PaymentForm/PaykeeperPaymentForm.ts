export interface PaykeeperPaymentForm {
  paymentPlatformType: 'paykeeper';
  id: string;
  amount: string;
  clientId: string;
  clientEmail: string;
  clientPhone: string;
  sign: string;
}
