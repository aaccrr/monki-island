import { PaginationInterface } from '../Typing';
import { DEFAUL_PAGINATION_LIMIT, MIN_PAGINATION_OFFSET } from '../Constant';

export function RequestQueryPagination(requestQuery: any, defaultPagination: Partial<PaginationInterface> = {}): PaginationInterface {
  if (typeof requestQuery.limit !== 'number') {
    requestQuery.limit = defaultPagination.limit || DEFAUL_PAGINATION_LIMIT;
  }

  if (typeof requestQuery.offset !== 'number') {
    requestQuery.offset = defaultPagination.offset || MIN_PAGINATION_OFFSET;
  }

  return {
    limit: parseInt(requestQuery.limit),
    offset: parseInt(requestQuery.offset),
  };
}
