export interface WithPagination<T> {
  total: number;
  offset: number;
  items: T[];
}
