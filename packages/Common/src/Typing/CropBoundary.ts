export interface CropBoundary {
  width: number;
  height: number;
  x: number;
  y: number;
}
