import { URL } from 'url';

export const DefineStaticHost = (fullUrl: string): string => {
  if (process.env.NODE_ENV === 'production') {
    return fullUrl;
  }

  if (typeof fullUrl !== 'string') {
    return fullUrl;
  }

  if (!fullUrl.startsWith('http')) {
    return fullUrl;
  }

  const url = new URL(fullUrl);

  const staticStorageHost = new URL(process.env.STATIC_STORAGE_HOST);

  if (url.hostname !== staticStorageHost.hostname) {
    return `${process.env.STATIC_STORAGE_HOST}${url.pathname}`;
  }

  return fullUrl;
};
