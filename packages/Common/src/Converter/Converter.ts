import * as xmlToJson from 'xml-js';
import * as showndown from 'showdown';
import { ConverterInterface } from './ConverterInterface';

export class Converter implements ConverterInterface {
  constructor() {
    this.markdownConverter = new showndown.Converter();
  }

  private markdownConverter: showndown.Converter;

  jsonToXml(json: object) {
    const xml = xmlToJson.json2xml(JSON.stringify(json), { compact: true });
    return xml;
  }

  markdownToHtml(markdown: string): string {
    return this.markdownConverter.makeHtml(markdown);
  }
}
