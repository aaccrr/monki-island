export interface ConverterInterface {
  jsonToXml(json: object): any;
  markdownToHtml(markdown: string): string;
}
