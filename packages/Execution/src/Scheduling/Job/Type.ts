export enum Type {
  Singletone = 'singletone',
  Repeated = 'repeated',
}
