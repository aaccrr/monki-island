import * as Agenda from 'agenda';
import * as Instrumentary from '../../Instrumentary';
import * as Observability from '../../Observability';
import { Task } from '../Task';
import { Configuration } from './Configuration';
import { Planner } from '../Planner';
import * as Job from '../Job';

export class Scheduler {
  constructor(private DependencyContainer: Instrumentary.DependencyContainer) {}

  private scheduler: Agenda.Agenda;

  async init(configuration: Configuration): Promise<void> {
    const { dbHost, dbUsername, dbPassword, dbName, dbCollection } = configuration;

    this.scheduler = new Agenda.Agenda({
      db: {
        address: `mongodb://${dbUsername}:${dbPassword}@${dbHost}/${dbName}?authSource=admin`,
        collection: dbCollection,
      },
      defaultConcurrency: 1,
      maxConcurrency: 1,
      defaultLockLifetime: 10000,
    });

    await this.scheduler.start();

    // await this.removeDeadlocks();
  }

  attachFiber(fiber: Instrumentary.FiberConstructor): void {
    this.scheduler.define(fiber.namespace, this.wrapFiber(fiber));
  }

  async schedule(task: Task): Promise<void> {
    const isScheduled = await this.isScheduled(task);

    if (!isScheduled) {
      await this.flushNamespace(task);

      const meta: Job.Meta = {
        type: task.isSingletone ? Job.Type.Singletone : Job.Type.Repeated,
        repeatInterval: task.repeated?.interval,
        repeatBetween: task.repeated?.between,
        key: task.key(),
        namespace: task.namespace(),
      };

      await this.scheduler.schedule(Planner.planSchedule(meta), task.processor, {
        body: task.payload,
        meta,
      });
    }
  }

  async revoke(task: Task): Promise<void> {
    await this.scheduler.cancel({
      name: task.processor,
      'data.meta.key': task.key(),
    });
  }

  protected async flushNamespace(task: Task): Promise<void> {
    await this.scheduler.cancel({
      name: task.processor,
      'data.meta.namespace': task.namespace(),
    });
  }

  protected async flush(task: Task): Promise<void> {
    await this.scheduler.cancel({
      name: task.processor,
      'data.meta.key': task.key(),
    });
  }

  private async isScheduled(task: Task): Promise<boolean> {
    const scheduledJob = await this.scheduler.jobs({
      name: task.processor,
      'data.meta.key': task.key(),
    });

    if (Array.isArray(scheduledJob) && scheduledJob.length > 0) {
      return true;
    }

    return false;
  }

  private wrapFiber(fiber: Instrumentary.FiberConstructor) {
    return async (job: Agenda.Job<any>): Promise<void> => {
      const tracer = this.DependencyContainer.getResource<Observability.Tracing.TracerInterface>(Observability.Tracing.Tracer.name);

      const span = tracer.createNewSpan(`Scheduler("${job.attrs.name}")`);

      try {
        span.addLog('payload', job.attrs.data.body);

        const runner = new fiber(this.DependencyContainer, span.extractContext());

        runner.initTracing();

        runner.initMetricsStore();

        const result = await runner.execute(job.attrs.data.body);

        span.withResponse(result);

        const repeat = Planner.planRepeat(job.attrs.data.meta);

        if (repeat !== null) {
          job.schedule(repeat);
          return;
        }

        job.disable();
      } catch (e) {
        span.withError(e);

        const failsCount = job.attrs.failCount || 0;

        const retry = Planner.planRetry(job.attrs.data.meta, failsCount + 1);

        if (retry !== null) {
          job.schedule(retry);
          return;
        }

        job.disable();
      } finally {
        await job.save();
      }
    };
  }

  private async removeDeadlocks(): Promise<void> {
    const expired = await this.scheduler.jobs({
      lockedAt: {
        $ne: null,
      },
    });

    for (const expiredTask of expired) {
      await expiredTask.touch();
    }
  }
}
