export interface Configuration {
  dbHost: string;
  dbUsername: string;
  dbPassword: string;
  dbName: string;
  dbCollection: string;
}
