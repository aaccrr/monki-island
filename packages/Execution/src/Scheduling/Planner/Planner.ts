import * as Luxon from 'luxon';
import * as Common from '@monki-island/common';
import * as Job from '../Job';

export class Planner {
  static maxFailsCount = 10;

  static delay = 2000;

  static planSchedule(meta: Job.Meta): Date {
    if (meta.type === Job.Type.Singletone) {
      if (typeof meta.excute !== 'undefined') {
        return meta.excute;
      }

      return Luxon.DateTime.now().toJSDate();
    }

    return this.planRepeat(meta);
  }

  static planRetry(meta: Job.Meta, failsCount: number): Common.Nullable<Date> {
    if (meta.type === Job.Type.Singletone) {
      if (failsCount >= Planner.maxFailsCount) {
        return null;
      }

      return Luxon.DateTime.now()
        .plus({ milliseconds: failsCount * Planner.delay })
        .toJSDate();
    }

    return Planner.planRepeat(meta);
  }

  static planRepeat(meta: Job.Meta): Common.Nullable<Date> {
    if (meta.type === Job.Type.Singletone) {
      return null;
    }

    if (typeof meta.repeatInterval === 'number') {
      return Luxon.DateTime.now().plus({ milliseconds: meta.repeatInterval }).toJSDate();
    }

    if (Array.isArray(meta.repeatBetween)) {
      const interval = Planner.millisInBetween(meta.repeatBetween);

      return Luxon.DateTime.now().plus({ milliseconds: interval }).toJSDate();
    }

    return null;
  }

  static millisInBetween(between: [number, number]): number {
    const min = between[0];

    const max = between[1];

    return Math.floor(Math.random() * (max - min) + min);
  }
}
