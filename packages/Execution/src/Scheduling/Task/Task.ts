import * as Crypto from 'crypto';

export abstract class Task<T = any> {
  abstract processor: string;

  singletone: {
    execute?: Date;
  };

  repeated: {
    interval?: number;
    between?: [number, number];
  };

  payload: T;

  get isSingletone(): boolean {
    return typeof this.singletone !== 'undefined';
  }

  get isRepeated(): boolean {
    return typeof this.repeated !== 'undefined';
  }

  namespace(): string {
    const payload = `${this.processor}:${JSON.stringify(this.payload)}`;

    return Crypto.createHash('sha256').update(payload).digest('hex');
  }

  key(): string {
    if (this.isRepeated) {
      return this.namespace();
    }

    const payload = `${this.processor}:${JSON.stringify(this.payload)}:${this.singletone.execute || new Date()}`;

    return Crypto.createHash('sha256').update(payload).digest('hex');
  }
}
