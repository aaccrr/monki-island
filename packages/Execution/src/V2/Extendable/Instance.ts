import * as Tracing from '../Tracing';
import * as Metrics from '../Metrics';
import * as Overlay from '../Overlay';
import * as Scheduling from '../Scheduling';
import * as Storage from '../Storage';
import * as Printing from '../Printing';
import { Interface } from './Interface';
import { Services } from './Services';

export abstract class Instance implements Interface {
  private _tracing: Tracing.Interface;

  private _metrics: Metrics.Interface;

  private _storage: Storage.Interface;

  private _overlay: Overlay.Interface;

  private _scheduling: Scheduling.Interface;

  private _printing: Printing.Interface;

  extend(context: Instance): void {
    const services = context.provide();

    this._tracing = services.tracing;

    this._metrics = services.metrics;

    this._storage = services.storage;

    this._overlay = services.overlay;

    this._scheduling = services.scheduling;

    this._printing = services.printing;
  }

  extendTracing(tracing: Tracing.Interface): void {
    this._tracing = tracing;
  }

  extendMetrics(metrics: Metrics.Interface): void {
    this._metrics = metrics;
  }

  extendStorage(storage: Storage.Interface): void {
    this._storage = storage;
  }

  extendOverlay(overlay: Overlay.Interface): void {
    this._overlay = overlay;
  }

  extendScheduling(scheduling: Scheduling.Interface): void {
    this._scheduling = scheduling;
  }

  extendPrinting(printing: Printing.Interface): void {
    this._printing = printing;
  }

  provide(): Services {
    return {
      tracing: this._tracing,
      metrics: this._metrics,
      storage: this._storage,
      overlay: this._overlay,
      scheduling: this._scheduling,
      printing: this._printing,
    };
  }
}
