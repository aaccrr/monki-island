import * as Tracing from '../Tracing';
import * as Metrics from '../Metrics';
import * as Storage from '../Storage';
import * as Overlay from '../Overlay';
import * as Scheduling from '../Scheduling';
import * as Printing from '../Printing';

export interface Services {
  tracing: Tracing.Interface;
  metrics: Metrics.Interface;
  storage: Storage.Interface;
  overlay: Overlay.Interface;
  scheduling: Scheduling.Interface;
  printing: Printing.Interface;
}
