import * as Extendable from '../Extendable';
import * as Overlay from '../Overlay';
import * as Tracing from '../Tracing';
import * as Storage from '../Storage';
import * as Scheduling from '../Scheduling';
import * as Application from '../Application';
import * as External from '../External';
import * as Printing from '../Printing';
import * as Mailing from '../Mailing';

export abstract class Instance extends Extendable.Instance {
  static namespace = '';

  namespace = '';

  protected trace: Tracing.Span.Interface;

  continue(carrier: Tracing.Carrier.Interface): void {
    const { tracing } = this.provide();

    this.trace = tracing.continue(carrier, this.namespace);
  }

  expose(): Tracing.Carrier.Interface {
    return this.trace.extract();
  }

  protected async execute<R = void>(fiber: Application.Fibers.Interface): Promise<R> {
    (fiber as Application.Fibers.Fiber).extend(this);

    (fiber as Application.Fibers.Fiber).continue(this.expose());

    const response = await fiber.perform();

    return response;
  }

  protected async apply<T = void>(command: Storage.Commands.Interface): Promise<T> {
    const { storage } = this.provide();

    command.extend(this);

    await command.setup();

    const response = (await storage.apply(command as Storage.Commands.Command)) as Promise<T>;

    return response;
  }

  protected async schedule(job: Scheduling.Jobs.Interface): Promise<void> {
    const { scheduling } = this.provide();

    (job as Scheduling.Jobs.Job).extend(this);

    (job as Scheduling.Jobs.Job).continue(this.expose());

    await job.setup();

    await scheduling.enqueue(job);
  }

  protected async revoke(job: Scheduling.Jobs.Interface): Promise<void> {
    const { scheduling } = this.provide();

    (job as Scheduling.Jobs.Job).extend(this);

    (job as Scheduling.Jobs.Job).continue(this.expose());

    await job.setup();

    await scheduling.dequeue(job);
  }

  protected async request<R = void>(method: External.Interface): Promise<R> {
    (method as External.Api).extend(this);

    const response = await method.perform();

    return response;
  }

  protected async dispatch<R = void>(signal: Overlay.Signals.Interface): Promise<R> {
    const { overlay } = this.provide();

    signal.extend(this);

    (signal as Overlay.Signals.Signal).continue(this.expose());

    await signal.setup();

    const response = await overlay.dispatch<R>(signal);

    return response;
  }

  protected async print<R = Buffer>(template: Printing.Templates.Interface): Promise<R> {
    const { printing } = this.provide();

    (template as Printing.Templates.Template).extend(this);

    (template as Printing.Templates.Template).continue(this.expose());

    await (template as Printing.Templates.Template).setup();

    const pdf = (await printing.print(template)) as any;

    return pdf;
  }

  protected async send(template: Mailing.Templates.Interface): Promise<void> {
    (template as Mailing.Templates.Template).extend(this);

    (template as Mailing.Templates.Template).continue(this.expose());

    await (template as Mailing.Templates.Template).onSetup();

    await template.run();
  }
}
