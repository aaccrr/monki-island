export interface Interface<R = any> {
  perform(): Promise<R>;
}
