import * as Traceable from '../../Traceable';
import { Interface } from './Interface';

export abstract class Fiber<R = void> extends Traceable.Instance implements Interface<R> {
  protected payload: any;

  protected async onPerform(): Promise<R> {
    return;
  }

  async perform(): Promise<R> {
    try {
      this.trace.log('payload', this.payload);

      const result = await this.onPerform();

      this.trace.log('result', result);

      this.trace.finish();

      return result;
    } catch (e) {
      this.trace.fail(e);

      throw e;
    }
  }
}
