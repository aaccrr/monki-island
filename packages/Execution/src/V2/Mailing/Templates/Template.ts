import * as Nodemailer from 'nodemailer';
import * as Utility from '../../../Utility';
import * as Traceable from '../../Traceable';
import { Connection } from './Connection';
import { Interface } from './Interface';
import { Attachment } from './Attachment';

export abstract class Template extends Traceable.Instance implements Interface {
  protected connection: Connection;

  protected from: string;

  protected to: string | string[];

  protected subject: string;

  protected html: string;

  protected attachments: Attachment[] = [];

  async run(): Promise<void> {
    try {
      const mail = this.prepareMail();

      this.trace.log('mail', mail);

      const transport = Nodemailer.createTransport({
        host: this.connection.hostname,
        auth: {
          user: this.connection.username,
          pass: this.connection.password,
        },
        port: parseInt(this.connection.port),
        secure: false,
        tls: {
          ciphers: 'SSLv3',
          rejectUnauthorized: false,
        },
      });

      const result = await transport.sendMail(mail);

      this.trace.log('result', result);

      this.trace.finish();

      await this.onSended(result);
    } catch (e) {
      this.trace.fail(e);

      await this.onFailed(e);
    }
  }

  async onSetup(): Promise<void> {}

  protected async onSended(result: any): Promise<any> {
    return result;
  }

  protected async onFailed(error: any): Promise<any> {
    throw error;
  }

  private prepareMail(): Nodemailer.SendMailOptions {
    const mail: Nodemailer.SendMailOptions = {
      from: this.from,
      to: this.to,
      subject: this.subject,
    };

    if (Utility.Check.Defined(this.html)) {
      mail.html = this.html;
    }

    if (Utility.Check.IsArray(this.attachments)) {
      mail.attachments = this.attachments;
    }

    return mail;
  }
}
