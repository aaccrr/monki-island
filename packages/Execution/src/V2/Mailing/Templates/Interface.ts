export interface Interface {
  onSetup(): Promise<void>;
  run(): Promise<void>;
}
