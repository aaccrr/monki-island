export interface Connection {
  hostname: string;
  username: string;
  password: string;
  port: string;
}
