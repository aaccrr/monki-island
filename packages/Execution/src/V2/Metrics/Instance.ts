import * as prom from 'prom-client';
import * as Extendable from '../Extendable';
import * as Samples from './Samples';
import { Interface } from './Interface';

export class Instance extends Extendable.Instance implements Interface {
  async add(sample: Samples.Interface): Promise<void> {
    await this.updateCounter(sample);
  }

  pull(): Promise<string> {
    return prom.register.metrics();
  }

  private async updateCounter(sample: Samples.Interface): Promise<void> {
    const metric = await this.ensureCounterFor(sample);

    metric.inc(sample.labels());
  }

  private async ensureCounterFor(sample: Samples.Interface): Promise<prom.Counter<any>> {
    const storedMetric = await prom.register.getSingleMetric(sample.title());

    if (!storedMetric) {
      return new prom.Counter({
        name: sample.title(),
        help: sample.title(),
        labelNames: sample.labelsNames(),
      });
    } else {
      return storedMetric as prom.Counter<any>;
    }
  }
}
