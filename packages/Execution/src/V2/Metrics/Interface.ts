import * as Samples from './Samples';

export interface Interface {
  add(sample: Samples.Interface): Promise<void>;
  pull(): Promise<string>;
}
