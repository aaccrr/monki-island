import { Interface } from './Interface';

export abstract class Sample<L = any> implements Interface<L> {
  static namespace: string;

  protected namespace: string;

  protected _labels: L;

  title(): string {
    return this.namespace;
  }

  labelsNames(): string[] {
    const labels = this._labels || {};

    return Object.keys(labels);
  }

  labels(): L {
    return this._labels;
  }
}
