export interface Interface<L = any> {
  title(): string;
  labelsNames(): string[];
  labels(): L;
}
