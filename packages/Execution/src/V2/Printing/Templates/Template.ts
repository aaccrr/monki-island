import * as Traceable from '../../Traceable';
import { Interface } from './Interface';

export class Template extends Traceable.Instance implements Interface {
  protected template: string;

  getTemplate(): string {
    return this.template;
  }

  async setup(): Promise<void> {
    await this.onSetup();
  }

  async printed(pdf: Buffer): Promise<any> {
    this.trace.finish();

    const result = await this.onPrinted(pdf);

    return result;
  }

  failed(error: any): Promise<void> {
    this.trace.fail(error);

    return this.onFailed(error);
  }

  override extend(context: Traceable.Instance): this {
    const { tracing } = context.provide();

    this.extendTracing(tracing);

    this.continue(context.expose());

    return this;
  }

  protected async onSetup(): Promise<void> {}

  protected async onPrinted(pdf: Buffer): Promise<any> {
    return pdf;
  }

  protected async onFailed(error: any): Promise<void> {
    throw error;
  }
}
