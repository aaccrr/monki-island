import * as Templates from './Templates';

export interface Interface {
  init(): Promise<void>;
  print(template: Templates.Interface): Promise<Buffer>;
}
