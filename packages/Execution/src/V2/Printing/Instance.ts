import * as Puppeteer from 'puppeteer';
import * as Extendable from '../Extendable';
import * as Templates from './Templates';
import { Interface } from './Interface';

export class Printing extends Extendable.Instance implements Interface {
  private browser: Puppeteer.Browser;

  async init(): Promise<void> {
    this.browser = await Puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-dev-shm-usage'],
    });
  }

  async print(template: Templates.Template): Promise<any> {
    try {
      const page = await this.browser.newPage();

      await page.setViewport({ width: 794, height: 1122, deviceScaleFactor: 2 });

      const content = template.getTemplate();

      await page.setContent(content);

      const pdf = await page.pdf({ format: 'a4', printBackground: true });

      await page.close();

      const result = await template.printed(pdf);

      return result;
    } catch (e) {
      const error = await template.failed(e);

      return error;
    }
  }
}
