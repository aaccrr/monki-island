export const Create = (query: object): string => {
  const string = Object.keys(query).reduce((queryString, queryProp) => {
    if (query[queryProp] !== null && query[queryProp] !== undefined) {
      if (queryString !== '') {
        queryString = `${queryString}&${queryProp}=${query[queryProp]}`;
      } else {
        queryString = `${queryProp}=${query[queryProp]}`;
      }
    }

    return queryString;
  }, '');

  return encodeURI(string);
};
