import * as Url from 'url';
import * as Utility from '../../Utility';
import * as Traceable from '../Traceable';
import * as Authorization from './Authorization';
import * as Querystring from './Querystring';
import { Interface } from './Interface';

export abstract class Api<P = any, T = any> extends Traceable.Instance implements Interface<P, T> {
  protected url: string;

  protected authorization: Authorization.Interface = {
    type: 'none',
  };

  protected querystring: any;

  protected sendable: boolean = true;

  abstract perform(): Promise<T>;

  async onSetup(): Promise<void> {}

  async onResponse(response: any): Promise<T> {
    return response;
  }

  async onError(error: any): Promise<T> {
    throw new Error(error);
  }

  override extend(context: Traceable.Instance): Interface<P, T> {
    const { tracing } = context.provide();

    this.extendTracing(tracing);

    this.continue(context.expose());

    return this;
  }

  protected _headers(): any {
    const headers: any = {};

    if (this.authorization.type === 'bearer') {
      headers['Authorization'] = `Bearer ${this.authorization.token}`;
    }

    return headers;
  }

  protected create_url(): Url.URL {
    const url = new Url.URL(this.url);

    if (Utility.Check.Defined(this.querystring)) {
      for (const param of Object.keys(this.querystring)) {
        url.searchParams.append(param, this.querystring[param].toString());
      }
    }

    return url;
  }

  protected _url(): string {
    if (Utility.Check.Defined(this.querystring)) {
      return this.url + '?' + Querystring.Create(this.querystring);
    }

    return this.url;
  }

  protected _basic_auth(): any {
    if (this.authorization.type === 'basic') {
      return {
        username: this.authorization.username,
        password: this.authorization.password,
      };
    }

    return {};
  }
}
