export interface Interface<P = any, T = any> {
  perform(): Promise<T>;
}
