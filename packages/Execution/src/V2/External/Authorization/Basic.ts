export interface Basic {
  type: 'basic';
  username: string;
  password: string;
}
