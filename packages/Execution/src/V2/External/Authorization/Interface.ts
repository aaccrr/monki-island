import { None } from './None';
import { Bearer } from './Bearer';
import { Basic } from './Basic';

export type Interface = None | Bearer | Basic;
