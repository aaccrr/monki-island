export interface Bearer {
  type: 'bearer';
  token: string;
}
