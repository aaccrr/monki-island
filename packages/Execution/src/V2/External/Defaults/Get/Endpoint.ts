import * as Network from '@monki-island/network';
import { Api } from '../../Api';

export abstract class Endpoint<P = any, T = any> extends Api<P, T> {
  async perform(): Promise<T> {
    await this.onSetup();

    if (this.sendable) {
      try {
        this.logRequest();

        const request = new Network.Http.Requests.Get(this.create_url(), {});

        const raw = await request.send();

        this.trace.log('response', raw);

        const response = await this.onResponse(raw);

        return response;
      } catch (e) {
        this.trace.fail(e);

        return this.onError(e);
      } finally {
        this.trace.finish();
      }
    } else {
      this.trace.finish();

      return this.onResponse(null);
    }
  }

  private logRequest(): void {
    this.trace.log('request', {
      url: this.create_url(),
      method: 'get',
      headers: this._headers(),
    });
  }
}
