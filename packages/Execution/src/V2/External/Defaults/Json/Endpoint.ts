import * as Got from 'got';
import { Api } from '../../Api';

export abstract class Endpoint<P = any, T = any> extends Api<P, T> {
  protected method: 'post' | 'put';

  protected body: any = {};

  async perform(): Promise<T> {
    await this.onSetup();

    try {
      this.logRequest();

      const response = await Got.default(this._url(), {
        method: this.method,
        headers: this._headers(),
        resolveBodyOnly: true,
        responseType: 'json',
        json: this.body,
        ...this._basic_auth(),
      });

      this.trace.log('response', response);

      return this.onResponse(response);
    } catch (e) {
      this.trace.fail(e);

      return this.onError(e);
    } finally {
      this.trace.finish();
    }
  }

  private logRequest(): void {
    this.trace.log('request', {
      url: this._url(),
      method: this.method,
      headers: this._headers(),
      json: this.body,
    });
  }
}
