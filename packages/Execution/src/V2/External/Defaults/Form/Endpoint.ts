import * as FormData from 'form-data';
import * as Got from 'got';
import { Api } from '../../Api';
import { Data } from './Data';

export abstract class Endpoint<P = any, T = any> extends Api<P, T> {
  protected form_data: Data;

  async perform(): Promise<T> {
    await this.onSetup();

    try {
      this.logRequest();

      const response = await Got.default(this.url, {
        method: 'post',
        headers: this._headers(),
        resolveBodyOnly: true,
        responseType: 'buffer',
        body: this._body(),
        ...this._basic_auth(),
      });

      this.logResponse(response);

      return this.onResponse(response);
    } catch (e) {
      let error = e;

      this.trace.fail(error);

      return this.onError(error);
    } finally {
      this.trace.finish();
    }
  }

  private logRequest(): void {
    const body = Object.keys(this.form_data).reduce((log, field) => {
      if (typeof this.form_data[field].value !== 'string') {
        log[field] = {
          value: 'file',
          filename: this.form_data[field].filename,
        };
      } else {
        log[field] = this.form_data[field];
      }

      return log;
    }, {});

    this.trace.log('request', {
      url: this._url(),
      method: 'post',
      headers: this._headers(),
      body,
    });
  }

  private logResponse(response: any): void {
    if (typeof response !== 'string') {
      this.trace.log('response', 'binary');
    } else {
      this.trace.log('response', response);
    }
  }

  private _body(): FormData {
    const form = new FormData();

    Object.keys(this.form_data).map((field) => {
      const { value, filename } = this.form_data[field];

      form.append(field, value, filename);
    });

    return form;
  }
}
