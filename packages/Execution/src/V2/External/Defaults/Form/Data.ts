import * as Streams from 'stream';

export interface Data {
  [name: string]: {
    value: string | Buffer | Streams.Readable;
    filename?: string;
  };
}
