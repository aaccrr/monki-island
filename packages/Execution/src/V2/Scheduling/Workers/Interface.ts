export interface Interface {
  setup(): Promise<void>;
  start(): void;
  stop(): void;
}
