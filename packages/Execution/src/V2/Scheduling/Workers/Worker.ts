import * as Mongodb from 'mongodb';
import * as Extendable from '../../Extendable';
import * as Metrics from '../Metrics';
import * as Jobs from '../Jobs';
import * as Tasks from '../Tasks';
import * as Queues from '../Queues';
import { Interface } from './Interface';

export class Worker extends Extendable.Instance implements Interface {
  private _queue: Queues.Interface;

  private _poll: NodeJS.Timeout;

  private _task: Tasks.Constructor;

  protected async on_setup(): Promise<void> {}

  async setup(): Promise<void> {
    await this.on_setup();
  }

  start(): void {
    this._poll = setInterval(async () => {
      await this.perform();
    }, 100);
  }

  stop(): void {
    clearInterval(this._poll);
  }

  attach_queue(queue: Queues.Interface): void {
    this._queue = queue;
  }

  protected attach_task(task: Tasks.Constructor): void {
    this._task = task;
  }

  protected select(): Mongodb.Filter<any> {
    return {};
  }

  protected return(job: Jobs.Model): Jobs.Model {
    return job;
  }

  private async perform(): Promise<void> {
    const selection = this.select();

    if (selection === null) {
      return;
    }

    const job = await this._queue.pop(this.payloadify(selection));

    if (job !== null) {
      const task = new this._task(job.payload);

      task.extend(this);

      task.continue({});

      const performed = await task.perform();

      job.locked = null;

      if (!performed) {
        job.state = 'failed';

        job.fails++;

        await this._queue.return(job);
      }

      if (performed) {
        job.state = 'performed';

        await this._queue.return(this.return(job));
      }

      /*
      if (failedJob.state === 'rejected') {
        const { metrics } = this.provide();

        await metrics.add(new Metrics.Rejected(job));

        await task.onReject();
      }
      */
    }
  }

  private payloadify(selection: Mongodb.Filter<any>): Mongodb.Filter<Jobs.Model> {
    return Object.keys(selection).reduce((payloadified_selection, key) => {
      payloadified_selection[`payload.${key}`] = selection[key];

      return payloadified_selection;
    }, {} as Mongodb.Filter<Jobs.Model>);
  }
}
