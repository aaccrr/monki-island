import * as Extendable from '../Extendable';
import * as Queues from './Queues';
import * as Jobs from './Jobs';
import { Interface } from './Interface';

export abstract class Instance extends Extendable.Instance implements Interface {
  private queues = new Map<string, Queues.Interface>();

  async init(): Promise<void> {
    await this.on_init();

    for (const queue of this.queues.values()) {
      (queue as any).extend(this);

      await queue.setup();
    }
  }

  async start(): Promise<void> {
    for (const queue of this.queues.values()) {
      await queue.start();
    }

    await this.on_start();
  }

  async stop(): Promise<void> {
    for (const queue of this.queues.values()) {
      await queue.stop();
    }

    await this.on_stop();
  }

  async on_init(): Promise<void> {}

  async on_start(): Promise<void> {}

  async on_stop(): Promise<void> {}

  async enqueue(job: Jobs.Interface): Promise<void> {
    const queue = this.queues.get(job.getQueue());

    if (!queue) {
      throw new Error('queue_not_found');
    }

    await queue.add(job);
  }

  async dequeue(job: Jobs.Interface): Promise<void> {
    const queue = this.queues.get(job.getQueue());

    if (!queue) {
      throw new Error('queue_not_found');
    }

    await queue.remove(job.getId());
  }

  protected attach_queue(queue: Queues.Interface): void {
    if (!this.queues.has(queue.name())) {
      this.queues.set(queue.name(), queue);
    }
  }
}
