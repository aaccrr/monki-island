import * as Crypto from 'crypto';
import * as Utility from '../../../Utility';
import * as Traceable from '../../Traceable';
import { Interface } from './Interface';

export abstract class Job<T = any> extends Traceable.Instance implements Interface<T> {
  protected queue = this.constructor.name;

  protected payload: T;

  protected scheduled = null;

  protected idempotency: Partial<T>;

  protected replace = false;

  getQueue(): string {
    return this.queue;
  }

  getId(): string {
    if (Utility.Check.Defined(this.idempotency)) {
      return Crypto.createHash('sha256')
        .update(`${this.queue}:${JSON.stringify(this.idempotency)}`)
        .digest('hex');
    }

    return Crypto.createHash('sha256')
      .update(`${this.queue}:${JSON.stringify(this.payload)}`)
      .digest('hex');
  }

  getSchedule(): Date | null {
    return this.scheduled;
  }

  getPayload(): T {
    return this.payload || ({} as T);
  }

  requireReplace(): boolean {
    return this.replace;
  }

  async setup(): Promise<void> {
    await this.onSetup();

    this.trace.log('setup', {});
  }

  async queued(): Promise<void> {
    this.trace.finish();

    await this.onQueued();
  }

  async failed(error: any): Promise<void> {
    this.trace.fail(error);

    await this.onFail(error);
  }

  protected async onSetup(): Promise<void> {}

  protected async onQueued(): Promise<void> {}

  protected async onFail(error: any): Promise<void> {
    throw new Error(error);
  }
}
