export interface Interface<T = any> {
  getQueue(): string;
  getId(): string;
  getPayload(): T;
  getSchedule(): Date | null;
  requireReplace(): boolean;
  setup(): Promise<void>;
  queued(): Promise<void>;
  failed(error: any): Promise<any>;
}
