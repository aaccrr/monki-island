import * as Common from '@monki-island/common';

export interface Model<T = any> {
  _id: string;
  payload: T;
  state: 'pending' | 'locked' | 'performed' | 'failed' | 'rejected';
  fails: number;
  scheduled: Common.Nullable<Date>;
  locked?: Common.Nullable<Date>;
  performed?: Common.Nullable<Date>;
  rejected?: Common.Nullable<Date>;
}
