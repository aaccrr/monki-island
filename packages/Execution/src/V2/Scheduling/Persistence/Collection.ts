import * as Luxon from 'luxon';
import * as Mongodb from 'mongodb';
import * as Jobs from '../Jobs';
import { Configuration } from './Configuration';

export class Collection {
  private _collection: Mongodb.Collection<Jobs.Model>;

  async init(configuration: Configuration): Promise<void> {
    const { username, hostname, password, database, collection } = configuration;

    const client = await Mongodb.MongoClient.connect(`mongodb://${username}:${password}@${hostname}?authSource=admin`);

    const db = client.db(database);

    this._collection = db.collection(collection);
  }

  async add(job: Jobs.Model): Promise<void> {
    try {
      await this._collection.insertOne(job);
    } catch (e) {}
  }

  async replace(job: Jobs.Model): Promise<void> {
    await this._collection.updateOne(
      {
        _id: job._id,
      },
      {
        $set: {
          ...job,
        },
      },
      {
        upsert: true,
      }
    );
  }

  async promote(job: Jobs.Model): Promise<void> {
    await this._collection.updateOne(
      {
        _id: job._id,
        'payload.stage': {
          $lt: job.payload.stage,
        },
      },
      {
        $set: {
          ...job,
        },
      },
      {
        upsert: true,
      }
    );
  }

  async update(filter: Mongodb.Filter<Jobs.Model>, job: Jobs.Model): Promise<void> {
    await this._collection.updateOne(filter, {
      $set: {
        ...job,
      },
    });
  }

  async delete_one(job_id: string): Promise<void> {
    await this._collection.deleteOne({
      _id: job_id,
    });
  }

  async find_one_and_update(selection: Mongodb.Filter<any>): Promise<Jobs.Model | null> {
    const job = await this._collection.findOneAndUpdate(
      {
        state: 'pending',
        scheduled: {
          $lte: new Date(),
        },
        ...selection,
      },
      {
        $set: {
          state: 'locked',
          locked: new Date(),
        },
      },
      {
        returnDocument: 'after',
      }
    );

    if (typeof job.value === 'undefined') {
      return null;
    }

    return job.value;
  }

  async update_many(): Promise<void> {
    await this._collection.updateMany(
      {
        state: 'locked',
        locked: {
          $lte: Luxon.DateTime.fromJSDate(new Date()).minus(60000).toJSDate(),
        },
      },
      {
        $set: {
          state: 'pending',
          locked: null,
        },
      }
    );
  }
}
