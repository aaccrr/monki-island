export interface Configuration {
  hostname: string;
  username: string;
  password: string;
  database: string;
  collection: string;
}
