import * as Jobs from './Jobs';

export interface Interface {
  init(): Promise<void>;
  enqueue(job: Jobs.Interface): Promise<void>;
  dequeue(job: Jobs.Interface): Promise<void>;
}
