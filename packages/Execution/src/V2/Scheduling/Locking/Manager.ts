import * as Queues from '../Queues';
import { Interface } from './Interface';

export class Manager implements Interface {
  constructor(private _queue: Queues.Interface) {}

  private poll: NodeJS.Timeout;

  start(): void {
    this.poll = setInterval(async () => {
      await this.wreck();
    }, 60000);
  }

  stop(): void {
    clearInterval(this.poll);
  }

  private async wreck(): Promise<void> {
    console.log('locking:wreck:called ');

    await this._queue.drop_deadlocks();
  }
}
