import * as Schedulers from '../Schedulers';
import * as Jobs from '../Jobs';
import { Interface } from './Interface';
import { Queue } from './Queue';

export abstract class Poll extends Queue implements Interface {
  constructor() {
    super();

    this.scheduler = new Schedulers.Poll(this.interval, this.bounce);
  }

  protected interval = 1000;

  protected bounce = this.interval / 2;

  override async add(job: Jobs.Interface): Promise<void> {
    try {
      const model = this.scheduler.schedule({
        _id: job.getId(),
        payload: job.getPayload(),
        state: 'pending',
        fails: 0,
        scheduled: job.getSchedule(),
      });

      await this.collection.replace(model);

      await job.queued();
    } catch (e) {
      await job.failed(e);
    }
  }

  override async return(job: Jobs.Model): Promise<void> {
    console.log('poll:return, ', job);

    await this.collection.update(
      {
        _id: job._id,
        state: 'locked',
      },
      this.scheduler.schedule(this.reject(job))
    );
  }

  override reject(job: Jobs.Model): Jobs.Model {
    if (job.state === 'failed') {
      if (job.fails >= this.max_fails) {
        job.state = 'pending';

        job.fails = 0;
      }
    }

    return job;
  }
}
