import * as Mongodb from 'mongodb';
import * as Jobs from '../Jobs';

export interface Interface {
  name(): string;
  setup(): Promise<void>;
  start(): Promise<void>;
  stop(): Promise<void>;
  add(job: Jobs.Interface): Promise<void>;
  pop(selection: Mongodb.Filter<any>): Promise<Jobs.Model | null>;
  remove(job_id: string): Promise<void>;
  return(job: Jobs.Model): Promise<void>;
  drop_deadlocks(): Promise<void>;
}
