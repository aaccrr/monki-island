import * as Schedulers from '../Schedulers';
import * as Jobs from '../Jobs';
import { Interface } from './Interface';
import { Queue } from './Queue';

export abstract class Scheduled extends Queue implements Interface {
  constructor() {
    super();

    this.scheduler = new Schedulers.Scheduled();
  }

  override async add(job: Jobs.Interface): Promise<void> {
    try {
      const model = this.scheduler.schedule({
        _id: job.getId(),
        payload: job.getPayload(),
        state: 'pending',
        fails: 0,
        scheduled: job.getSchedule(),
      });

      if (job.requireReplace()) {
        await this.collection.replace(model);
      }

      if (!job.requireReplace()) {
        await this.collection.add(model);
      }

      await job.queued();
    } catch (e) {
      await job.failed(e);
    }
  }

  override async return(job: Jobs.Model): Promise<void> {
    await this.collection.update(
      {
        _id: job._id,
        state: 'locked',
      },
      this.scheduler.schedule(this.reject(job))
    );
  }
}
