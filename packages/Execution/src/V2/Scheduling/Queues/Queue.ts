import * as Mongodb from 'mongodb';
import * as Extendable from '../../Extendable';
import * as Jobs from '../Jobs';
import * as Workers from '../Workers';
import * as Locking from '../Locking';
import * as Persistence from '../Persistence';
import * as Schedulers from '../Schedulers';
import { Interface } from './Interface';

export abstract class Queue extends Extendable.Instance implements Interface {
  private _worker: Workers.Worker;

  private _lock_manager: Locking.Manager;

  protected scheduler: Schedulers.Interface;

  protected persistence: Persistence.Configuration;

  protected collection: Persistence.Collection;

  protected max_fails = 10;

  abstract on_setup(): Promise<void>;

  abstract add(job: Jobs.Interface): Promise<void>;

  abstract return(job: Jobs.Model): Promise<void>;

  name(): string {
    return this.constructor.name;
  }

  async setup(): Promise<void> {
    await this.on_setup();

    await this.attach_persistence();

    this.attach_manager();

    await this.setup_worker();
  }

  async start(): Promise<void> {
    await this._worker.start();

    await this._lock_manager.start();
  }

  async stop(): Promise<void> {
    await this._worker.stop();

    await this._lock_manager.stop();
  }

  async remove(job_id: string): Promise<void> {
    await this.collection.delete_one(job_id);
  }

  async pop(selection: Mongodb.Filter<any>): Promise<Jobs.Model | null> {
    const job = await this.collection.find_one_and_update(selection);

    return job;
  }

  async drop_deadlocks(): Promise<void> {
    await this.collection.update_many();
  }

  protected attach_worker(worker: Workers.Worker): void {
    this._worker = worker;

    this._worker.extend(this);
  }

  protected reject(job: Jobs.Model): Jobs.Model {
    if (job.state === 'failed') {
      if (job.fails >= this.max_fails) {
        job.state = 'rejected';

        job.rejected = new Date();
      }
    }

    return job;
  }

  private async attach_persistence(): Promise<void> {
    this.collection = new Persistence.Collection();

    await this.collection.init(this.persistence);
  }

  private attach_manager(): void {
    this._lock_manager = new Locking.Manager(this);
  }

  private async setup_worker(): Promise<void> {
    await this._worker.setup();

    await this._worker.attach_queue(this);
  }
}
