import * as Instrumentary from '../../../Instrumentary';
import * as Metrics from '../../Metrics';

@Instrumentary.Namespace(__filename)
export class Rejected extends Metrics.Samples.Sample {
  constructor(queue: string) {
    super();

    this._labels = {
      queue,
    };
  }
}
