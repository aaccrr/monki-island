import * as Jobs from '../Jobs';

export interface Interface {
  schedule(job: Jobs.Model): Jobs.Model;
}
