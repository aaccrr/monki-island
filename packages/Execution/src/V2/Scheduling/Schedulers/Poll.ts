import * as Luxon from 'luxon';
import * as Jobs from '../Jobs';
import { Scheduler } from './Scheduler';

export class Poll extends Scheduler {
  constructor(private interval: number, private bounce: number | null) {
    super();
  }

  override schedule(job: Jobs.Model): Jobs.Model {
    const schedule = Luxon.DateTime.fromJSDate(new Date()).plus(this.interval);

    if (typeof this.bounce === 'number') {
      schedule.plus(this.add_bounce());
    }

    job.scheduled = schedule.toJSDate();

    job.state = 'pending';

    return job;
  }

  private add_bounce(): number {
    if (typeof this.bounce === 'undefined' || this.bounce === null) {
      return 0;
    }

    const min = 0 - this.bounce;

    const max = this.bounce;

    return Math.floor(Math.random() * (max - min) + min);
  }
}
