import * as Luxon from 'luxon';
import * as Jobs from '../Jobs';
import { Scheduler } from './Scheduler';

export class Scheduled extends Scheduler {
  override schedule(job: Jobs.Model): Jobs.Model {
    if (job.state === 'failed') {
      return this.retry(job);
    }

    if (job.scheduled === null) {
      job.scheduled = new Date();
    }

    return job;
  }
}
