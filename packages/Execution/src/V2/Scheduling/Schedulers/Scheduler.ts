import * as Luxon from 'luxon';
import * as Jobs from '../Jobs';
import { Interface } from './Interface';

export class Scheduler implements Interface {
  schedule(job: Jobs.Model): Jobs.Model {
    return job;
  }

  protected retry(job: Jobs.Model): Jobs.Model {
    const timeout = job.fails * 2;

    job.scheduled = Luxon.DateTime.fromJSDate(new Date()).plus({ minutes: timeout }).toJSDate();

    return job;
  }
}
