import * as Jobs from '../Jobs';
import { Scheduler } from './Scheduler';

export class Conveyor extends Scheduler {
  override schedule(job: Jobs.Model): Jobs.Model {
    if (job.state === 'failed') {
      return this.retry(job);
    }

    job.scheduled = new Date();

    return job;
  }
}
