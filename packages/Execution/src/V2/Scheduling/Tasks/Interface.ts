import * as Jobs from '../Jobs';

export interface Interface {
  perform(job: Jobs.Model): Promise<boolean>;
}
