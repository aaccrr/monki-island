import { Task } from './Task';

export interface Constructor<P = any> {
  new (payload: P): Task;
  namespace: string;
}
