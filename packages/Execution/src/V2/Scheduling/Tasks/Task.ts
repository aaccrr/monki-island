import * as Traceable from '../../Traceable';

export abstract class Task<T = any> extends Traceable.Instance {
  constructor(protected payload: T) {
    super();
  }

  protected abstract on_perform(): Promise<void>;

  async on_reject(): Promise<void> {}

  async perform(): Promise<boolean> {
    try {
      this.trace.log('payload', this.payload);

      await this.on_perform();

      this.trace.log('result', 'void');

      this.trace.finish();

      return true;
    } catch (e) {
      this.trace.fail(e);

      return false;
    }
  }

  protected update(payload: Partial<T>): void {
    Object.keys(payload).forEach((key) => {
      this.payload[key] = payload[key];
    });
  }
}
