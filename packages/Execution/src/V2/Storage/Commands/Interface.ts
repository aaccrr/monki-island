import * as Traceable from '../../Traceable';

export interface Interface<R = any> {
  extend(context: Traceable.Instance): Interface<R>;
  setup(): Promise<void>;
}
