export interface Composition<E = any[]> {
  handler: string;
  execute: E;
}
