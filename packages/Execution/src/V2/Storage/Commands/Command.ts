import * as Traceable from '../../Traceable';
import { Interface } from './Interface';
import { Composition } from './Composition';

export abstract class Command<R = any> extends Traceable.Instance implements Interface<R> {
  protected bucket: string;

  abstract compose(): Composition;

  override extend(context: Traceable.Instance): Interface<R> {
    const { tracing, storage } = context.provide();

    this.extendTracing(tracing);

    this.extendStorage(storage);

    this.continue(context.expose());

    return this;
  }

  async setup(): Promise<void> {
    await this.onSetup();

    this.trace.log('setup', this.compose());
  }

  async response(response: any): Promise<R> {
    this.trace.log('response', response);

    this.trace.finish();

    return this.onResponse(response);
  }

  async error(error: any): Promise<any> {
    this.trace.fail(error);

    return this.onError(error);
  }

  protected async onSetup(): Promise<void> {}

  protected async onResponse(response: any): Promise<R> {
    return response;
  }

  protected async onError(error: any): Promise<any> {
    throw new Error(error);
  }
}
