import * as Commands from '../Commands';

export interface Interface {
  handle(composition: Commands.Composition): any;
}
