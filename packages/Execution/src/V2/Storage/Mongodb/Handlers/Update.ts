import * as Mongodb from 'mongodb';
import * as Dot from 'dot-object';
import * as Commands from '../../Commands';

export class Update<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<[Mongodb.Filter<T>, Mongodb.UpdateFilter<T>]>): Promise<T> {
    Dot.keepArray = true;

    composition.execute[1].$set = Dot.dot(composition.execute[1].$set);

    const model = await this.collection.findOneAndUpdate(...composition.execute, {
      returnDocument: 'after',
    });

    if (!model.value) {
      return null;
    }

    return model.value as T;
  }
}
