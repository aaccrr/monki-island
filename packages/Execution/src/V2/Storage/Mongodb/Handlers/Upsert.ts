import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export class Upsert<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<[Mongodb.Filter<T>, Mongodb.UpdateFilter<T>]>): Promise<T> {
    const model = await this.collection.findOneAndUpdate(...composition.execute, {
      upsert: true,
      returnDocument: 'after',
    });

    if (!model.value) {
      return null;
    }

    return model.value as T;
  }
}
