import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export class List<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<[Mongodb.Filter<T>, Mongodb.FindOptions]>): Promise<T[]> {
    return this.collection.find(...composition.execute).toArray() as Promise<T[]>;
  }
}
