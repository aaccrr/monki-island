import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export class Aggregate<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<Mongodb.Document[]>): Promise<any> {
    const result = await this.collection.aggregate(composition.execute).toArray();

    return result;
  }
}
