import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export class Count<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<[Mongodb.Filter<T>]>): Promise<number> {
    const count = await this.collection.countDocuments(...composition.execute);

    return count;
  }
}
