import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export class Create<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<[any]>): Promise<void> {
    await this.collection.insertOne(...composition.execute);
  }
}
