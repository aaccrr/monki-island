import * as Mongodb from 'mongodb';
import * as Common from '@monki-island/common';
import * as Commands from '../../Commands';

export class Paginate<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<[Mongodb.Filter<T>, Mongodb.FindOptions]>): Promise<Common.WithPagination<T>> {
    const [total, items] = await Promise.all([
      this.collection.countDocuments(composition.execute[0]),
      this.collection.find(...composition.execute).toArray(),
    ]);

    return {
      total,
      offset: composition.execute[1].skip,
      items: items as T[],
    };
  }
}
