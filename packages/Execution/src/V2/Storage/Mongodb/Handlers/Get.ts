import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export class Get<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<[Mongodb.Filter<T>]>): Promise<T> {
    const document = await this.collection.findOne(...composition.execute);

    if (!document) {
      return null;
    }

    return document as T;
  }
}
