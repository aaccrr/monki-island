import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export class Delete<T = any> {
  constructor(private collection: Mongodb.Collection<T>) {}

  async handle(composition: Commands.Composition<[Mongodb.Filter<T>]>): Promise<T> {
    const document = await this.collection.findOneAndDelete(...composition.execute);

    if (!document.value) {
      return null;
    }

    return document.value as T;
  }
}
