import * as Mongodb from 'mongodb';
import * as Handlers from '../Handlers';
import { Connection } from './Connection';

export abstract class Bucket {
  protected connection: Connection;

  private database: Mongodb.Db;

  private _handlers = new Map();

  abstract onInit(): Promise<void>;

  async init(): Promise<void> {
    const { username, hostname, password, database } = this.connection;

    const client = await Mongodb.MongoClient.connect(`mongodb://${username}:${password}@${hostname}?authSource=admin`);

    this.database = client.db(database);

    await this.onInit();

    /*
    for (const index of config.indexes) {
      await this.client.db(index.database).collection(index.collection).createIndex(index.specification, index.options);
    }
    */
  }

  handlers(): Map<string, any> {
    return this._handlers;
  }

  collection(name: string): void {
    const collection = this.database.collection(name);

    const handlers = [
      Handlers.Aggregate,
      Handlers.Count,
      Handlers.Create,
      Handlers.Delete,
      Handlers.Get,
      Handlers.List,
      Handlers.Paginate,
      Handlers.Update,
      Handlers.Upsert,
    ];

    for (const handler of handlers) {
      this._handlers.set(`${name}.${handler.name}`, new handler(collection));
    }
  }
}
