import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export abstract class Upsert<T = any> extends Commands.Command {
  protected filter: Mongodb.Filter<T> = {};

  protected insertion: Mongodb.UpdateFilter<T> = {};

  compose(): Commands.Composition {
    return {
      handler: `${this.bucket}.Upsert`,
      execute: [this.filter, this.insertion],
    };
  }
}
