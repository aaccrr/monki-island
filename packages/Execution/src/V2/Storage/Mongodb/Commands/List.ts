import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export abstract class List<T = any> extends Commands.Command {
  protected filter: Mongodb.Filter<T> = {};

  protected options: Mongodb.FindOptions = {};

  compose(): Commands.Composition {
    return {
      handler: `${this.bucket}.List`,
      execute: [this.filter, this.options],
    };
  }
}
