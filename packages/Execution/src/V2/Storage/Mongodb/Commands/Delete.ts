import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export abstract class Delete<T = any> extends Commands.Command {
  protected filter: Mongodb.Filter<T> = {};

  compose(): Commands.Composition {
    return {
      handler: `${this.bucket}.Delete`,
      execute: [this.filter],
    };
  }
}
