import * as Commands from '../../Commands';

export abstract class Create<T = any> extends Commands.Command {
  protected insertion: T;

  compose(): Commands.Composition {
    return {
      handler: `${this.bucket}.Insert`,
      execute: [this.insertion],
    };
  }
}
