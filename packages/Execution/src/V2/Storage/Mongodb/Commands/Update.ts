import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export abstract class Update<T = any> extends Commands.Command {
  protected filter: Mongodb.Filter<T> = {};

  protected insertion: Mongodb.UpdateFilter<T> = {};

  compose(): Commands.Composition {
    return {
      handler: `${this.bucket}.Update`,
      execute: [this.filter, this.insertion],
    };
  }
}
