import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export abstract class Aggregate<R = any> extends Commands.Command<R> {
  protected stages: Mongodb.Document[] = [];

  compose(): Commands.Composition {
    return {
      handler: `${this.bucket}.Aggregate`,
      execute: this.stages,
    };
  }

  protected stage(stage: Mongodb.Document): void {
    this.stages.push(stage);
  }
}
