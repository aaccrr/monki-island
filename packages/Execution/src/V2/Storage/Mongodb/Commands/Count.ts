import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export abstract class Count<M = any, R = any> extends Commands.Command<R> {
  protected filter: Mongodb.Filter<M> = {};

  compose(): Commands.Composition {
    return {
      handler: `${this.bucket}.Count`,
      execute: [this.filter],
    };
  }
}
