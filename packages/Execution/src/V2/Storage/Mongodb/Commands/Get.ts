import * as Mongodb from 'mongodb';
import * as Commands from '../../Commands';

export abstract class Get<T = any> extends Commands.Command {
  protected filter: Mongodb.Filter<T> = {};

  compose(): Commands.Composition {
    return {
      handler: `${this.bucket}.Get`,
      execute: [this.filter],
    };
  }
}
