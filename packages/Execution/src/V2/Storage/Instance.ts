import * as Extendable from '../Extendable';
import * as Commands from './Commands';
import * as Buckets from './Buckets';
import * as Handlers from './Handlers';
import { Interface } from './Interface';

export abstract class Instance extends Extendable.Instance implements Interface {
  private handlers = new Map<string, Handlers.Interface>();

  async init(): Promise<void> {
    await this.onInit();
  }

  abstract onInit(): Promise<void>;

  async addBucket(bucket: Buckets.Interface): Promise<void> {
    await bucket.init();

    bucket.handlers().forEach((handler, key) => {
      this.handlers.set(key, handler);
    });
  }

  async apply<T = any>(command: Commands.Command): Promise<T> {
    const composition = command.compose();

    const handler = this.handlers.get(composition.handler);

    if (!handler) {
      throw new Error('handler_not_found');
    }

    try {
      const response = await handler.handle(composition);

      return command.response(response);
    } catch (e) {
      return command.error(e);
    }
  }
}
