export interface Interface {
  init(): Promise<void>;
  handlers(): Map<string, any>;
  collection(name: string): void;
}
