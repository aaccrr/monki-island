import * as Buckets from './Buckets';
import * as Commands from './Commands';

export interface Interface {
  init(): Promise<void>;
  addBucket(bucket: Buckets.Interface): Promise<void>;
  apply<T = any>(command: Commands.Command): Promise<T>;
}
