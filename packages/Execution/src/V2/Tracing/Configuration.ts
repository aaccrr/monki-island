export interface Configuration {
  group: string;
  hostname: string;
}
