export interface Interface {
  log(name: string, body: any): void;
  fail(error: any): void;
  extract(): any;
  finish(): void;
}
