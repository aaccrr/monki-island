import * as Opentracing from 'opentracing';
import { Interface } from './Interface';

export class Instance implements Interface {
  constructor(private span: Opentracing.Span) {}

  log(name: string, body: any): void {
    const payload = this.hideConstructorsFromLogging(body);

    this.findTags(payload);

    this.span.log({
      [name]: payload,
    });
  }

  fail(error: any): void {
    this.span.setTag('error', true);

    this.span.log({
      error: {
        message: error.message,
        stack: error.stack,
      },
    });

    this.finish();
  }

  extract(): any {
    const carrier = {};

    this.span.tracer().inject(this.span.context(), Opentracing.FORMAT_TEXT_MAP, carrier);

    return carrier;
  }

  finish(): void {
    this.span.finish();
  }

  private hideConstructorsFromLogging(payload: any): any {
    try {
      JSON.stringify(payload);

      return payload;
    } catch (e) {
      return {};
    }
  }

  private findTags(payload: any): void {
    this.span.setTag('env', process.env.NODE_ENV);

    if (payload) {
      if (typeof payload.meta?.user?.id === 'string' && payload.meta?.user?.id?.length > 0) {
        this.span.setTag('user.id', payload.meta?.user?.id);
      }

      if (payload.body) {
        Object.keys(payload.body).forEach((payloadKey) => {
          if (payloadKey.toLowerCase().endsWith('id')) {
            this.span.setTag(`entity.${payloadKey}`, payload.body[payloadKey]);
          }
        });
      }
    }
  }
}
