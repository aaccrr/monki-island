import * as Opentracing from 'opentracing';
import * as Jaeger from 'jaeger-client';
import * as Extendable from '../Extendable';
import * as Span from './Span';
import * as Carrier from './Carrier';
import { Interface } from './Interface';
import { Configuration } from './Configuration';

export class Instance extends Extendable.Instance implements Interface {
  constructor(private configuration: Configuration) {
    super();
  }

  private _tracer: Opentracing.Tracer;

  async init(): Promise<void> {
    this._tracer = Jaeger.initTracer(
      {
        serviceName: this.configuration.group,
        reporter: {
          logSpans: true,
          ...this.agentConfiguration(this.configuration),
        },
        sampler: {
          type: 'const',
          param: 1,
        },
      },
      {}
    );
  }

  create(title: string): Span.Interface {
    return new Span.Instance(this._tracer.startSpan(title));
  }

  link(target: Carrier.Interface, title: string): Span.Interface {
    const link = this._tracer.extract(Opentracing.FORMAT_TEXT_MAP, target);

    return new Span.Instance(this._tracer.startSpan(title, { references: [Opentracing.followsFrom(link)] }));
  }

  continue(parent: Carrier.Interface, title: string): Span.Interface {
    const trace = this._tracer.extract(Opentracing.FORMAT_TEXT_MAP, parent);

    return new Span.Instance(this._tracer.startSpan(title, { childOf: trace }));
  }

  private agentConfiguration(configuration: Configuration): Partial<Jaeger.ReporterConfig> {
    const [agentHost, agentPort] = configuration.hostname.split(':');

    return {
      agentHost,
      agentPort: parseInt(agentPort),
    };
  }
}
