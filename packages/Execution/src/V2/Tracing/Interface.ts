import * as Carrier from './Carrier';
import * as Span from './Span';

export interface Interface {
  init(): Promise<void>;
  create(title: string): Span.Interface;
  link(target: Carrier.Interface, title: string): Span.Interface;
  continue(parent: Carrier.Interface, title: string): Span.Interface;
}
