import * as Extendable from '../Extendable';
import * as Tracing from '../Tracing';
import * as Metrics from '../Metrics';
import * as Storage from '../Storage';
import * as Overlay from '../Overlay';
import * as Scheduling from '../Scheduling';
import * as Http from '../Http';
import * as Printing from '../Printing';

export abstract class Instance {
  private services: Map<string, Extendable.Interface> = new Map();

  abstract onInit(): Promise<void>;

  async onStart(): Promise<void> {}

  async init(): Promise<void> {
    await this.onInit();

    if (process.env.MODE === 'create_specification') {
      const http = this.services.get('http');

      if (http) {
        await (http as any).init();

        await (http as any).specification();
      }

      process.exit();
    }

    if (process.env.MODE === 'run') {
      for (const [key, service] of this.services) {
        service.extend(this as any);

        if (typeof (service as any).init === 'function') {
          await (service as any).init();
        }

        if (typeof (service as any).start === 'function') {
          await (service as any).start();
        }
      }

      await this.onStart();
    }
  }

  provide(): any {
    return {
      tracing: this.services.get('tracing'),
      metrics: this.services.get('metrics'),
      storage: this.services.get('storage'),
      overlay: this.services.get('overlay'),
      scheduling: this.services.get('scheduling'),
      printing: this.services.get('printing'),
    };
  }

  protected async tracing(tracing: Tracing.Interface): Promise<void> {
    this.services.set('tracing', tracing as any);
  }

  protected async metrics(metrics: Metrics.Interface): Promise<void> {
    this.services.set('metrics', metrics as any);
  }

  protected async storage(storage: Storage.Interface): Promise<void> {
    this.services.set('storage', storage as any);
  }

  protected async overlay(overlay: Overlay.Interface): Promise<void> {
    this.services.set('overlay', overlay as any);
  }

  protected async scheduling(scheduling: Scheduling.Interface): Promise<void> {
    this.services.set('scheduling', scheduling as any);
  }

  protected async http(http: Http.Interface): Promise<void> {
    this.services.set('http', http as any);
  }

  protected async printing(printing: Printing.Interface): Promise<void> {
    this.services.set('printing', printing as any);
  }
}
