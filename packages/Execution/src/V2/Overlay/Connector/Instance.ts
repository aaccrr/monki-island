import * as Events from 'events';
import * as Nats from 'nats';
import { Configuration } from '../Configuration';

export class Instance extends Events.EventEmitter {
  constructor(private configuration: Configuration) {
    super();
  }

  private connection: Nats.NatsConnection;

  async connect(): Promise<void> {
    try {
      this.connection = await Nats.connect({
        servers: this.configuration.hostname,
        name: this.configuration.group,
      });
    } catch (e) {
      throw new Error('overlay_connection_error');
    }
  }

  disconnect(): void {}

  async publish<T>(signal: string, message: T): Promise<void> {
    await this.connection.publish(signal, Nats.JSONCodec().encode(message));
  }

  async request<T = any, Y = any>(signal: string, message: T): Promise<Y> {
    const responseSignal = await this.connection.request(signal, Nats.JSONCodec().encode(message), {
      timeout: 30000,
    });

    return Nats.JSONCodec<any>().decode(responseSignal.data);
  }

  listen(signal: string): void {
    this.connection.subscribe(signal, {
      queue: this.configuration.group,
      callback: (err, message) => {
        this.emit(signal, Nats.JSONCodec().decode(message.data), (response: any): void => {
          message.respond(
            Nats.JSONCodec().encode({
              data: response,
            })
          );
        });
      },
    });
  }
}
