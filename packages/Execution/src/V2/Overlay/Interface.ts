import * as Handler from './Handler';
import * as Signals from './Signals';

export interface Interface {
  init(): Promise<void>;
  listen(signal: string, Handler: Handler.Constructor): void;
  dispatch<R = void>(signal: Signals.Interface): Promise<R>;
}
