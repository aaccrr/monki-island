import * as Traceable from '../../Traceable';
import { Message } from './Message';

export interface Interface {
  extend(context: Traceable.Instance): Interface;
  message(): Message;
  setup(): Promise<void>;
}
