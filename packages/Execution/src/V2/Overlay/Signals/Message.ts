import * as Tracing from '../../Tracing';

export interface Message<P = any> {
  signal: string;
  payload: P;
  trace: Tracing.Carrier.Interface;
}
