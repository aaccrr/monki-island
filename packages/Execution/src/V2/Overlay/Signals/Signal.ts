import * as Traceable from '../../Traceable';
import { Interface } from './Interface';
import { Message } from './Message';

export abstract class Signal<P = any> extends Traceable.Instance implements Interface {
  protected name: string;

  protected payload: P;

  override extend(context: Traceable.Instance): Interface {
    const { tracing, storage } = context.provide();

    this.extendTracing(tracing);

    this.extendStorage(storage);

    this.continue(context.expose());

    return this;
  }

  message(): Message<P> {
    return {
      signal: this.name,
      payload: this.payload,
      trace: this.expose(),
    };
  }

  async setup(): Promise<void> {
    await this.onSetup();

    this.trace.log('setup', this.message());
  }

  async response(response: any): Promise<any> {
    this.trace.log('response', response);

    this.trace.finish();

    return this.onResponse(response);
  }

  async error(error: any): Promise<any> {
    this.trace.fail(error);

    return this.onError(error);
  }

  protected async onSetup(): Promise<void> {}

  protected async onResponse(response: any): Promise<any> {
    return response;
  }

  protected async onError(error: any): Promise<any> {
    throw new Error(error);
  }
}
