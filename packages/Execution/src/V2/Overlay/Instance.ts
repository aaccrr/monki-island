import * as Extendable from '../Extendable';
import * as Connector from './Connector';
import { Configuration } from './Configuration';
import * as Handler from './Handler';
import * as Signals from './Signals';
import { Interface } from './Interface';

export abstract class Service extends Extendable.Instance implements Interface {
  constructor(private configuration: Configuration) {
    super();
  }

  private connector = new Connector.Instance(this.configuration);

  protected abstract onInit(): Promise<void>;

  async init(): Promise<void> {
    await this.connector.connect();

    await this.onInit();
  }

  listen(signal: string, Handler: Handler.Constructor): void {
    this.connector.on(signal, async (message, respond) => {
      try {
        const handler = new Handler();

        handler.extend(this);

        handler.continue(message.trace);

        handler.onInit();

        const result = await handler.handle({
          signal,
          payload: message.payload,
          trace: message.trace,
        });

        respond(result || {});
      } catch (e) {
        respond({
          error: true,
          message: e.message,
        });
      }
    });

    this.connector.listen(signal);
  }

  async dispatch<R = void>(signal: Signals.Signal): Promise<R> {
    try {
      const message = signal.message();

      if (message.signal.endsWith('ed') || message.signal.endsWith('id')) {
        await this.connector.publish(message.signal, message);

        return {} as R;
      }

      const raw = await this.connector.request(message.signal, message);

      if (raw.error || raw.data.error) {
        throw new Error(raw.message || raw.data.message);
      }

      const response = await signal.response(raw.data || raw || {});

      return response;
    } catch (e) {
      const error = await signal.error(e);

      return error;
    }
  }
}
