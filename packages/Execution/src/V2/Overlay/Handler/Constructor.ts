import { Instance } from './Instance';

export interface Constructor {
  new (): Instance;
}
