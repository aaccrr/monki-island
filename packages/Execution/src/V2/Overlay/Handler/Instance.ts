import * as Traceable from '../../Traceable';
import * as Signals from '../Signals';
import { Adapter } from './Adapter';

export abstract class Instance<T = any, B = any> extends Traceable.Instance {
  private adapters: Map<string, Adapter> = new Map();

  protected abstract onHandle(message: T): Promise<B>;

  async onInit(): Promise<void> {}

  async handle(message: Signals.Message<T>): Promise<B> {
    try {
      this.trace.log('message', message.payload);

      if (this.adapters.has(message.signal)) {
        const adapter = this.adapters.get(message.signal);

        message.payload = adapter(message.payload);

        this.trace.log('adapted', message.payload);
      }

      const result = await this.onHandle(message.payload);

      this.trace.log('result', result);

      this.trace.finish();

      return result;
    } catch (e) {
      this.trace.fail(e);

      throw e;
    }
  }

  protected adapter(signal: string, adapter: Adapter): void {
    this.adapters.set(signal, adapter);
  }
}
