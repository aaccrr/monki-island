export type Adapter<I = any, O = any> = (input: I) => O;
