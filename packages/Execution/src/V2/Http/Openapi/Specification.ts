import { OpenAPIV3 } from 'openapi-types';
import { Validator } from './Validator';
import { Request } from './Request';
import { Response } from './Response';
import { Error } from './Error';

export class Specification {
  constructor(private method: string, private path: string) {}

  protected description = '';

  protected tags: string[] = [];

  protected request: Request = {};

  protected response: Response = {};

  protected components: OpenAPIV3.ComponentsObject = {
    schemas: {},
  };

  validate(ctx: any): void {
    const openapiSchemaValidator = new Validator(this.request);

    openapiSchemaValidator.validate({
      query: ctx.request.query,
      params: ctx.params,
      body: ctx.request.body,
      headers: ctx.headers,
    });
  }

  getMethod(): string {
    return this.method;
  }

  getPath(): string {
    return this.path;
  }

  getSpecification(): OpenAPIV3.OperationObject {
    return {
      ...this.request,
      responses: this.response,
      description: this.description,
      tags: this.tags,
    };
  }

  getComponents(): OpenAPIV3.ComponentsObject {
    return this.components;
  }

  getError(code: string): any {
    const errorsExamples = Object.keys(this.response)
      .filter((httpCode) => parseInt(httpCode) >= 400)
      .reduce((errors, httpCode) => {
        const examples = this.getErrorExamples(parseInt(httpCode));

        examples.forEach((example) => {
          errors.push({
            httpCode: parseInt(httpCode),
            example: {
              code: example.properties.code.example,
              message: example.properties.message.example,
            },
          });
        });

        return errors;
      }, []);

    console.log('errorsExamples ', errorsExamples);

    return errorsExamples.find((error) => error?.example?.code === code) || null;
  }

  getInternalError(): any {
    const internalError = this.getErrorExamples(500).pop();

    return {
      code: internalError.properties.code.example,
      message: internalError.properties.message.example,
    };
  }

  private getErrorExamples(httpCode: number): any[] {
    const schema = (this.response[httpCode] as OpenAPIV3.ResponseObject).content['application/json'].schema as any;

    const examples = schema.properties.errors.items.oneOf;

    if (Array.isArray(examples)) {
      return examples;
    }

    return [];
  }

  protected addSchema(schemaName: string, schema: OpenAPIV3.SchemaObject): void {
    this.components.schemas[schemaName] = schema;
  }
}
