import { OpenAPIV3 } from 'openapi-types';

export interface Payload {
  description: string;
  success: OpenAPIV3.SchemaObject;
  errors: OpenAPIV3.SchemaObject[];
}
