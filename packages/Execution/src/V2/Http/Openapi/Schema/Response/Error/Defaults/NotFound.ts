import { OpenAPIV3 } from 'openapi-types';
import { Success } from '../../Success';
import { Code } from '../Code';

export const NotFound = (error: OpenAPIV3.SchemaObject): OpenAPIV3.ResponseObject => {
  return Code({
    description: 'запрашиваемый ресурс не найден',
    success: Success(false),
    errors: [error],
  });
};
