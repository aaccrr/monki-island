import { OpenAPIV3 } from 'openapi-types';

export const Success = (success: boolean): OpenAPIV3.SchemaObject => {
  return {
    type: 'boolean',
    example: success,
  };
};
