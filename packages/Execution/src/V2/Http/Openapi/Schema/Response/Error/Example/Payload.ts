export interface Payload {
  code: string;
  message?: string;
}
