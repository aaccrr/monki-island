import { OpenAPIV3 } from 'openapi-types';
import { Payload } from './Payload';

export const Example = (payload: Payload): OpenAPIV3.SchemaObject => {
  return {
    type: 'object',
    properties: {
      code: {
        type: 'string',
        example: payload.code,
      },
      message: {
        type: 'string',
        example: payload.message || '',
      },
    },
  };
};
