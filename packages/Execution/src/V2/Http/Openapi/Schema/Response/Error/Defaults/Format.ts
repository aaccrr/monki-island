import { OpenAPIV3 } from 'openapi-types';
import { Success } from '../../Success';
import { Code } from '../Code';
import { Example } from '../Example';

export const Format = (errors: OpenAPIV3.SchemaObject[] = []): OpenAPIV3.ResponseObject => {
  return Code({
    description: 'запрос не прошел форматно-логический контроль',
    success: Success(false),
    errors: [
      Example({
        code: 'format_error',
        message: 'сообщение ошибки контроля формата',
      }),
      ...errors,
    ],
  });
};
