import { OpenAPIV3 } from 'openapi-types';

export const Array = (params: Partial<OpenAPIV3.ArraySchemaObject> = {}): OpenAPIV3.SchemaObject => {
  return {
    type: 'array',
    ...(params as OpenAPIV3.ArraySchemaObject),
  };
};
