import { OpenAPIV3 } from 'openapi-types';

export const Boolean = (params: OpenAPIV3.BaseSchemaObject = {}): OpenAPIV3.SchemaObject => {
  return {
    type: 'boolean',
    ...params,
  };
};
