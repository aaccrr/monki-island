import { Instance } from './Instance';

export interface Interface {
  middleware(): Instance;
}
