import * as Parser from 'query-string';
import { Instance } from '../Instance';
import { Interface } from '../Interface';

export class QueryString implements Interface {
  middleware(): Instance {
    return async (ctx, next) => {
      ctx.request.query = await Parser.parse(ctx.request.querystring, {
        parseNumbers: false,
        parseBooleans: true,
        arrayFormat: 'comma',
      });

      if (typeof ctx.request.query.limit === 'string') {
        ctx.request.query.limit = parseInt(ctx.request.query.limit);
      }

      if (typeof ctx.request.query.offset === 'string') {
        ctx.request.query.offset = parseInt(ctx.request.query.offset);
      }

      await next();
    };
  }
}
