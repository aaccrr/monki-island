import * as Tracing from '../../../Tracing';
import * as Endpoint from '../../Endpoint';
import { Instance } from '../Instance';
import { Interface } from '../Interface';

export class Trace implements Interface {
  constructor(private tracing: Tracing.Interface, private location: Endpoint.Location) {}

  middleware(): Instance {
    return async (ctx, next) => {
      const span = this.tracing.create(`${this.location.method} ${this.location.path}`);

      span.log('payload', {
        body: ctx.request.body,
        query: ctx.request.query,
        params: ctx.params,
        auth: ctx.state._auth,
      });

      ctx.state.trace = span.extract();

      try {
        await next();

        span.finish();
      } catch (e) {
        span.fail(e);

        throw e;
      }
    };
  }
}
