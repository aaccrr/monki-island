import { Instance } from '../Instance';
import { Interface } from '../Interface';

export class Meta implements Interface {
  middleware(): Instance {
    return async (ctx, next) => {
      ctx.state.meta = {
        clientIp: ctx.headers['x-real-ip'] || ctx.headers['x-forwarded-for'] || ctx.headers['client-ip'],
      };

      await next();
    };
  }
}
