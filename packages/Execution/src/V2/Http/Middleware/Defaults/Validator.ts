import * as Openapi from '../../Openapi';
import { Instance } from '../Instance';
import { Interface } from '../Interface';

export class Validator implements Interface {
  constructor(private specification: Openapi.Specification) {}

  middleware(): Instance {
    return async (ctx, next) => {
      this.specification.validate(ctx);

      await next();
    };
  }
}
