import * as BodyParser from 'co-body';
import { Instance } from '../Instance';
import { Interface } from '../Interface';

export class Json implements Interface {
  middleware(): Instance {
    return async (ctx, next) => {
      ctx.request.body = await BodyParser.json(ctx.req);

      await next();
    };
  }
}
