import * as Openapi from '../../Openapi';
import { Instance } from '../Instance';
import { Interface } from '../Interface';

export class ErrorHandling implements Interface {
  constructor(private specification: Openapi.Specification) {}

  middleware(): Instance {
    return async (ctx, next) => {
      try {
        await next();
      } catch (e) {
        if (e.message === 'format_error') {
          ctx.status = 400;

          ctx.body = {
            success: false,
            errors: e.validationErrors.map((validationError) => {
              return {
                code: 'format_error',
                message: validationError.message,
              };
            }),
          };

          return;
        }

        const errorView = this.specification.getError(e.message);

        if (errorView !== null) {
          ctx.status = errorView.httpCode;

          ctx.body = {
            success: false,
            errors: [errorView.example],
          };

          return;
        }

        ctx.status = 500;

        ctx.body = {
          success: false,
          errors: [this.specification.getInternalError()],
        };
      }
    };
  }
}
