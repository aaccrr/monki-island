import * as Extendable from '../Extendable';
import * as Server from './Server';
import * as Endpoint from './Endpoint';
import { Interface } from './Interface';

export abstract class Instance extends Extendable.Instance implements Interface {
  private server = new Server.Instance();

  abstract onInit(): Promise<void>;

  async init(): Promise<void> {
    await this.onInit();

    this.server.extend(this);
  }

  async start(): Promise<void> {
    await this.server.startServer();
  }

  async specification(): Promise<void> {
    await this.server.createSpecification();
  }

  protected addEndpoint(endpoint: Endpoint.Constructor): void {
    this.server.addEndpoint(endpoint);
  }
}
