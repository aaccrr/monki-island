import * as Stream from 'stream';

export class Request<U = any, P = any> {
  constructor(private ctx: any) {
    this.user = this.ctx.state._auth;

    this.payload = {
      query: ctx.request.query,
      params: ctx.params,
      body: ctx.request.body,
      user: ctx.state._auth,
    } as any;
  }

  user: U;

  payload: P;

  sendHeader(name: string, value: string): void {
    this.ctx.set(name, value);
  }

  sendStatus(status: number): void {
    this.ctx.status = status;
  }

  send(response: Stream.Readable): void;

  send(response: Buffer): void;

  send(response: Object): void;

  send(response: string): void;

  send(response: void): void;

  send(response: Stream.Readable | Buffer | Object | string | void): void {
    this.ctx.body = response;
  }
}
