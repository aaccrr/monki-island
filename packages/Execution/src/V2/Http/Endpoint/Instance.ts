import * as Traceable from '../../Traceable';
import * as Openapi from '../Openapi';
import * as Request from '../Request';
import { Interface } from './Interface';
import { Location } from './Location';

export abstract class Instance extends Traceable.Instance implements Interface {
  static location: Location;

  static specification: Openapi.Specification;

  async init(): Promise<void> {
    await this.onInit();
  }

  async onInit(): Promise<void> {}

  async handleRequest(request: Request.Request): Promise<void> {
    try {
      this.trace.log('payload', request.payload);

      const response = await this.onRequest(request);

      this.trace.log('response', response);

      this.trace.finish();
    } catch (e) {
      this.trace.fail(e);

      throw e;
    }
  }

  abstract onRequest(request: Request.Request): Promise<void>;
}
