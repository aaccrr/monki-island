export interface Location {
  method: 'get' | 'post' | 'put' | 'delete';
  path: string;
}
