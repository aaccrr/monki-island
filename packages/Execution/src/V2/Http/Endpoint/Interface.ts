import * as Request from '../Request';

export interface Interface {
  init(): Promise<void>;
  handleRequest(request: Request.Request): Promise<void>;
}
