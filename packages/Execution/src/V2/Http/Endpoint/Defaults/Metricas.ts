import * as Request from '../../Request';
import { Instance } from '../Instance';
import { Location } from '../Location';

export class Metricas extends Instance {
  static location: Location = {
    method: 'get',
    path: '/metrics',
  };

  override async onRequest(request: Request.Request): Promise<void> {
    const { metrics } = this.provide();

    const metricas = await metrics.pull();

    request.send(metricas);
  }
}
