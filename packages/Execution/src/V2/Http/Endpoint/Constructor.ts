import * as Openapi from '../Openapi';
import { Instance } from './Instance';
import { Location } from './Location';

export interface Constructor {
  new (): Instance;

  location: Location;

  specification: Openapi.Specification;
}
