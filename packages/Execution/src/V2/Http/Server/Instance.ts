import * as Koa from 'koa';
import * as KoaRouter from 'koa-router';
import * as Extendable from '../../Extendable';
import * as Endpoint from '../Endpoint';
import * as Request from '../Request';
import * as Middleware from '../Middleware';
import { Specification } from './Specification';

export class Instance extends Extendable.Instance {
  private server = new Koa();

  private router = new KoaRouter();

  private endpoints: Endpoint.Constructor[] = [];

  addEndpoint(endpointConstructor: Endpoint.Constructor): void {
    this.endpoints.push(endpointConstructor);
  }

  startServer(): void {
    this.bindEndpoints();

    this.server.use(this.router.routes());

    this.server.listen(81);
  }

  async createSpecification(): Promise<void> {
    const specification = new Specification();

    this.endpoints.forEach((endpointConstructor) => {
      specification.addEndpointSpecification(endpointConstructor.specification);
    });

    await specification.flush();
  }

  private bindEndpoints(): void {
    this.router.register(
      Endpoint.Defaults.Metricas.location.path,
      [Endpoint.Defaults.Metricas.location.method],
      [
        async (ctx) => {
          const metricas = new Endpoint.Defaults.Metricas();

          metricas.extend(this);

          metricas.continue({});

          await metricas.init();

          await metricas.handleRequest(new Request.Request(ctx));
        },
      ]
    );

    this.endpoints.forEach((endpointConstructor) => {
      const middleware = [
        new Middleware.Defaults.ErrorHandling(endpointConstructor.specification).middleware(),
        new Middleware.Defaults.QueryString().middleware(),
        new Middleware.Defaults.Json().middleware(),
        new Middleware.Defaults.Trace(this.provide().tracing, endpointConstructor.location).middleware(),
        new Middleware.Defaults.Auth().middleware(),
        new Middleware.Defaults.Validator(endpointConstructor.specification).middleware(),
      ];

      middleware.push(async (ctx, next) => {
        const endpoint = new endpointConstructor();

        endpoint.extend(this);

        endpoint.continue(ctx.state.trace);

        await endpoint.init();

        await endpoint.handleRequest(new Request.Request(ctx));
      });

      this.router.register(endpointConstructor.location.path, [endpointConstructor.location.method], middleware, {});
    });
  }
}
