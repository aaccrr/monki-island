export interface Interface {
  init(): Promise<void>;
  start(): Promise<void>;
  specification(): Promise<void>;
}
