import { SignalCarrier } from './SignalCarrier';

export interface DispatcherInterface {
  dispatchSignal<T, Y = void>(name: string, signalPayload: T): Promise<Y>;
  dispatchSignalWithTrace<T, Y = void>(name: string, signalPayload: SignalCarrier<T>): Promise<Y>;
}
