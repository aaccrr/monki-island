import * as Observability from '../../Observability';

export interface SignalCarrier<T = any> {
  payload: T;
  trace: Observability.Tracing.CarrierInterface;
}
