export interface DispatcherConfiguration {
  serviceName: string;
  host: string;
}
