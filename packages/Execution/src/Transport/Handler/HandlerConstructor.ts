import * as Observability from '../../Observability';
import * as Instrumetary from '../../Instrumentary';
import { Handler } from './Handler';

export interface HandlerConstructor {
  new (DependencyContainer: Instrumetary.DependencyContainer, trace: Observability.Tracing.CarrierInterface): Handler;
}
