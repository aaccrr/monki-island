export interface Message<T = any> {
  signal: string;
  payload: T;
}
