import * as Observability from '../../Observability';
import * as Transport from '../../Transport';
import * as Instrumentary from '../../Instrumentary';
import { Adapter } from './Adapter';
import { Message } from './Message';

export class Handler<T = any, B = any> {
  constructor(private DependencyContainer: Instrumentary.DependencyContainer, private TraceCarrier: Observability.Tracing.CarrierInterface) {}

  static namespace = '';

  namespace = '';

  private Span: Observability.Tracing.SpanInterface;

  private adapters: Map<string, Adapter> = new Map();

  async initialize(): Promise<void> {
    const tracer = this.DependencyContainer.getResource<Observability.Tracing.TracerInterface>(Observability.Tracing.Tracer.name);

    this.Span = tracer.extendsFrom(this.TraceCarrier, `Handler("${this.namespace}")`);

    await this.onInit();
  }

  async onInit(): Promise<void> {}

  async onHandle(payload: T): Promise<B> {
    return {} as B;
  }

  async handle(message: Message<T>): Promise<B> {
    try {
      this.traceStart(message);

      if (this.adapters.has(message.signal)) {
        const adapter = this.adapters.get(message.signal);

        message.payload = adapter(message.payload);
      }

      this.log('adapted', message.payload);

      const result = await this.onHandle(message.payload);

      this.traceFinish(result);

      return result;
    } catch (e) {
      this.traceError(e);

      throw e;
    }
  }

  getName(): string {
    return this.constructor.name;
  }

  getDependencyContainer(): Instrumentary.DependencyContainer {
    return this.DependencyContainer;
  }

  getTrace(): Observability.Tracing.CarrierInterface {
    return this.Span.extractContext();
  }

  protected adapter(signal: string, adapter: Adapter): void {
    this.adapters.set(signal, adapter);
  }

  protected log(title: string, message: any): void {
    this.Span.addLog(title, message);
  }

  protected provide<D>(): D {
    return this.DependencyContainer as any as D;
  }

  protected dispatch<T, Y = void>(signalName: string, signalPayload: T): Promise<Y> {
    const dispatcher = this.DependencyContainer.getResource<Transport.DispatcherInterface>(Transport.Dispatcher.name);

    return dispatcher.dispatchSignalWithTrace<T, Y>(signalName, {
      payload: signalPayload,
      trace: this.getTrace(),
    });
  }

  private traceStart(payload: any): void {
    this.Span.addLog('payload', payload);
  }

  private traceFinish(result: any): void {
    this.Span.withResponse(result);
  }

  private traceError(error: any): void {
    this.Span.withError(error);
  }
}
