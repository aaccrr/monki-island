export interface TransporterConfiguration {
  serviceName: string;
  host: string;
}
