import { EventEmitter } from 'events';
import { TransporterConfiguration } from './TransporterConfiguration';

export interface TransporterInterface extends EventEmitter {
  init(config: TransporterConfiguration): Promise<void>;
  publish<T>(signalName: string, signalPayload: T): Promise<void>;
  request<T = any, Y = any>(signalName: string, signalPayload: T): Promise<Y>;
  subscribe(signalName: string): void;
  disconnect(): void;
}
