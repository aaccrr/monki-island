import { EventEmitter } from 'events';
import { connect, NatsConnection, Codec, JSONCodec } from 'nats';
import { TransporterConfiguration } from './TransporterConfiguration';
import { TransporterInterface } from './TransporterInterface';

export class Transporter extends EventEmitter implements TransporterInterface {
  private connection: NatsConnection;
  private JSONCodec: Codec<any> = JSONCodec();
  private groupName: string;

  async init(config: TransporterConfiguration): Promise<void> {
    this.groupName = config.serviceName;

    try {
      this.connection = await connect({
        servers: config.host,
        name: this.groupName,
      });
    } catch (e) {
      console.log('nats connection error: ', e);
    }
  }

  disconnect(): void {}

  async publish<T>(signal: string, signalPayload: T): Promise<void> {
    await this.connection.publish(signal, this.JSONCodec.encode(signalPayload));
  }

  async request<T = any, Y = any>(signalName: string, signalPayload: T): Promise<Y> {
    const responseSignal = await this.connection.request(signalName, this.JSONCodec.encode(signalPayload), {
      timeout: 30000,
    });

    return this.JSONCodec.decode(responseSignal.data);
  }

  subscribe(signalName: string): void {
    this.connection.subscribe(signalName, {
      queue: this.groupName,
      callback: (err, signal) => {
        const repondCallback = (response: any): void => {
          signal.respond(this.JSONCodec.encode(response));
        };

        const message = this.JSONCodec.decode(signal.data);

        this.emit(signalName, message, repondCallback);
      },
    });
  }
}
