import * as Instrumentary from '../../Instrumentary';
import { Transporter } from '../Transporter';
import { HandlerConstructor } from '../Handler';

export class Overlay {
  constructor(private DependencyContainer: Instrumentary.DependencyContainer) {
    this.Transporter = this.DependencyContainer.getResource<Transporter>(Transporter.name);
  }

  private Transporter: Transporter;

  addHandler(signal: string, Handler: HandlerConstructor): void {
    this.Transporter.on(signal, async (message, respond) => {
      try {
        const handler = new Handler(this.DependencyContainer, message.trace);

        await handler.initialize();

        const result = await handler.handle({
          signal,
          payload: message.payload,
        });

        respond(result);
      } catch (e) {
        respond(e);
      }
    });

    this.Transporter.subscribe(signal);
  }
}
