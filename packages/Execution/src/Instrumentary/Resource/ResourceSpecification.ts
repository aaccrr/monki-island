export interface ResourceSpecification<T = any, C = any> {
  resource: T;
  namespace?: string;
  configuration: C;
  require: string[];
}
