import { DependencyContainer } from '../DependencyContainer';
import { ResourceSpecification } from './ResourceSpecification';

export class Resource<T = any, C = any> {
  constructor(private specification: ResourceSpecification<T, C>) {
    this.resource = specification.resource;
  }

  public isInited = false;

  protected resource: T;

  async init(required: any[]): Promise<void> {
    if (!this.isInited) {
      const constructor = this.specification.resource as any;

      this.resource = new constructor(...required);

      if (typeof (this.resource as any).init === 'function') {
        await (this.resource as any).init(this.specification.configuration);
      }

      this.isInited = true;
    }
  }

  getInstance(): T {
    return this.resource;
  }

  getName(): string {
    const name = (this.resource as any).name;

    if (typeof this.specification.namespace === 'string') {
      return `${this.specification.namespace}.${name}`;
    }

    return name;
  }

  getRequire(): string[] {
    return this.specification.require.filter((require) => require !== DependencyContainer.name);
  }

  requireContainer(): boolean {
    return this.specification.require.includes(DependencyContainer.name);
  }
}
