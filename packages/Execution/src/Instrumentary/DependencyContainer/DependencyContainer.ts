import { Nullable } from '@monki-island/common';
import { Resource } from '../Resource';

export class DependencyContainer {
  private resources: Map<string, Resource> = new Map();

  async init(): Promise<void> {
    for (const resource of this.resources.values()) {
      await this.initResource(resource);
    }
  }

  getResource<R>(resourceName: string): Nullable<R> {
    if (this.resources.has(resourceName)) {
      return this.resources.get(resourceName).getInstance();
    }

    return null;
  }

  protected async initResource(resource: Resource): Promise<void> {
    const require = resource.getRequire().map((required) => this.resources.get(required));

    for (const requiredResource of require) {
      await this.initResource(requiredResource);
    }

    const required = require.map((requiredResource) => requiredResource.getInstance());

    if (resource.requireContainer()) {
      required.push(this);
    }

    await resource.init(required);
  }

  protected addResource(resource: Resource): void {
    this.resources.set(resource.getName(), resource);
  }
}
