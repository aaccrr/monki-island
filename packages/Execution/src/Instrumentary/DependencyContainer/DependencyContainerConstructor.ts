import { DependencyContainer } from '../DependencyContainer';

export interface DependencyContainerConstructor {
  new (): DependencyContainer;
}
