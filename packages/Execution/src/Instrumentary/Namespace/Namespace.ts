export const Namespace = (filename: string) => {
  return <T extends { new (...args: any[]): {} }>(constructor: T) => {
    const namespace = filename.replace('/app/src/', '').replace('/app/dist/', '').replace('.ts', '').replace('.js', '').split('/').join('.');

    return class extends constructor {
      static namespace = namespace;
      namespace = namespace;
    };
  };
};
