import * as Observability from '../../Observability';
import { DependencyContainer } from '../DependencyContainer';

export interface Extendable {
  getDependencyContainer(): DependencyContainer;
  getTrace(): Observability.Tracing.CarrierInterface;
}
