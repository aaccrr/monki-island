import * as Observability from '../../Observability';
import * as Transport from '../../Transport';
import { DependencyContainer } from '../DependencyContainer';
import { Extendable } from '../Extendable';

export class Fiber<T = any, B = any> {
  constructor(private DependencyContainer: DependencyContainer, private TraceCarrier: Observability.Tracing.CarrierInterface) {}

  static namespace = '';

  namespace = '';

  private Span: Observability.Tracing.SpanInterface;

  private MetricsStore: Observability.Metrics.MetricsStore;

  protected traceable = true;

  protected metrics = true;

  static extend<T extends Fiber>(this: new (DependencyContainer, TraceCarrier) => T, parent: Extendable): T {
    const fiber = new this(parent.getDependencyContainer(), parent.getTrace());

    fiber.initTracing();

    fiber.initMetricsStore();

    return fiber;
  }

  async onExecution(payload: T): Promise<B> {
    return {} as B;
  }

  async execute(payload: T): Promise<B> {
    try {
      this.traceStart(payload);
      const result = await this.onExecution(payload);
      this.traceFinish(result);
      return result;
    } catch (e) {
      this.traceError(e);
      throw e;
    }
  }

  initTracing(): void {
    if (this.traceable) {
      const tracer = this.DependencyContainer.getResource<Observability.Tracing.TracerInterface>(Observability.Tracing.Tracer.name);
      this.Span = tracer.extendsFrom(this.TraceCarrier, `Fiber("${this.namespace}")`);
    }
  }

  protected traceStart(payload: any): void {
    if (this.traceable) {
      this.Span.addLog('payload', payload);
    }
  }

  protected traceFinish(result: any): void {
    if (this.traceable) {
      this.Span.withResponse(result);
    }
  }

  protected traceError(error: any): void {
    if (this.traceable) {
      this.Span.withError(error);
    }
  }

  initMetricsStore(): void {
    if (this.metrics) {
      this.MetricsStore = this.DependencyContainer.getResource(Observability.Metrics.MetricsStore.name);
    }
  }

  getDependencyContainer(): DependencyContainer {
    return this.DependencyContainer;
  }

  getTrace(): Observability.Tracing.CarrierInterface {
    if (this.traceable) {
      return this.Span.extractContext();
    } else {
      return {};
    }
  }

  protected log(title: string, message: any): void {
    if (this.traceable) {
      this.Span.addLog(title, message);
    }
  }

  protected async metric(sample: Observability.Metrics.SampleInterface): Promise<void> {
    await this.MetricsStore.addSample(sample);
  }

  protected provide<D>(): D {
    return this.DependencyContainer as any as D;
  }

  protected dispatch<T, Y = void>(signalName: string, signalPayload: T): Promise<Y> {
    const dispatcher = this.DependencyContainer.getResource<Transport.DispatcherInterface>(Transport.Dispatcher.name);

    return dispatcher.dispatchSignalWithTrace<T, Y>(signalName, {
      payload: signalPayload,
      trace: this.getTrace(),
    });
  }
}
