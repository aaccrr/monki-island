import * as Observability from '../../Observability';
import { DependencyContainer } from '../DependencyContainer';
import { Fiber } from './Fiber';

export interface FiberConstructor {
  new (DependencyContainer: DependencyContainer, trace: Observability.Tracing.CarrierInterface): Fiber;
  namespace: string;
}
