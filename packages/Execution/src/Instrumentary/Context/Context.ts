import { EventEmitter } from 'events';
import * as Transport from '../../Transport';
import * as Http from '../../Http';
import * as Scheduling from '../../Scheduling';
import { DependencyContainer, DependencyContainerConstructor } from '../DependencyContainer';

export abstract class Context extends EventEmitter {
  protected DependencyContainer: DependencyContainerConstructor;

  protected Overlay: Transport.Overlay;

  protected Http: Http.Server.Server;

  protected Scheduler: Scheduling.Scheduler;

  private _DependencyContainer: DependencyContainer;

  async start(): Promise<void> {
    switch (this.getAppMode()) {
      case 'run_app': {
        await this.run();

        break;
      }

      case 'create_specification': {
        this.createHttpServer();

        await this.initializeHttp();

        await this.Http.createSpecification();

        break;
      }

      default: {
        process.exit();
      }
    }
  }

  async run(): Promise<void> {
    await this.initializeDependencyContainer();

    this.createTransport();

    this.startScheduler();

    this.createHttpServer();

    await this.initializeHttp();

    await this.initializeOverlay();

    await this.initializeScheduler();

    this.emit('initialized', this._DependencyContainer);

    this.startHttpServer();
  }

  async initializeDependencyContainer(): Promise<void> {
    this._DependencyContainer = new this.DependencyContainer();

    await this._DependencyContainer.init();
  }

  createTransport(): void {
    this.Overlay = new Transport.Overlay(this._DependencyContainer);
  }

  createHttpServer(): void {
    this.Http = new Http.Server.Server(this._DependencyContainer);
  }

  startHttpServer(): void {
    this.Http.startServer();
  }

  startScheduler(): void {
    const scheduler = this._DependencyContainer.getResource<Scheduling.Scheduler>(Scheduling.Scheduler.name);

    if (scheduler) {
      this.Scheduler = scheduler;
    }
  }

  protected async initializeHttp(): Promise<void> {}

  protected async initializeOverlay(): Promise<void> {}

  protected async initializeScheduler(): Promise<void> {}

  private getAppMode(): 'run_app' | 'create_specification' {
    if (process.env.MODE === 'run_app') {
      return 'run_app';
    }

    if (process.env.MODE === 'create_specification') {
      return 'create_specification';
    }
  }
}
