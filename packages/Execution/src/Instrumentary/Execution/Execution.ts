import * as Observability from '../../Observability';
import { DependencyContainer } from '../DependencyContainer';
import { Extendable } from '../Extendable';

export class Execution<P = any, R = any> implements Extendable {
  constructor(private DependencyContainer: DependencyContainer, private TraceCarrier: Observability.Tracing.CarrierInterface) {}

  private Span: Observability.Tracing.SpanInterface;

  static extend<T extends Execution>(this: new (DependencyContainer, TraceCarrier) => T, parent: Extendable): T {
    const execution = new this(parent.getDependencyContainer(), parent.getTrace());
    execution.onInit();
    return execution;
  }

  getName(): string {
    return this.constructor.name;
  }

  async onPerform(payload: P): Promise<R> {
    return {} as R;
  }

  async perform(payload: P): Promise<R> {
    try {
      this.Span.addLog('payload', payload);
      const result = await this.onPerform(payload);
      this.Span.withResponse(result);
      return result;
    } catch (e) {
      this.Span.withError(e);
      throw e;
    }
  }

  onInit(): void {
    const tracer = this.DependencyContainer.getResource<Observability.Tracing.TracerInterface>(Observability.Tracing.Tracer.name);
    this.Span = tracer.extendsFrom(this.TraceCarrier, `Execution::${this.constructor.name}`);
  }

  getTrace(): Observability.Tracing.CarrierInterface {
    return this.Span.extractContext();
  }

  getDependencyContainer(): DependencyContainer {
    return this.DependencyContainer;
  }
}
