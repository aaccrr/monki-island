import * as Observability from '../../Observability';
import { DependencyContainer } from '../DependencyContainer';
import { Execution } from './Execution';

export interface ExecutionConstructor {
  new (dependencyContainer: DependencyContainer, TraceCarrier: Observability.Tracing.CarrierInterface): Execution;
}
