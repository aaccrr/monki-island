import * as prom from 'prom-client';
import { MetricsStoreInterface } from '.';
import { MetricType } from '../MetricType';
import { SampleInterface } from '../Sample';

export class MetricsStore implements MetricsStoreInterface {
  async addSample(sample: SampleInterface): Promise<void> {
    const metricType = sample.getMetricType();

    if (metricType === MetricType.Counter) {
      await this.updateCounter(sample);
    }
  }

  pullMetrics(): Promise<string> {
    return prom.register.metrics();
  }

  private async updateCounter(sample: SampleInterface): Promise<void> {
    const metric = await this.ensureCounterFor(sample);
    metric.inc(sample.getLabels());
  }

  private async ensureCounterFor(sample: SampleInterface): Promise<prom.Counter<any>> {
    const storedMetric = await prom.register.getSingleMetric(sample.getMetricName());

    if (!storedMetric) {
      return new prom.Counter({
        name: sample.getMetricName(),
        help: sample.getMetricDescription(),
        labelNames: sample.getLabelsNames(),
      });
    } else {
      return storedMetric as prom.Counter<any>;
    }
  }
}
