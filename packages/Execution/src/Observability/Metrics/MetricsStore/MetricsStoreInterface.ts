import { SampleInterface } from '../Sample';

export interface MetricsStoreInterface {
  addSample(sample: SampleInterface): Promise<void>;
  pullMetrics(): Promise<string>;
}
