import { MetricType } from '../MetricType';

export interface SampleInterface<L = any> {
  getMetricName(): string;
  getMetricType(): MetricType;
  getLabelsNames(): string[];
  getMetricDescription(): string;
  getLabels(): L;
}
