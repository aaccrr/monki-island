import { SampleInterface } from './SampleInterface';
import { MetricType } from '../MetricType';

export class Sample<L = any> implements SampleInterface<L> {
  protected metricType: MetricType;
  protected labels: L;

  getMetricName(): string {
    return this.constructor.name;
  }

  getMetricType(): MetricType {
    return this.metricType;
  }

  getLabelsNames(): string[] {
    const labels = this.labels || {};
    return Object.keys(labels);
  }

  getMetricDescription(): string {
    return this.getMetricName().split('_').join(' ');
  }

  getLabels(): L {
    return this.labels;
  }
}
