export enum MetricType {
  Counter,
  Gauge,
  Histogram,
  Summary,
}
