export interface SpanInterface {
  addLog(name: string, body: any): void;
  withError(error: any): void;
  withResponse(response: any): void;
  markError(): void;
  extractContext(): any;
  finish(): void;
}
