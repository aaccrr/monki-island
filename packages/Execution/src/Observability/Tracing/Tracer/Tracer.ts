import { Tracer as OpenTracer, FORMAT_TEXT_MAP, followsFrom } from 'opentracing';
import { initTracer, ReporterConfig } from 'jaeger-client';
import { TracerInterface } from './TracerInterface';
import { TracerConfiguration, TracerConfigurationReporter } from './TracerConfiguration';
import { Span, SpanInterface } from '../Span';
import { CarrierInterface } from '../Carrier';

export class Tracer implements TracerInterface {
  private _tracer: OpenTracer;

  async init(configuration: TracerConfiguration): Promise<void> {
    this._tracer = initTracer(
      {
        serviceName: configuration.serviceName,
        reporter: {
          logSpans: true,
          ...this.getReporterConnectionConfig(configuration.reporter),
        },
        sampler: {
          type: 'const',
          param: 1,
        },
      },
      {}
    );
  }

  createNewSpan(title: string): SpanInterface {
    return new Span(this._tracer.startSpan(title));
  }

  createLinkOn(target: CarrierInterface, title: string): SpanInterface {
    const referenceSpan = this._tracer.extract(FORMAT_TEXT_MAP, target);
    return new Span(this._tracer.startSpan(title, { references: [followsFrom(referenceSpan)] }));
  }

  extendsFrom(parent: CarrierInterface, title: string): SpanInterface {
    const parentSpan = this._tracer.extract(FORMAT_TEXT_MAP, parent);
    return new Span(this._tracer.startSpan(title, { childOf: parentSpan }));
  }

  private getReporterConnectionConfig(reporterConfig: TracerConfigurationReporter): Partial<ReporterConfig> {
    if (reporterConfig.agent) {
      const [agentHost, agentPort] = reporterConfig.agent.host.split(':');

      return {
        agentHost,
        agentPort: parseInt(agentPort),
      };
    }

    return {
      collectorEndpoint: reporterConfig.collector.host,
    };
  }
}
