import { TracerConfiguration } from './TracerConfiguration';
import { CarrierInterface } from '../Carrier';
import { SpanInterface } from '../Span/SpanInterface';

export interface TracerInterface {
  init(configuration: TracerConfiguration): Promise<void>;
  createNewSpan(title: string): SpanInterface;
  createLinkOn(target: CarrierInterface, title: string): SpanInterface;
  extendsFrom(parent: CarrierInterface, title: string): SpanInterface;
}
