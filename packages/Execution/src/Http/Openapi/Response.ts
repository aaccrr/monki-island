import { OpenAPIV3 } from 'openapi-types';

export type Response = Partial<OpenAPIV3.ResponsesObject>;
