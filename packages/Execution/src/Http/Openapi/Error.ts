export interface Error {
  code: number;
  example: {
    code: string;
    message: string;
  };
}
