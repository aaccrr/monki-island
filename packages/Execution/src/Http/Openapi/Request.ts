import { OpenAPIV3 } from 'openapi-types';

export type Request = Partial<OpenAPIV3.OperationObject>;
