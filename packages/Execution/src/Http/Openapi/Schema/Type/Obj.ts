import { OpenAPIV3 } from 'openapi-types';

export const Obj = (params: OpenAPIV3.BaseSchemaObject = {}): OpenAPIV3.SchemaObject => {
  return {
    type: 'object',
    additionalProperties: false,
    ...params,
  };
};
