import { OpenAPIV3 } from 'openapi-types';

export const String = (params: OpenAPIV3.BaseSchemaObject = {}): OpenAPIV3.SchemaObject => {
  return {
    type: 'string',
    ...params,
  };
};
