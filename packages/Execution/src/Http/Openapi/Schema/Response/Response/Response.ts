import { OpenAPIV3 } from 'openapi-types';
import { Payload } from './Payload';

export const Response = (payload: Payload): OpenAPIV3.ResponseObject => {
  const schema: OpenAPIV3.ResponseObject = {
    description: payload.description || 'успешное выполнение запроса',
    headers: {},
    content: {
      [payload.contentType || 'application/json']: {
        schema: payload.schema,
      },
    },
  };

  if (Array.isArray(payload.headers)) {
    for (const header of payload.headers) {
      schema.headers = {
        ...schema.headers,
        ...header,
      };
    }
  }

  return schema;
};
