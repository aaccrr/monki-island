import { OpenAPIV3 } from 'openapi-types';

export interface Payload {
  schema: OpenAPIV3.SchemaObject;
  description?: string;
  contentType?: string;
  headers?: {
    [header: string]: OpenAPIV3.HeaderObject;
  }[];
}
