import { OpenAPIV3 } from 'openapi-types';
import { Success } from '../../Success';
import { Code } from '../Code';
import { Example } from '../Example';

export const Internal = (): OpenAPIV3.ResponseObject => {
  return Code({
    description: 'внутренняя ошибка сервера',
    success: Success(false),
    errors: [
      Example({
        code: 'internal_error',
        message: 'внутренняя ошибка сервера',
      }),
    ],
  });
};
