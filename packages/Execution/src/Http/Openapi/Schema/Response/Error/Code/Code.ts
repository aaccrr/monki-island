import { OpenAPIV3 } from 'openapi-types';
import { Payload } from './Payload';

export const Code = (payload: Payload): OpenAPIV3.ResponseObject => {
  return {
    description: payload.description,
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            success: payload.success,
            errors: {
              type: 'array',
              items: {
                oneOf: payload.errors,
              },
            },
          },
        },
      },
    },
  };
};
