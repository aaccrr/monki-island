import { OpenAPIV3 } from 'openapi-types';
import { Payload } from './Payload';

export const Header = (
  payload: Payload
): {
  [header: string]: OpenAPIV3.HeaderObject;
} => {
  return {
    [payload.name]: {
      schema: {
        type: 'string',
        description: payload.description,
        example: payload.example,
      },
    },
  };
};
