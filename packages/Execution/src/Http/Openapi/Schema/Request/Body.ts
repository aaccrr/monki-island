import { OpenAPIV3 } from 'openapi-types';

export const Body = (schema: OpenAPIV3.SchemaObject): OpenAPIV3.RequestBodyObject => {
  return {
    content: {
      'application/json': {
        schema,
      },
    },
  };
};
