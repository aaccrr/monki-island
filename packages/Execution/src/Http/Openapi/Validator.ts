import { OpenAPIV3 } from 'openapi-types';
import { default as OpenAPIRequestValidator } from 'openapi-request-validator';
import { Request } from './Request';
import { CheckHostname, DATE_REGEXP, EMAIL_REGEXP, ONLY_DIGITS_FORMAT_REGEXP, PHONE_REGEXP, TIME_REGEXP } from './Format';

export class Validator {
  constructor(openApiSchema: Request) {
    this.validator = new OpenAPIRequestValidator({
      parameters: openApiSchema.parameters,
      requestBody: openApiSchema.requestBody as OpenAPIV3.RequestBodyObject,
      customFormats: this.addFormats(),
      errorTransformer: this.addErrorsTransformer(),
    });
  }

  private validator: OpenAPIRequestValidator;

  validate(validationPayload: any): void {
    const validationErrors = this.validator.validateRequest(validationPayload);

    if (typeof validationErrors !== 'undefined' && Array.isArray(validationErrors.errors)) {
      const error = new Error('format_error');

      (error as any).validationErrors = validationErrors.errors;

      throw error;
    }
  }

  private addErrorsTransformer(): any {
    return (errors, ajvError) => {
      return {
        message: ajvError.message,
      };
    };
  }

  private addFormats(): { [formatName: string]: any } {
    return {
      hostname: CheckHostname,
      digits: (input) => new RegExp(ONLY_DIGITS_FORMAT_REGEXP).test(input),
      email: (input) => new RegExp(EMAIL_REGEXP).test(input),
      phone: (input) => new RegExp(PHONE_REGEXP).test(input),
      international_string_date: (input) => new RegExp(DATE_REGEXP).test(input),
      date: (input) => new RegExp(DATE_REGEXP).test(input),
      time: (input) => new RegExp(TIME_REGEXP).test(input),
    };
  }
}
