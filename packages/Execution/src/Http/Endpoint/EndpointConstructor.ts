import * as Instrumentary from '../../Instrumentary';
import * as Observability from '../../Observability';
import * as Openapi from '../Openapi';
import { Endpoint } from './Endpoint';
import { EndpointLocation } from './EndpointLocation';

export interface EndpointConstructor {
  new (DependencyContainer: Instrumentary.DependencyContainer, TraceCarrier: Observability.Tracing.CarrierInterface): Endpoint;

  location: EndpointLocation;

  specification: Openapi.Specification;
}
