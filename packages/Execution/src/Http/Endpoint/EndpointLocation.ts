export interface EndpointLocation {
  method: 'get' | 'post' | 'put' | 'delete';
  path: string;
}
