import * as Instrumentary from '../../Instrumentary';
import * as Observability from '../../Observability';
import * as Transport from '../../Transport';
import * as Openapi from '../Openapi';
import * as Request from '../Request';
import { EndpointLocation } from './EndpointLocation';

export abstract class Endpoint {
  constructor(private DependencyContainer: Instrumentary.DependencyContainer, private TraceCarrier: Observability.Tracing.CarrierInterface) {}

  static location: EndpointLocation;

  static specification: Openapi.Specification;

  static namespace: string;

  public namespace: string;

  private span: Observability.Tracing.SpanInterface;

  async init(): Promise<void> {
    const tracer = this.DependencyContainer.getResource<Observability.Tracing.TracerInterface>(Observability.Tracing.Tracer.name);

    this.span = tracer.extendsFrom(this.TraceCarrier, `Endpoint("${this.namespace}")`);

    await this.onInit();
  }

  async onInit(): Promise<void> {}

  async handleRequest(request: Request.Request): Promise<void> {
    try {
      this.span.addLog('payload', request.payload);

      await this.onRequest(request);
    } catch (e) {
      this.span.withError(e);

      throw e;
    } finally {
      this.span.finish();
    }
  }

  abstract onRequest(request: Request.Request): Promise<void>;

  getTrace(): Observability.Tracing.CarrierInterface {
    return this.span.extractContext();
  }

  getDependencyContainer(): Instrumentary.DependencyContainer {
    return this.DependencyContainer;
  }

  protected provide<D>(): D {
    return this.DependencyContainer as any as D;
  }

  protected dispatch<T, Y = void>(signalName: string, signalPayload: T): Promise<Y> {
    const dispatcher = this.DependencyContainer.getResource<Transport.DispatcherInterface>(Transport.Dispatcher.name);

    return dispatcher.dispatchSignalWithTrace<T, Y>(signalName, {
      payload: signalPayload,
      trace: this.getTrace(),
    });
  }
}
