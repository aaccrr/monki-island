import * as Instrumentary from '../../../Instrumentary';
import { MetricsStore, MetricsStoreInterface } from '../../../Observability/Metrics/MetricsStore';
import { Endpoint, EndpointLocation } from '../../Endpoint';
import * as Request from '../../Request';

export class Metrics extends Endpoint {
  static location: EndpointLocation = {
    method: 'get',
    path: '/metrics',
  };

  async onRequest(request: Request.Request): Promise<void> {
    const metricsStore = this.provide<Instrumentary.DependencyContainer>().getResource<MetricsStoreInterface>(MetricsStore.name);

    const metrics = await metricsStore.pullMetrics();

    request.send(metrics);
  }
}
