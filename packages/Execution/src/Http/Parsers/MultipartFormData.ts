import { IncomingMessage } from 'http';
import * as busboy from 'busboy';

export class MultipartFormData {
  parse(message: IncomingMessage): Promise<any> {
    return new Promise((resolve, reject) => {
      const result = {
        fields: {},
        files: {},
      };

      const parser = busboy({ headers: message.headers as any });

      parser
        .on('field', (fieldName, value) => {
          result.fields[fieldName] = value;
        })
        .on('file', (name, file, info) => {
          result.files[name] = file;
        })
        .on('close', () => resolve(result))
        .on('error', reject);

      message.pipe(parser);
    });
  }
}
