import { Middleware } from './Middleware';

export interface MiddlewareInterface {
  middleware(): Middleware;
}
