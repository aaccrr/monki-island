import * as Parsers from '../../Parsers';
import { Middleware } from '../Middleware';
import { MiddlewareInterface } from '../MiddlewareInterface';

export class Multipart implements MiddlewareInterface {
  middleware(): Middleware {
    return async (ctx, next) => {
      const multipartParser = new Parsers.MultipartFormData();

      ctx.state.multipart = await multipartParser.parse(ctx.req);

      await next();
    };
  }
}
