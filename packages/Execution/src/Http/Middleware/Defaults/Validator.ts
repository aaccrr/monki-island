import * as Openapi from '../../Openapi';
import { Middleware } from '../Middleware';
import { MiddlewareInterface } from '../MiddlewareInterface';

export class Validator implements MiddlewareInterface {
  constructor(private specification: Openapi.Specification) {}

  middleware(): Middleware {
    return async (ctx, next) => {
      this.specification.validate(ctx);

      await next();
    };
  }
}
