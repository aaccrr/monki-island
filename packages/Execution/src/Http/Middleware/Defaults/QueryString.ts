import * as Parser from 'query-string';
import { Middleware } from '../Middleware';
import { MiddlewareInterface } from '../MiddlewareInterface';

export class QueryString implements MiddlewareInterface {
  middleware(): Middleware {
    return async (ctx, next) => {
      ctx.request.query = await Parser.parse(ctx.request.querystring, {
        arrayFormat: 'comma',
        parseNumbers: false,
        parseBooleans: true,
      });

      await next();
    };
  }
}
