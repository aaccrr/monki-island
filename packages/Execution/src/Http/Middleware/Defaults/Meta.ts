import { Middleware } from '../Middleware';
import { MiddlewareInterface } from '../MiddlewareInterface';

export class Meta implements MiddlewareInterface {
  middleware(): Middleware {
    return async (ctx, next) => {
      ctx.state.meta = {
        clientIp: ctx.headers['x-real-ip'] || ctx.headers['x-forwarded-for'] || ctx.headers['client-ip'],
      };

      await next();
    };
  }
}
