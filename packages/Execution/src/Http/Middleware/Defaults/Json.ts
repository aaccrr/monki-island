import * as BodyParser from 'co-body';
import { Middleware } from '../Middleware';
import { MiddlewareInterface } from '../MiddlewareInterface';

export class Json implements MiddlewareInterface {
  middleware(): Middleware {
    return async (ctx, next) => {
      ctx.request.body = await BodyParser.json(ctx.req);

      await next();
    };
  }
}
