import * as Observability from '../../../Observability';
import * as Instrumentary from '../../../Instrumentary';
import { EndpointLocation } from '../../Endpoint/EndpointLocation';
import { MiddlewareInterface } from '../MiddlewareInterface';
import { Middleware } from '../Middleware';

export class Tracing implements MiddlewareInterface {
  constructor(private DependencyContainer: Instrumentary.DependencyContainer, private location: EndpointLocation) {}

  middleware(): Middleware {
    return async (ctx, next) => {
      const tracer = this.DependencyContainer.getResource<Observability.Tracing.TracerInterface>(Observability.Tracing.Tracer.name);

      const span = tracer.createNewSpan(`${this.location.method} ${this.location.path}`);

      span.addLog('payload', {
        body: ctx.request.body,
        query: ctx.request.query,
        params: ctx.params,
        auth: ctx.state._auth,
      });

      ctx.state.trace = span.extractContext();

      try {
        await next();
        span.withResponse(undefined);
      } catch (e) {
        span.withError(e);
        throw e;
      }
    };
  }
}
