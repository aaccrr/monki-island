import * as Koa from 'koa';
import * as KoaRouter from 'koa-router';
import * as Instrumentary from '../../Instrumentary';
import { EndpointConstructor } from '../Endpoint';
import * as Request from '../Request';
import * as Middleware from '../Middleware';
import { Specification } from './Specification';

export class Server {
  constructor(private DependencyContainer: Instrumentary.DependencyContainer) {
    this.server = new Koa();
    this.router = new KoaRouter();
  }

  private server: Koa;

  private router: KoaRouter;

  private endpoints: EndpointConstructor[] = [];

  addEndpoint(endpointConstructor: EndpointConstructor): void {
    this.endpoints.push(endpointConstructor);
  }

  startServer(): void {
    this.bindEndpoints();

    this.server.use(this.router.routes());

    this.server.listen(81);
  }

  async createSpecification(): Promise<void> {
    const specification = new Specification();

    this.endpoints.forEach((endpointConstructor) => {
      specification.addEndpointSpecification(endpointConstructor.specification);
    });

    await specification.flush();
  }

  private bindEndpoints(): void {
    this.endpoints.forEach((endpointConstructor) => {
      const middleware = [
        new Middleware.Defaults.ErrorHandling(endpointConstructor.specification).middleware(),
        new Middleware.Defaults.Tracing(this.DependencyContainer, endpointConstructor.location).middleware(),
        new Middleware.Defaults.Auth().middleware(),
        new Middleware.Defaults.Json().middleware(),
        new Middleware.Defaults.QueryString().middleware(),
        new Middleware.Defaults.Validator(endpointConstructor.specification).middleware(),
      ];

      middleware.push(async (ctx, next) => {
        const endpoint = new endpointConstructor(this.DependencyContainer, ctx.state.trace);

        await endpoint.init();

        await endpoint.handleRequest(new Request.Request(ctx));
      });

      this.router.register(endpointConstructor.location.path, [endpointConstructor.location.method], middleware, {});
    });
  }
}
