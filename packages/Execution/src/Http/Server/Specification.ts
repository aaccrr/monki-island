import * as fs from 'fs';
import * as path from 'path';
import { OpenAPIV3 } from 'openapi-types';
import * as Openapi from '../Openapi';

export class Specification {
  constructor() {
    this.specification = {
      openapi: '3.0.0',
      info: {
        title: '',
        version: '1.0.0',
      },
      paths: {},
      components: {
        schemas: {},
      },
    };
  }

  private specification: Partial<OpenAPIV3.Document>;

  addEndpointSpecification(endpointSpecification: Openapi.Specification): void {
    this.addSchemas(endpointSpecification);

    if (typeof this.specification.paths[endpointSpecification.getPath()] === 'undefined') {
      this.specification.paths[endpointSpecification.getPath()] = {};
    }

    this.specification.paths[endpointSpecification.getPath()][endpointSpecification.getMethod()] = endpointSpecification.getSpecification();
  }

  async flush(): Promise<void> {
    const openapiDirectory = path.resolve(process.cwd(), './openapi');

    const isOpenapiDirectoryExists = fs.existsSync(openapiDirectory);

    if (!isOpenapiDirectoryExists) {
      await fs.promises.mkdir(openapiDirectory, { recursive: true });
    }

    await fs.promises.writeFile(`${openapiDirectory}/spec.json`, JSON.stringify(this.specification));
  }

  private addSchemas(endpointSpecification: Openapi.Specification): void {
    const components = endpointSpecification.getComponents();

    const schemas = components.schemas || {};

    Object.keys(schemas).forEach((schemaName) => {
      if (this.specification.components.schemas[schemaName] === undefined) {
        this.specification.components.schemas[schemaName] = schemas[schemaName];
      }
    });
  }
}
