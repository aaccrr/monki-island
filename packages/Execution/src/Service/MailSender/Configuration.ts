export interface Configuration {
  hostname: string;
  username: string;
  password: string;
  port: number;
}
