import * as Stream from 'stream';

export interface Attachment {
  filename: string;
  content: string | Buffer | Stream.Readable;
}
