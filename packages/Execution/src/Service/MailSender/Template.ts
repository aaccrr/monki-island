import { Attachment } from './Attachment';

export interface Template {
  from: string;
  to: string | string[];
  subject: string;
  text?: string;
  attachments?: Attachment[];
  html?: string;
}
