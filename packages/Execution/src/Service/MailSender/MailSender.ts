import * as Nodemailer from 'nodemailer';
import { Configuration } from './Configuration';
import { Template } from './Template';

export class MailSender {
  private transport: Nodemailer.Transporter;

  async init(configuration: Configuration): Promise<void> {
    this.transport = Nodemailer.createTransport({
      host: configuration.hostname,
      auth: {
        user: configuration.username,
        pass: configuration.password,
      },
      port: configuration.port,
    });
  }

  async send(template: Template): Promise<void> {
    try {
      await this.transport.sendMail(template);
    } catch (e) {
      throw e;
    }
  }
}
