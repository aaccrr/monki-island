import * as puppeteer from 'puppeteer';

export class Printer {
  private browser: puppeteer.Browser;

  async init(): Promise<void> {
    this.browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-dev-shm-usage'],
    });
  }

  async print(template: string): Promise<Buffer> {
    const page = await this.browser.newPage();

    await page.setViewport({ width: 794, height: 1122, deviceScaleFactor: 2 });

    await page.setContent(template);

    const printedPdf = await page.pdf({ format: 'a4', printBackground: true });

    await page.close();

    return printedPdf;
  }
}
