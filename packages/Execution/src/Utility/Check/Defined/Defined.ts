export const Defined = (variable: any): boolean => {
  return typeof variable !== 'undefined';
};
