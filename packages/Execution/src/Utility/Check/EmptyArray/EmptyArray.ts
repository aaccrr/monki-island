export const EmptyArray = (array: any[]): boolean => {
  return Array.isArray(array) && array.length === 0;
};
