export const IsArray = (array: any[]): boolean => {
  if (!Array.isArray(array)) {
    return false;
  }

  return array.length > 0;
};
