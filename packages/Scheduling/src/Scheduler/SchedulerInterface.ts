import { ProcessorInterface } from '../Processor/ProcessorInterface';
import { TaskInterface } from '../Task';

export interface SchedulerInterface {
  init(): Promise<void>;
  defineTaskProcessor(processor: ProcessorInterface[]): void;
  scheduleTask(task: TaskInterface | TaskInterface[]): Promise<void>;
  cancelTask(task: TaskInterface | TaskInterface[]): Promise<void>;
}
