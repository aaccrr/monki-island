import * as Agenda from 'agenda';
import { ProcessorInterface } from '../Processor/ProcessorInterface';
import { TaskInterface } from '../Task';
import { SchedulerConfiguration } from './Interface';
import { SchedulerInterface } from './SchedulerInterface';

export class Scheduler implements SchedulerInterface {
  constructor(private configuration: SchedulerConfiguration) {}

  private scheduler: Agenda.Agenda;

  async init(): Promise<void> {
    const { dbHost, dbUsername, dbPassword, dbName, dbCollection } = this.configuration;

    this.scheduler = new Agenda.Agenda({
      db: {
        address: `mongodb://${dbUsername}:${dbPassword}@${dbHost}/${dbName}?authSource=admin`,
        collection: dbCollection,
      },
    });

    await this.scheduler.start();

    // await this.executeExpiredTask();
  }

  defineTaskProcessor(processor: ProcessorInterface[]): void {
    for (const taskProcessor of processor) {
      this.scheduler.define(taskProcessor.getTaskName(), taskProcessor.getProcessingWrapper());
    }
  }

  async scheduleTask(task: TaskInterface | TaskInterface[]): Promise<void> {
    if (Array.isArray(task)) {
      for (const scheduledTask of task) {
        await this.scheduleSingleTask(scheduledTask);
      }

      return;
    }

    await this.scheduleSingleTask(task);
  }

  async cancelTask(task: TaskInterface | TaskInterface[]): Promise<void> {
    if (Array.isArray(task)) {
      for (const canceledTask of task) {
        await this.cancelSingleTask(canceledTask);
      }

      return;
    }

    await this.cancelSingleTask(task);
  }

  protected async scheduleSingleTask(task: TaskInterface): Promise<void> {
    if (task.isUnique) {
      await this.cancelSingleTask(task);
    }

    if (task.isCron) {
      await this.scheduler.every(task.getSchedule() as string, task.getName(), task.getConfiguration());
      return;
    }

    await this.scheduler.schedule(task.getSchedule(), task.getName(), task.getConfiguration());
  }

  protected async cancelSingleTask(task: TaskInterface): Promise<void> {
    if (task.isUnique) {
      await this.scheduler.cancel({
        name: task.getName(),
        'data.uniqueId': task.getUniqueId(),
      });
    } else {
      await this.scheduler.cancel({
        name: task.getName(),
      });
    }
  }

  protected async executeExpiredTask(): Promise<void> {
    const expired = await this.scheduler.jobs({
      nextRunAt: {
        $lt: new Date(),
      },
    });

    for (const expiredTask of expired) {
      await expiredTask.schedule('in 20 seconds').save();
    }
  }

  private serializeTaskBodySelector(body: any): any {
    return Object.keys(body).reduce((preparedSelector, selectorProp) => {
      preparedSelector[`data.body.${selectorProp}`] = body[selectorProp];
      return preparedSelector;
    }, {});
  }
}
