export interface SchedulerConfiguration {
  dbHost: string;
  dbUsername: string;
  dbPassword: string;
  dbName: string;
  dbCollection: string;
}
