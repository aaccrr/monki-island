import { Type } from './Type';

export interface Meta {
  type: Type;
  key: string;
  namespace: string;
  excute?: Date;
  repeatInterval?: number;
  repeatBetween?: [number, number];
}
