import * as Execution from '@monki-island/execution';
import { Task } from '../Task';
import { SchedulerConfiguration } from './SchedulerConfiguration';

export interface SchedulerInterface {
  init(configuration: SchedulerConfiguration): Promise<void>;
  attachFiber(fiber: Execution.Instrumentary.FiberConstructor): void;
  schedule(task: Task): Promise<void>;
  revoke(task: Task): Promise<void>;
}
