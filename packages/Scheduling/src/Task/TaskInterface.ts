import { Nullable } from '@monki-island/common';
import { TaskConfiguration } from './Interface';

export interface TaskInterface<T = any> {
  isCron: boolean;
  isUnique: boolean;
  getSchedule(): Date | string;
  getName(): string;
  getConfiguration(): TaskConfiguration<T>;
  getUniqueId(): Nullable<string>;
}
