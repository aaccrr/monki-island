import { Nullable, Timezone } from '@monki-island/common';

export interface TaskRetryPolicy {
  delay: number;
  attempt: number;
}

export interface TaskReshedulePolicy {
  delay?: number | number[];
  range?: [Date, Date];
  repeatable: boolean;
}

export interface TaskConfiguration<T> {
  uniqueId?: string;
  body: T;
  timezone: Timezone;
  retryPolicy: TaskRetryPolicy;
  reschedulePolicy: Nullable<TaskReshedulePolicy>;
}
