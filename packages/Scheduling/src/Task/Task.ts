import { Nullable, Timezone } from '@monki-island/common';
import { calculateTaskReschedule } from '..';
import { TaskConfiguration, TaskReshedulePolicy, TaskRetryPolicy } from './Interface';
import { TaskInterface } from './TaskInterface';
import { CalculateTaskRescheduleFromRange } from '../Util/CalculateTaskRescheduleFromRange';

export abstract class Task<T> implements TaskInterface<T> {
  protected abstract name: string;
  protected abstract payload: T;
  protected schedule: Date | string;
  protected abstract timezone: Timezone;
  protected retryPolicy: TaskRetryPolicy = {
    delay: 2,
    attempt: 5,
  };
  protected reschedulePolicy: TaskReshedulePolicy = null;

  isCron: boolean = false;

  uniqueId: string = null;

  getConfiguration(): TaskConfiguration<T> {
    const configuration: Partial<TaskConfiguration<T>> = {
      body: this.payload,
      timezone: this.timezone,
      retryPolicy: this.retryPolicy,
      reschedulePolicy: this.reschedulePolicy,
    };

    if (this.uniqueId !== null) {
      configuration.uniqueId = this.uniqueId;
    }

    return configuration as TaskConfiguration<T>;
  }

  getSchedule(): Date | string {
    if (!this.schedule) {
      if (this.reschedulePolicy && this.reschedulePolicy.range) {
        return CalculateTaskRescheduleFromRange(this.reschedulePolicy.range, this.timezone);
      }

      if (this.reschedulePolicy && this.reschedulePolicy.delay) {
        return calculateTaskReschedule(this.reschedulePolicy, this.timezone);
      }

      return new Date();
    }

    return this.schedule;
  }

  getName(): string {
    return this.name;
  }

  getUniqueId(): Nullable<string> {
    return this.uniqueId;
  }

  get isUnique(): boolean {
    return this.uniqueId !== null;
  }
}
