import { DependencyContainerCommonInterface } from '@monki-island/dependency-container';
import * as Observability from '@monki-island/observability';

import { ProcessorInterface } from '.';
import { calculateTaskReschedule, calculateTaskRetrySchedule } from '../Util';
import { ProcessedTask, ProcessingWrapper, TaskRunner, TaskRunnerContext } from './Interface';

export abstract class Processor implements ProcessorInterface {
  constructor(private DependencyContainer: DependencyContainerCommonInterface) {}

  protected abstract taskName: string;
  protected abstract getTaskRunner(): TaskRunner;

  getProcessingWrapper(): ProcessingWrapper {
    const observability = this.DependencyContainer.getObserver();

    return async (task: ProcessedTask) => {
      const span = observability.tracer.startSpan({
        name: `scheduled ${this.taskName}`,
      });

      span.withPayload(task.attrs.data.body);

      const trace = span.extractContext();

      const taskProcessor = this.getTaskRunner();

      try {
        const taskResult = await taskProcessor(this.createTaskRunnerContext(trace, task.attrs.data.body));

        span.withResponse(taskResult);

        if (task.attrs.data.reschedulePolicy.repeatable) {
          task.schedule(calculateTaskReschedule(task.attrs.data.reschedulePolicy, task.attrs.data.timezone));
          return;
        }

        task.disable();
      } catch (e) {
        span.withError(e);

        if (task.attrs.data.reschedulePolicy.repeatable) {
          task.schedule(calculateTaskReschedule(task.attrs.data.reschedulePolicy, task.attrs.data.timezone));
          return;
        }

        if (task.attrs.failCount < task.attrs.data.retryPolicy.attempt) {
          task.schedule(calculateTaskRetrySchedule(task.attrs.data.retryPolicy.delay, task.attrs.data.timezone));
          return;
        }

        task.disable();
      } finally {
        await task.save();
      }
    };
  }

  getTaskName(): string {
    return this.taskName;
  }

  private createTaskRunnerContext(trace: Observability.Tracing.TraceCarrier, payload: any): TaskRunnerContext {
    const dispatcher = this.DependencyContainer.getDispatcher();

    const dispatchSignal = <T, Y = void>(signalName: string, signalPayload: T): Promise<Y> => {
      return dispatcher.dispatchSignalWithTrace<T, Y>(signalName as any, {
        payload: signalPayload,
        trace,
      });
    };

    return {
      dispatchSignal,
      payload,
    };
  }
}
