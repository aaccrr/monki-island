import * as Agenda from 'agenda';
import { TaskConfiguration } from '../Task';

export interface TaskRunnerContext<P = any> {
  dispatchSignal<T, Y = void>(signalName: string, signalPayload: T): Promise<Y>;
  payload: P;
}

export type ProcessedTask<T = any> = Agenda.Job<TaskConfiguration<T>>;

export type ProcessingWrapper = (task: ProcessedTask) => Promise<void>;

export type TaskRunner = (context: TaskRunnerContext) => Promise<any>;
