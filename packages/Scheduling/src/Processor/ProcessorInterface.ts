import { ProcessingWrapper } from './Interface';

export interface ProcessorInterface {
  getProcessingWrapper(): ProcessingWrapper;
  getTaskName(): string;
}
