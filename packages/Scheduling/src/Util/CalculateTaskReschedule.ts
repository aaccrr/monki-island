import { Timezone } from '@monki-island/common';
import { VmuzeyTimezonedDateHelper } from 'monki-island';
import { TaskReshedulePolicy } from '../Task';
import { randomizeScheduleSecond } from './RandomizeScheduleSecond';

export const calculateTaskReschedule = (reschedulePolicy: TaskReshedulePolicy, timezone: Timezone): Date => {
  if (Array.isArray(reschedulePolicy.delay)) {
    const randomizedSchedule = randomizeScheduleSecond(reschedulePolicy.delay);
    return new VmuzeyTimezonedDateHelper(new Date(), { timezone, subTimezone: false }).addSeconds(randomizedSchedule).getDate();
  }

  return new VmuzeyTimezonedDateHelper(new Date(), { timezone, subTimezone: false }).addSeconds(reschedulePolicy.delay).getDate();
};
