import { VmuzeyTimezonedDateHelper } from 'monki-island';
import { Timezone } from '@monki-island/common';
import { CalculateScheduleDrift } from './CalculateScheduleDrift';

export const CalculateTaskRescheduleFromRange = (range: [Date, Date], timezone: Timezone): Date => {
  const scheduleDrift = CalculateScheduleDrift(range[0].getSeconds(), range[1].getSeconds());
  return new VmuzeyTimezonedDateHelper(range[0], { timezone, subTimezone: false }).addSeconds(scheduleDrift).getDate();
};
