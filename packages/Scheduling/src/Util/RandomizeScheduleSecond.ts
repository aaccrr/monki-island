export const randomizeScheduleSecond = (range: number[]): number => {
  const min = range[0] * 60;
  const max = range[1] * 60;
  return Math.floor(Math.random() * (max - min) + min);
};
