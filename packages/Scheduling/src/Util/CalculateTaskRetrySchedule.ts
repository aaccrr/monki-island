import { Timezone } from '@monki-island/common';
import { VmuzeyTimezonedDateHelper } from 'monki-island';

export const calculateTaskRetrySchedule = (delay: number, timezone: Timezone): Date => {
  return new VmuzeyTimezonedDateHelper(new Date(), { timezone, subTimezone: false }).addMinutes(delay).getDate();
};
