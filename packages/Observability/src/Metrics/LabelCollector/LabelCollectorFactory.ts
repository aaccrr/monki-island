import { EntranceTicketLabelCollector, LabelCollectorInterface } from '.';

export class LabelCollectorFactory {
  create(source: any): LabelCollectorInterface {
    if (typeof source.entranceTicket !== 'undefined') {
      return new EntranceTicketLabelCollector(source.entranceTicket);
    }

    return null;
  }
}
