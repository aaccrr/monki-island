import { MetricLabel } from '../Collector/Interface';

export interface LabelCollectorInterface {
  pullCollectedLabel(): MetricLabel;
  pullPossibleLableEnum(): string[];
}
