import { IItemModel } from '@monki-island/data-model';
import { MetricLabel } from '../Collector/Interface';
import { LabelCollectorInterface } from './LabelCollectorInterface';

export class EntranceTicketLabelCollector implements LabelCollectorInterface {
  constructor(entranceTicket: IItemModel) {
    this.collectNomenclatureType(entranceTicket);
    this.collectPaymentType(entranceTicket);
  }

  private labelEnum: string[] = ['nomenclatureType', 'paymentType'];
  private collectedLabel: MetricLabel = {};

  pullCollectedLabel(): MetricLabel {
    return this.collectedLabel;
  }

  pullPossibleLableEnum(): string[] {
    return this.labelEnum;
  }

  private collectNomenclatureType(source: IItemModel): void {
    const nomenclatureType = source.nomenclature.type === 'ABONEMENT' ? 'season_ticket' : source.nomenclature.type.toLowerCase();
    this.collectedLabel['nomenclatureType'] = nomenclatureType;
  }

  private collectPaymentType(source: IItemModel): void {
    if (source.payment.pushkinCard) {
      this.collectedLabel['paymentType'] = 'pushkin_card';
    } else {
      this.collectedLabel['paymentType'] = 'debit_card';
    }
  }
}
