import * as prom from 'prom-client';
import { LabelCollectorFactory } from '../LabelCollector';

interface MetricContainer {
  [key: string]: prom.Counter<string>;
}

export class Collector {
  private metricContainer: MetricContainer = {};

  async collectMetric(name: string, source: any): Promise<void> {
    const metricName = this.serializeMetricName(name);

    const labelCollector = new LabelCollectorFactory().create(source);
    const possibleLabel = labelCollector !== null ? labelCollector.pullPossibleLableEnum() : [];

    if (!this.metricContainer[metricName]) {
      const storedMetric = await prom.register.getSingleMetric(metricName);

      if (!storedMetric) {
        this.metricContainer[metricName] = new prom.Counter({
          name: metricName,
          help: metricName.split('_').join(' '),
          labelNames: possibleLabel,
        });
      } else {
        this.metricContainer[metricName] = storedMetric as prom.Counter<string>;
      }
    }

    if (labelCollector !== null) {
      this.metricContainer[metricName].inc(labelCollector.pullCollectedLabel());
    } else {
      this.metricContainer[metricName].inc();
    }
  }

  pullMetric(): Promise<string> {
    return prom.register.metrics();
  }

  private serializeMetricName(name: string): string {
    return `${name.split('.').join('_')}_total`;
  }
}
