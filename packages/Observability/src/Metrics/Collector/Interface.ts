export interface MetricLabel {
  [key: string]: string;
}
