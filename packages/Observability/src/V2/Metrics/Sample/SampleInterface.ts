import * as prom from 'prom-client';

export interface SampleInterface<L = any> {
  getMetricName(): string;
  getMetricType(): prom.MetricType;
  getLabelsNames(): string[];
  getMetricDescription(): string;
  getLabels(): L;
}
