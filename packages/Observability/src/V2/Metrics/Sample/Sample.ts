import * as prom from 'prom-client';
import { SampleInterface } from './SampleInterface';

export class Sample<L = any> implements SampleInterface<L> {
  protected metricName: string;
  protected metricType: prom.MetricType;
  protected labels: L;

  getMetricName(): string {
    return this.metricName;
  }

  getMetricType(): prom.MetricType {
    return this.metricType;
  }

  getLabelsNames(): string[] {
    const labels = this.labels || {};
    return Object.keys(labels);
  }

  getMetricDescription(): string {
    return this.metricName.split('_').join(' ');
  }

  getLabels(): L {
    return this.labels;
  }
}
