import { Nullable } from '@monki-island/common';
import * as prom from 'prom-client';
import { SampleInterface } from '../Sample';

export class MetricFactory {
  createBySample(sample: SampleInterface): Nullable<prom.Metric<any>> {
    const metricType = sample.getMetricType();

    if (metricType === prom.MetricType.Counter) {
      return new prom.Counter({
        name: sample.getMetricName(),
        help: sample.getMetricDescription(),
        labelNames: sample.getLabelsNames(),
      });
    }

    if (metricType === prom.MetricType.Gauge) {
      return new prom.Gauge({
        name: sample.getMetricName(),
        help: sample.getMetricDescription(),
        labelNames: sample.getLabelsNames(),
      });
    }

    return null;
  }
}
