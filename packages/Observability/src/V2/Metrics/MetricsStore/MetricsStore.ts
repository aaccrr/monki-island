import * as prom from 'prom-client';
import { MetricsStoreInterface } from '.';
import { SampleInterface } from '../Sample';

export class MetricsStore implements MetricsStoreInterface {
  addSample(sample: SampleInterface): void {
    const metricType = sample.getMetricType();

    if (metricType === prom.MetricType.Counter) {
      this.updateCounter(sample);
    }
  }

  pullMetrics(): Promise<string> {
    return prom.register.metrics();
  }

  private updateCounter(sample: SampleInterface): void {
    const counter = new prom.Counter({
      name: sample.getMetricName(),
      help: sample.getMetricDescription(),
      labelNames: sample.getLabelsNames(),
    });

    counter.inc(sample.getLabels());
  }
}
