import { SampleInterface } from '../Sample';

export interface MetricsStoreInterface {
  addSample(sample: SampleInterface): void;
  pullMetrics(): Promise<string>;
}
