import { FORMAT_TEXT_MAP, Span as OpenSpan } from 'opentracing';
import { SpanInterface } from './SpanInterface';

export class Span implements SpanInterface {
  constructor(private span: OpenSpan) {}

  addLog(name: string, body: any): void {
    this.findTags(body);
    this.span.log({ [name]: body });
  }

  markError(): void {
    this.span.setTag('error', true);
  }

  extractContext(): any {
    const carrier = {};
    this.span.tracer().inject(this.span.context(), FORMAT_TEXT_MAP, carrier);
    return carrier;
  }

  finish(): void {
    this.span.finish();
  }

  private findTags(payload: any): void {
    if (payload) {
      const tags = {
        ...this.getEnvironmentTags(),
        ...this.getEntityTagsFromObject(payload),
        ...this.getEntityTagsFromObject(payload.body),
        ...this.getUserIdTagFromPayload(payload),
      };

      this.span.addTags(tags);
    }
  }

  private getEntityTagsFromObject(obj: any): any {
    return Object.keys(obj).reduce((tagsList, payloadKey) => {
      if (payloadKey.toLowerCase().endsWith('id')) {
        tagsList[`entity.${payloadKey}`] = obj[payloadKey];
      }

      return tagsList;
    }, {});
  }

  private getUserIdTagFromPayload(payload: any): any {
    if (typeof payload.meta?.user?.id === 'string' && payload.meta?.user?.id?.length > 0) {
      return {
        'user.id': payload.meta?.user?.id,
      };
    }

    return {};
  }

  private getEnvironmentTags(): any {
    return {
      env: process.env.NODE_ENV,
    };
  }
}
