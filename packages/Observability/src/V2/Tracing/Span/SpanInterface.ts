export interface SpanInterface {
  addLog(name: string, body: any): void;
  markError(): void;
  extractContext(): any;
  finish(): void;
}
