import { CarrierInterface } from '../Carrier';
import { SpanInterface } from '../Span/SpanInterface';

export interface TracerInterface {
  createNewSpan(title: string): SpanInterface;
  createLinkOn(target: CarrierInterface, title: string): SpanInterface;
  extendsFrom(parent: CarrierInterface, title: string): SpanInterface;
}
