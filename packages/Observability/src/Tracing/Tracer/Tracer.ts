import { Tracer as OpenTracer, FORMAT_TEXT_MAP, followsFrom } from 'opentracing';
import { initTracer, ReporterConfig } from 'jaeger-client';
import { TracerInterface } from './TracerInterface';
import { TraceCarrier, TracerConfiguration, TracerConfigurationReporter } from './Interface';
import { Span } from '../Span/Span';
import { SpanInterface } from '../Span/SpanInterface';
import { SpanContext } from '../Span/Interface';

export class Tracer implements TracerInterface {
  constructor(configuration: TracerConfiguration) {
    this._tracer = initTracer(
      {
        serviceName: configuration.serviceName,
        reporter: {
          logSpans: true,
          ...this.getReporterConnectionConfig(configuration.reporter),
        },
        sampler: {
          type: 'const',
          param: 1,
        },
      },
      {}
    );
  }

  private _tracer: OpenTracer;

  startSpan(context: SpanContext): SpanInterface {
    return new Span(this._tracer.startSpan(context.name));
  }

  linkSpan(context: SpanContext, link: TraceCarrier): SpanInterface {
    const referenceSpan = this._tracer.extract(FORMAT_TEXT_MAP, link);
    return new Span(this._tracer.startSpan(context.name, { references: [followsFrom(referenceSpan)] }));
  }

  forkSpan(context: SpanContext, parent: TraceCarrier): SpanInterface {
    const parentSpan = this._tracer.extract(FORMAT_TEXT_MAP, parent);
    return new Span(this._tracer.startSpan(context.name, { childOf: parentSpan }));
  }

  private getReporterConnectionConfig(reporterConfig: TracerConfigurationReporter): Partial<ReporterConfig> {
    if (reporterConfig.agent) {
      const [agentHost, agentPort] = reporterConfig.agent.host.split(':');

      return {
        agentHost,
        agentPort: parseInt(agentPort),
      };
    }

    return {
      collectorEndpoint: reporterConfig.collector.host,
    };
  }
}
