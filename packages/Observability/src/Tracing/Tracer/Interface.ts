export interface TracerConfiguration {
  serviceName: string;
  reporter: TracerConfigurationReporter;
}

export interface TracerConfigurationReporter {
  agent?: {
    host: string;
  };
  collector?: {
    host: string;
  };
}

export interface TraceCarrier {}
