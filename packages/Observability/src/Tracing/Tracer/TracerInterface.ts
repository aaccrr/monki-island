import { SpanInterface } from '../Span/SpanInterface';
import { SpanContext } from '../Span/Interface';
import { TraceCarrier } from './Interface';

export interface TracerInterface {
  startSpan(context: SpanContext): SpanInterface;
  linkSpan(context: SpanContext, link: TraceCarrier): SpanInterface;
  forkSpan(context: SpanContext, parent: TraceCarrier): SpanInterface;
}
