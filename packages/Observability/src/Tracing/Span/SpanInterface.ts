export interface SpanInterface {
  withPayload(payload: any): void;
  withError(error: any): void;
  withResponse(response: any): void;
  extractContext(): any;
  finish(): void;
}
