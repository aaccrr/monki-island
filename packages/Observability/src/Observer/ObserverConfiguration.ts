import { TracerConfiguration } from '../Tracing';

export interface ObserverConfiguration {
  tracer: TracerConfiguration;
}
