import { Collector } from '../Metrics';
import { Tracer } from '../Tracing';
import { ObserverConfiguration } from './ObserverConfiguration';

export class Observer {
  constructor(configuration: ObserverConfiguration) {
    this.tracer = new Tracer(configuration.tracer);
    this.collector = new Collector();
  }

  public tracer: Tracer;
  public collector: Collector;
}
