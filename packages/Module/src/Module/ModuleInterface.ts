import { DependencyContainerCommonInterface } from '@monki-island/dependency-container';

export interface ModuleInterface {
  start(): Promise<void>;
  getDependencyContainer<T extends DependencyContainerCommonInterface>(): T;
}
