import { DependencyContainerCommonInterface } from '@monki-island/dependency-container';
import { EndpointInterface, HttpServer, HttpServerInterface, InternalEndpoint } from '@monki-island/gateway';
import { RecieverConstructor, RecieverFactory } from '@monki-island/transport';
import { EventEmitter } from 'events';
import { OpenapiSpecification } from '../OpenapiSpecification';
import { ModuleInterface } from './ModuleInterface';

type RecieverRotation = {
  [key: string]: RecieverConstructor[];
};

type RecieverGroup = {
  [key: string]: RecieverFactory;
};

export class Module extends EventEmitter implements ModuleInterface {
  private httpServer: HttpServerInterface;
  protected dependencyContainer: DependencyContainerCommonInterface;

  protected endpoint: EndpointInterface[];
  protected reciever: RecieverRotation;
  protected internal: InternalEndpoint[];

  async start(): Promise<void> {
    if (process.env.MODE === 'run_app') {
      await this.init();
    }

    if (process.env.MODE === 'generate_specification') {
      await this.generateSpecification();
      process.exit(0);
    }
  }

  getDependencyContainer<T extends DependencyContainerCommonInterface>(): T {
    return this.dependencyContainer as T;
  }

  private async init(): Promise<void> {
    await this.dependencyContainer.init();
    await this.bootstrapGateway();

    this.emit('initialized', this.dependencyContainer);
  }

  private async bootstrapGateway(): Promise<void> {
    if (this.isRecieverDefined) {
      const recieverGroup = this.groupReciver();
      await this.subscribeReciver(recieverGroup);
    }

    if (this.isPublicEndpointDefined || this.isInternalEndpointDefined) {
      this.startHttpServer();
    }
  }

  private async generateSpecification(): Promise<void> {
    const specification = new OpenapiSpecification();

    if (this.isPublicEndpointDefined) {
      this.endpoint.forEach((endpoint) => {
        specification.addEndpointSpecification(endpoint.getSpecification());
      });
    }

    await specification.flush();
  }

  private groupReciver(): RecieverGroup {
    return Object.keys(this.reciever).reduce((recieverGroup: RecieverGroup, signal: string) => {
      recieverGroup[signal] = new RecieverFactory(this.dependencyContainer);

      this.reciever[signal].forEach((reciever) => {
        recieverGroup[signal].addReciever(reciever);
      });

      return recieverGroup;
    }, {} as RecieverGroup);
  }

  private async subscribeReciver(recieverGroup: RecieverGroup): Promise<void> {
    const transporter = this.dependencyContainer.getTransporter();

    for (const signal in recieverGroup) {
      await transporter.subscribe(signal, recieverGroup[signal]);
    }
  }

  private get isRecieverDefined(): boolean {
    return this.reciever && Array.isArray(Object.keys(this.reciever)) && Object.keys(this.reciever).length > 0;
  }

  private get isPublicEndpointDefined(): boolean {
    return Array.isArray(this.endpoint) && this.endpoint.length > 0;
  }

  private get isInternalEndpointDefined(): boolean {
    return Array.isArray(this.internal) && this.internal.length > 0;
  }

  private startHttpServer(): void {
    this.httpServer = new HttpServer(this.dependencyContainer);

    if (this.isPublicEndpointDefined) {
      this.endpoint.forEach((endpoint) => {
        this.httpServer.addEndpoint(endpoint);
      });
    }

    if (this.isInternalEndpointDefined) {
      this.internal.forEach((endpoint) => {
        this.httpServer.addInternalEndpoint(endpoint);
      });
    }

    this.httpServer.startServer();
  }
}
