import { EndpointSpecificationInterface } from '@monki-island/gateway/lib/EndpointSpecification';
import { VmuzeyFileStorageLocalDriver, VmuzeyFileStorageProvider } from 'monki-island';
import { OpenAPIV3 } from 'openapi-types';

export class OpenapiSpecification {
  constructor() {
    this.specification = {
      openapi: '3.0.0',
      info: {
        title: '',
        version: '1.0.0',
      },
      paths: {},
      components: {
        schemas: {},
      },
    };
  }

  private specification: Partial<OpenAPIV3.Document>;

  addEndpointSpecification(endpointSpecification: EndpointSpecificationInterface): void {
    this.addSchemas(endpointSpecification);

    if (typeof this.specification.paths[endpointSpecification.getPath()] === 'undefined') {
      this.specification.paths[endpointSpecification.getPath()] = {};
    }

    this.specification.paths[endpointSpecification.getPath()][endpointSpecification.getMethod()] = endpointSpecification.getSpecification();
  }

  async flush(): Promise<void> {
    const fileStorage = new VmuzeyFileStorageProvider(new VmuzeyFileStorageLocalDriver());
    await fileStorage.save(JSON.stringify(this.specification), { path: './openapi/spec.json' });
  }

  private addSchemas(endpointSpecification: EndpointSpecificationInterface): void {
    const components = endpointSpecification.getComponents();
    const schemas = components.schemas || {};

    Object.keys(schemas).forEach((schemaName) => {
      if (this.specification.components.schemas[schemaName] === undefined) {
        this.specification.components.schemas[schemaName] = schemas[schemaName];
      }
    });
  }
}
