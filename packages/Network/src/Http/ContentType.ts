export enum ContentType {
  Plain = 'plain/text',
  Html = 'application/html',
  Json = 'application/json',
  Xml = '',
}
