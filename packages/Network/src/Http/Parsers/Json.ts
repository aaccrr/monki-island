import { ContentType } from '../ContentType';

export class Json {
  constructor(private raw: string) {}

  static content_type = ContentType.Json;

  async parse(): Promise<object> {
    try {
      const parsed = JSON.parse(this.raw);

      return parsed;
    } catch (e) {
      throw new Error('parsing error');
    }
  }
}
