import * as Url from 'url';
import * as Http from 'http';
import * as Https from 'https';
import * as Parsers from '../Parsers';
import { ContentType } from '../ContentType';

export class Get {
  constructor(private url: Url.URL, private headers: any) {}

  private parsers = [Parsers.Json];

  private request: Http.ClientRequest;

  private response: Http.IncomingMessage;

  private consume(): Promise<string> {
    return new Promise((resolve, reject) => {
      let body = '';

      this.response.on('data', (d) => {
        try {
          body += d.toString();
        } catch (e) {
          reject(e);
        }
      });

      this.response.on('end', () => {
        resolve(body);
      });
    });
  }

  private async parse(response: string): Promise<any> {
    const content_type = this.detect_content_type();

    const body_parser = this.parsers.find((parser) => parser.content_type === content_type);

    if (body_parser) {
      const body = await new body_parser(response).parse();

      return body;
    }

    return response;
  }

  private detect_content_type(): ContentType {
    const content_types = [ContentType.Html, ContentType.Json, ContentType.Plain, ContentType.Xml];

    const body_type = content_types.find((content_type) => new RegExp(content_type).test(this.response.headers['content-type']));

    if (body_type) {
      return body_type;
    }

    return ContentType.Plain;
  }

  send(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.request = Https.request({
        method: 'GET',
        hostname: this.url.hostname,
        path: `${this.url.pathname}${this.url.search}`,
        headers: this.headers,
      });

      this.request.on('response', async (raw_response) => {
        this.response = raw_response;

        const response = await this.consume();

        const body = await this.parse(response);

        if (this.response.statusCode >= 400) {
          reject(body);

          return;
        }

        resolve(body);
      });

      this.request.on('error', (e) => {
        reject(e);
      });

      this.request.end();
    });
  }
}
