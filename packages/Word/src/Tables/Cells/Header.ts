import * as Docx from 'docx';

export class Header extends Docx.TableCell {
  constructor(text: string, span?: number, centered?: boolean) {
    super({
      columnSpan: span,
      margins: {
        left: 75,
        right: 75,
        top: 75,
        bottom: 75,
      },

      children: [
        new Docx.Paragraph({
          style: 'cell_header',
          alignment: centered ? Docx.AlignmentType.CENTER : Docx.AlignmentType.LEFT,
          children: [
            new Docx.TextRun({
              text,
            }),
          ],
        }),
      ],
    });
  }
}
