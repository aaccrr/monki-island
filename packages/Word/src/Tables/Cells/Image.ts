import * as Fs from 'fs';
import * as Docx from 'docx';

export class Image extends Docx.TableCell {
  constructor(width: number, height: number, src: string, capture?: string) {
    super({
      columnSpan: 1,
      margins: {
        left: 75,
        right: 75,
        top: 75,
        bottom: 75,
      },
      children: [
        new Docx.Paragraph({
          style: 'cell_image',
          children: [
            new Docx.ImageRun({
              data: Fs.readFileSync(src),
              transformation: {
                width,
                height,
              },
            }),
            new Docx.TextRun({
              text: capture || '',
            }),
          ],
        }),
      ],
    });
  }
}
