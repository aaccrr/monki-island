import * as Docx from 'docx';

export class Background extends Docx.TableCell {
  constructor(text: string, span?: number, centered?: boolean) {
    super({
      columnSpan: span,
      margins: {
        left: 75,
        right: 75,
        top: 75,
        bottom: 75,
      },
      children: [
        new Docx.Paragraph({
          style: 'cell_text',
          alignment: centered ? Docx.AlignmentType.CENTER : Docx.AlignmentType.LEFT,
          shading: {
            fill: 'f5f5f5',
            type: Docx.ShadingType.SOLID,
            color: 'f5f5f5',
          },
          children: [
            new Docx.TextRun({
              text,
            }),
          ],
        }),
      ],
    });
  }
}
