import * as Fs from 'fs';
import * as Docx from 'docx';

export class Header extends Docx.Paragraph {
  constructor() {
    super({
      style: 'header',
      children: [
        new Docx.ImageRun({
          data: Fs.readFileSync('./logo.jpg'),
          transformation: {
            width: 121,
            height: 26.5,
          },
          floating: {
            horizontalPosition: {
              relative: Docx.HorizontalPositionRelativeFrom.INSIDE_MARGIN,
              align: Docx.HorizontalPositionAlign.INSIDE,
            },
            verticalPosition: {
              relative: Docx.VerticalPositionRelativeFrom.PARAGRAPH,
              align: Docx.VerticalPositionAlign.INSIDE,
            },
          },
        }),
      ],
    });
  }
}
