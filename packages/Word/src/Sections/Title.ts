import * as Docx from 'docx';

export class Title extends Docx.Paragraph {
  constructor(value: string) {
    super({
      style: 'title',
      children: [
        new Docx.TextRun({
          text: value,
        }),
      ],
    });
  }
}
