import * as Docx from 'docx';

export class Text extends Docx.Paragraph {
  constructor(value: string) {
    super({
      style: 'text',
      children: [
        new Docx.TextRun({
          text: value,
        }),
      ],
    });
  }
}
