import * as Docx from 'docx';

export class Table extends Docx.Table {
  static columns(columns_count: number): number[] {
    const columns = [];

    for (let i = 0; i < columns_count; i++) {
      columns.push(9028 / columns_count);
    }

    return columns;
  }

  constructor(columns_count: number) {
    super({
      style: 'table',
      columnWidths: Table.columns(columns_count),
      width: {
        size: 0,
        type: Docx.WidthType.AUTO,
      },
      rows: [],
    });
  }

  protected add_row(...cells: Docx.TableCell[]): void {
    this.addChildElement(
      new Docx.TableRow({
        children: cells,
      })
    );
  }
}
