import * as Docx from 'docx';

export class Break extends Docx.Paragraph {
  constructor() {
    super({
      style: 'break',
      children: [],
    });
  }
}
