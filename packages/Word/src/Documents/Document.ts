import * as Fs from 'fs';
import * as Docx from 'docx';

export class Document {
  private sections: (Docx.Paragraph | Docx.Table)[] = [];

  private styles: Docx.IStylesOptions = {
    paragraphStyles: [
      {
        id: 'header',
        name: 'header',
        basedOn: 'Normal',
        next: 'Normal',
        run: {
          font: 'Arial',
          size: 24,
        },
        paragraph: {
          indent: {
            left: 0,
            right: 0,
          },
          spacing: {
            before: 0,
            after: 400,
          },
        },
      },
      {
        id: 'title',
        name: 'title',
        basedOn: 'Normal',
        next: 'Normal',
        run: {
          font: 'Arial',
          size: 24,
          bold: true,
        },
        paragraph: {
          alignment: Docx.AlignmentType.CENTER,
          indent: {
            left: 0,
            right: 0,
          },
          spacing: {
            before: 0,
            after: 400,
          },
        },
      },
      {
        id: 'text',
        name: 'text',
        basedOn: 'Normal',
        next: 'Normal',
        run: {
          font: 'Arial',
          size: 18,
        },
        paragraph: {
          indent: {
            left: 0,
            right: 0,
          },
          spacing: {
            line: 250,
            after: 125,
          },
        },
      },
      {
        id: 'break',
        name: 'break',
        basedOn: 'Normal',
        next: 'Normal',
        run: {
          font: 'Arial',
          size: 18,
        },
        paragraph: {
          indent: {
            left: 0,
            right: 0,
          },
          spacing: {
            line: 0,
            after: 125,
          },
        },
      },
      {
        id: 'cell_text',
        name: 'cell_text',
        run: {
          font: 'Arial',
          size: 18,
        },
      },
      {
        id: 'cell_header',
        name: 'cell_header',
        run: {
          font: 'Arial',
          size: 18,
          bold: true,
        },
      },
      {
        id: 'cell_image',
        name: 'cell_image',
        run: {
          font: 'Arial',
          size: 18,
        },
      },
    ],
  };

  on_create(): void {}

  async create(): Promise<Buffer> {
    this.on_create();

    const document = new Docx.Document({
      styles: this.styles,
      sections: [
        {
          properties: {
            page: {
              margin: {
                top: 1440,
                right: 1440,
                bottom: 1440,
                left: 1440,
              },
            },
          },
          children: this.sections,
        },
      ],
    });

    const buffer = await Docx.Packer.toBuffer(document);

    return buffer;

    // Fs.writeFileSync(`${this.constructor.name}.docx`, buffer);
  }

  protected add_section(section: Docx.Paragraph | Docx.Table): void {
    this.sections.push(section);
  }
}
