import { Client, HttpClient } from 'jayson/promise';
import { BusinessError, ValidationError } from '@monki-island/error';
import { IVmuzeyRpcClientProvider } from './i-vmuzey-rpc-client-provider';

export class VmuzeyRpcClientProvider implements IVmuzeyRpcClientProvider {
  constructor(private VmuzeyLoggerProvider: any, private requiredServices: string[]) {}

  private services = {};

  init(): void {
    this.services = this.requiredServices.reduce((servicesAcc, service) => {
      const serviceFullName = this.addServiceFullName(service);

      const host = `http://${serviceFullName}:50051`;
      servicesAcc[serviceFullName] = Client.http(host as any);
      return servicesAcc;
    }, {});
  }

  async call(options: any): Promise<any> {
    const serviceFullName = this.addServiceFullName(options.service);

    const service = this.pickService(serviceFullName);

    const message: any = {
      address: options.method,
      payload: options.payload,
    };

    const response = await this.callService(service, message);

    return response;
  }

  private addServiceFullName(serviceName: string): string {
    if (serviceName === 'code') {
      serviceName = 'codes';
    }

    if (process.env.NODE_ENV === 'production') {
      return `production_app_${serviceName}`;
    }

    if (process.env.SWARM_NAME !== undefined && process.env.STACK_NAME !== undefined) {
      return `${process.env.SWARM_NAME}_${process.env.STACK_NAME}_${serviceName}`;
    }

    if (process.env.NODE_ENV === 'local') {
      return serviceName;
    }

    const aliases = {
      billing: 'catalog_billing',
      ordering: 'catalog_ordering',
      search: 'catalog_search',
      widget: 'catalog_widget',
      accounting: 'management_accounting',
      museum: 'management_museum',
      nomenclature: 'management_nomenclature',
      organization: 'management_organization',
      new_reporting: 'management_reporting',
      seo_optimization: 'management_seo_optimization',
      codes: 'service_codes',
      geography: 'service_geography',
      identity: 'service_identity',
      mailer: 'service_mailer',
      pdf: 'service_pdf',
      upload: 'service_upload',
    };

    return aliases[serviceName] || serviceName;
  }

  private pickService(serviceName: string): any {
    const service = this.services[serviceName];

    if (!service) {
      new BusinessError('ru', 'INTERNAL_ERROR');
    }

    return service;
  }

  private async callService(service: HttpClient, message: any): Promise<any> {
    try {
      const res = await service.request(message.address, message.payload);

      if (res.result) {
        return res.result;
      }

      if (res.error) {
        throw res.error;
      }
    } catch (e) {
      const message = this.parseErrorMessage(e);

      if (message.type === 'BUSINESS_ERROR') {
        const error = new BusinessError(message.locale, message.code);
        error.stack = message.stacktrace;
        throw error;
      }

      if (message.type === 'VALIDATION_ERROR' && message.isPublic) {
        const error = new ValidationError(message.locale, message.errors);
        error.stack = message.stacktrace;
        throw error;
      }

      throw e;
    }
  }

  private parseErrorMessage(error) {
    try {
      const message = JSON.parse(error.message);
      return message;
    } catch (e) {
      return error;
    }
  }
}
