export interface IJsonRpcCallOptions {
  service: string;
  method: string;
  payload: any;
}

export interface IVmuzeyRpcClientProvider {
  init(): void;
  call(options: IJsonRpcCallOptions): Promise<any>;
}
