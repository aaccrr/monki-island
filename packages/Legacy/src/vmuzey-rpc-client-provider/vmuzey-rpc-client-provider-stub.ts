import { IVmuzeyRpcClientProvider } from './i-vmuzey-rpc-client-provider';

export class VmuzeyRpcClientProviderStub implements IVmuzeyRpcClientProvider {
  constructor(private LoggerProvider: any, private requiredServices: string[]) {}

  init(): void {}

  async call(options: any): Promise<any> {
    return {};
  }
}
