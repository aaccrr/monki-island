import { IVmuzeyRpcClientProvider } from './i-vmuzey-rpc-client-provider';
import { VmuzeyRpcClientProvider } from './vmuzey-rpc-client-provider';
import { VmuzeyRpcClientProviderStub } from './vmuzey-rpc-client-provider-stub';

export class VmuzeyRpcClientProviderFactory {
  static create(env: string, loggerProvider: any, services: string[]): IVmuzeyRpcClientProvider {
    if (env === 'test' || env === 'openapi') {
      return new VmuzeyRpcClientProviderStub(loggerProvider, services);
    }

    return new VmuzeyRpcClientProvider(loggerProvider, services);
  }
}
