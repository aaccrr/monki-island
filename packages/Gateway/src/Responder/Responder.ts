import { ConverterInterface } from '@monki-island/common';
import { HttpRequestPayload } from '../Interface';
import { ResponderConfiguration, ResponderInterface } from './ResponderInterface';

const DEFAULT_CONFIGURATION: ResponderConfiguration = {
  format: 'JSON',
  withSuccess: true,
};

export class Responder implements ResponderInterface {
  constructor(private Converter: ConverterInterface) {}

  reply(ctx: HttpRequestPayload, status: number, body: any, configuration?: ResponderConfiguration): void {
    configuration = this.ensureConfiguration(configuration);

    if (configuration.format === 'JSON') this.replyJSON(ctx, status, body, configuration);
    if (configuration.format === 'XML') this.replyXML(ctx, status, body);
    if (configuration.format === 'TEXT') this.replyTEXT(ctx, status, body);
  }

  private replyJSON(ctx: HttpRequestPayload, status: number, body: any, configuration: ResponderConfiguration): void {
    ctx.status = status;
    ctx.body = this.getResponseBody(status, body, configuration);
  }

  private replyXML(ctx: HttpRequestPayload, status: number, body: any): void {
    ctx.status = status;
    ctx.body = this.Converter.jsonToXml(body);
  }

  private replyTEXT(ctx: HttpRequestPayload, status: number, body: any): void {
    ctx.status = status;
    ctx.body = body;
  }

  private getResponseBody(status: number, body: any, configuration: ResponderConfiguration) {
    return configuration.withSuccess ? { success: status >= 200 && status < 300, data: body } : body;
  }

  private ensureConfiguration(configuration?: ResponderConfiguration): ResponderConfiguration {
    if (configuration === undefined) {
      return DEFAULT_CONFIGURATION;
    }

    if (configuration.format === undefined) {
      configuration.format = DEFAULT_CONFIGURATION.format;
    }

    if (configuration.withSuccess === undefined) {
      configuration.withSuccess = DEFAULT_CONFIGURATION.withSuccess;
    }

    return configuration;
  }
}
