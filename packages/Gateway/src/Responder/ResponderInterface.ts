import { HttpRequestPayload } from '../Interface';

export interface ResponderInterface {
  reply(ctx: HttpRequestPayload, status: number, body: any, configuration?: ResponderConfiguration): void;
}

export interface ResponderConfiguration {
  format?: 'JSON' | 'XML' | 'TEXT';
  withSuccess?: boolean;
}
