import * as Execution from '@monki-island/execution';
import { Middleware } from '..';
import { InternalEndpoint } from './InternalEndpoint';

export class MetricEndpoint extends InternalEndpoint {
  constructor() {
    super();

    this.endpoint = {
      method: 'get',
      path: '/metrics',
    };
  }

  getHandler(): Middleware {
    return async (ctx) => {
      const metricsStore = this.dependencyContainer.getResource<Execution.Observability.Metrics.MetricsStoreInterface>(
        Execution.Observability.Metrics.MetricsStore.name
      );

      ctx.status = 200;

      ctx.body = await metricsStore.pullMetrics();
    };
  }
}
