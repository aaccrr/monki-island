import * as Execution from '@monki-island/execution';
import { EndpointConfiguration, Middleware } from '..';

export abstract class InternalEndpoint {
  protected dependencyContainer: Execution.Instrumentary.DependencyContainer;
  protected endpoint: EndpointConfiguration;

  setDependencyContainer(dependencyContainer: Execution.Instrumentary.DependencyContainer): void {
    this.dependencyContainer = dependencyContainer;
  }

  abstract getHandler(): Middleware;

  getEndpoint(): EndpointConfiguration {
    return this.endpoint;
  }
}
