import { OpenAPIV3 } from 'openapi-types';

export type OpenapiRequest = Partial<OpenAPIV3.OperationObject>;
export type OpenapiResponse = Partial<OpenAPIV3.ResponsesObject>;
