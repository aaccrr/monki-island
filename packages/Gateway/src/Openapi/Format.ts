import * as url from 'url';

export const ONLY_DIGITS_FORMAT_REGEXP = '^\\d+$';
export const EMAIL_REGEXP = '.+@.+\\..+';
export const PHONE_REGEXP = '^\\+\\d{11,15}$';
export const TIME_REGEXP = '^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$';
export const DATE_REGEXP = '^([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))$';

export function CheckHostname(data) {
  const HostRegExp = new RegExp('^([\\w\\-]+?\\.?)+?\\.[\\w\\-]+?$');

  if (data.search(/^(http|https):\/\//) === -1) {
    data = 'http://' + data;
  }

  const { hostname } = url.parse(data);

  if (!hostname || hostname.length < 4 || hostname.length > 255 || !HostRegExp.test(hostname)) {
    return false;
  }
  return true;
}
