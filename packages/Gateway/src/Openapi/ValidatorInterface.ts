export interface ValidatorInterface {
  validate(validationPayload: any): void;
}
