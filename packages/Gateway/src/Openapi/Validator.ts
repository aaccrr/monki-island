import { OpenAPIV3 } from 'openapi-types';
import { default as OpenAPIRequestValidator } from 'openapi-request-validator';

import { ValidationError } from '@monki-island/error';

import { ValidatorInterface } from './ValidatorInterface';
import { OpenapiRequest } from './Interface';
import { CheckHostname, DATE_REGEXP, EMAIL_REGEXP, ONLY_DIGITS_FORMAT_REGEXP, PHONE_REGEXP, TIME_REGEXP } from './Format';

export class Validator implements ValidatorInterface {
  constructor(openApiSchema: OpenapiRequest) {
    this.validator = new OpenAPIRequestValidator({
      parameters: openApiSchema.parameters,
      requestBody: openApiSchema.requestBody as OpenAPIV3.RequestBodyObject,
      customFormats: this.addFormats(),
      errorTransformer: this.addErrorsTransformer(),
    });
  }

  private validator: OpenAPIRequestValidator;

  validate(validationPayload: any): void {
    const errors = this.validator.validateRequest(validationPayload);

    if (typeof errors !== 'undefined') {
      throw new ValidationError('ru', errors.errors, 400, false);
    }
  }

  private addErrorsTransformer(): any {
    return (errors, ajvError) => {
      return {
        message: ajvError.message,
        field: this.getErrorFieldName(ajvError),
        location: ajvError.location,
        rule: ajvError.keyword,
      };
    };
  }

  private addFormats(): { [formatName: string]: any } {
    return {
      hostname: CheckHostname,
      digits: (input) => new RegExp(ONLY_DIGITS_FORMAT_REGEXP).test(input),
      email: (input) => new RegExp(EMAIL_REGEXP).test(input),
      phone: (input) => new RegExp(PHONE_REGEXP).test(input),
      international_string_date: (input) => new RegExp(DATE_REGEXP).test(input),
      date: (input) => new RegExp(DATE_REGEXP).test(input),
      time: (input) => new RegExp(TIME_REGEXP).test(input),
    };
  }

  private getErrorFieldName(ajvError: any): string {
    const fieldLocationPrefixes = ['.body.', '.', '/'];

    if (typeof ajvError.params?.additionalProperty !== 'undefined') {
      return ajvError.params.additionalProperty;
    }

    if (typeof ajvError.params?.missingProperty !== 'undefined') {
      return ajvError.params.missingProperty;
    }

    return fieldLocationPrefixes.reduce((dataPath, prefix) => dataPath.replace(prefix, ''), ajvError.instancePath);
  }
}
