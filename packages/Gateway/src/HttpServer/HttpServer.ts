import * as Koa from 'koa';
import * as KoaRouter from 'koa-router';

import { Converter } from '@monki-island/common';
import { DependencyContainerCommonInterface } from '@monki-island/dependency-container';

import { EndpointInterface, EndpointConfiguration } from '../Endpoint';
import { HttpServerInterface } from './HttpServerInterface';

import {
  BodyParsingMiddleware,
  ErrorHandlingMiddleware,
  HtmlSanitizationMiddleware,
  MetaCollectionMiddleware,
  QueryParsingMiddleware,
  TracingMiddleware,
  AuthMiddleware,
} from '../Middleware';
import { Responder } from '../Responder';
import { InternalEndpoint } from '..';

export class HttpServer implements HttpServerInterface {
  constructor(private dependencyContainer: DependencyContainerCommonInterface) {
    this.httpServer = new Koa();
    this.router = new KoaRouter();
  }

  private httpServer: Koa;
  private router: KoaRouter;

  addEndpoint(endpoint: EndpointInterface): void {
    endpoint.setDependencyContainer(this.dependencyContainer);

    endpoint.setDispatcher(this.dependencyContainer.getDispatcher());

    const route = endpoint.getEndpoint();

    const middleware = [new TracingMiddleware(this.dependencyContainer.getObserver().tracer, route).middleware(), ...endpoint.getHandler()];

    this.router.register(route.path, [route.method], middleware);

    if (typeof route.alias === 'string') {
      const aliasedRoute: EndpointConfiguration = {
        method: route.method,
        path: route.alias,
      };

      const middleware = [new TracingMiddleware(this.dependencyContainer.getObserver().tracer, aliasedRoute).middleware(), ...endpoint.getHandler()];

      this.router.register(aliasedRoute.path, [aliasedRoute.method], middleware);
    }
  }

  addInternalEndpoint(endpoint: InternalEndpoint): void {
    endpoint.setDependencyContainer(this.dependencyContainer as any);
    const route = endpoint.getEndpoint();
    this.router.register(route.path, [route.method], [endpoint.getHandler()]);
  }

  startServer(): void {
    this.httpServer.use(new AuthMiddleware().middleware());
    this.httpServer.use(new ErrorHandlingMiddleware(new Responder(new Converter())).middleware());
    this.httpServer.use(new BodyParsingMiddleware().middleware());
    this.httpServer.use(new QueryParsingMiddleware().middleware());
    this.httpServer.use(new HtmlSanitizationMiddleware().middleware());
    this.httpServer.use(new MetaCollectionMiddleware().middleware());
    this.httpServer.use(this.router.routes());
    this.httpServer.listen(81);
  }
}
