import { InternalEndpoint } from '..';
import { EndpointInterface } from '../Endpoint';

export interface HttpServerInterface {
  addEndpoint(endpoint: EndpointInterface): void;
  addInternalEndpoint(endpoint: InternalEndpoint): void;
  startServer(): void;
}
