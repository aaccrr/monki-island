import { OpenAPIV3 } from 'openapi-types';
import { HttpRequestPayload } from '..';

export interface EndpointSpecificationInterface {
  validate(ctx: HttpRequestPayload): void;
  getMethod(): string;
  getPath(): string;
  getSpecification(): OpenAPIV3.OperationObject;
  getComponents(): OpenAPIV3.ComponentsObject;
}
