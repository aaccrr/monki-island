import { OpenAPIV3 } from 'openapi-types';
import { HttpRequestPayload } from '../Interface';
import { Validator } from '../Openapi';
import { EndpointSpecificationInterface } from './EndpointSpecificationInterface';

export class EndpointSpecification implements EndpointSpecificationInterface {
  constructor(private method: string, private path: string) {}

  protected description = '';
  protected tags: string[] = [];
  protected request: Partial<OpenAPIV3.OperationObject> = {};
  protected response: Partial<OpenAPIV3.ResponsesObject> = {};
  protected components: OpenAPIV3.ComponentsObject = {
    schemas: {},
  };

  validate(ctx: HttpRequestPayload): void {
    const openapiSchemaValidator = new Validator(this.request);

    openapiSchemaValidator.validate({
      query: ctx.request.query,
      params: ctx.params,
      body: ctx.request.body,
      headers: ctx.headers,
    });
  }

  getMethod(): string {
    return this.method;
  }

  getPath(): string {
    return this.path;
  }

  getSpecification(): OpenAPIV3.OperationObject {
    return {
      ...this.request,
      responses: this.response,
      description: this.description,
      tags: this.tags,
    };
  }

  getComponents(): OpenAPIV3.ComponentsObject {
    return this.components;
  }

  protected addSchema(schemaName: string, schema: OpenAPIV3.SchemaObject): void {
    this.components.schemas[schemaName] = schema;
  }
}
