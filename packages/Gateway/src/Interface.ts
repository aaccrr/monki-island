import { Tracing } from '@monki-island/observability';

export interface HttpRequestPayload {
  [key: string]: any;
  state: {
    trace: Tracing.TraceCarrier;
    session: any;
    payload: any;
    output: any;
    [key: string]: any;
  };
}

export type Middleware = (ctx: HttpRequestPayload, next: any) => void;

export interface MiddlewareInterface {
  middleware(): Middleware;
}
