import { OpenAPIV3 } from 'openapi-types';
import { Tracing } from '@monki-island/observability';
import { HttpRequestPayload } from '../Interface';

export interface EndpointConfiguration {
  method: 'get' | 'post' | 'put' | 'delete';
  path: string;
  alias?: string;
  group?: any[];
  response?: EndpointResponseConfiguration;
}

export interface EndpointResponseConfiguration {
  format: 'JSON' | 'XML';
  withSuccess: boolean;
}

export interface EndpointHandlerContext<P = any> {
  getDependencyContainer<T>(): T;
  dispatchSignal<T, Y = void>(signalName: string, signalPayload: T): Promise<Y>;
  payload: P;
}

export interface EndpointGuard {}

export interface IVmuzeyHttpEndpointAccessPolicy {
  auth: boolean;
  group: any[];
}

export type IVmuzeyHttpEndpointInputAdapter<T = any> = (ctx: HttpRequestPayload) => T;
export type IVmuzeyHttpEndpointOutputAdapter<T = any, Y = any> = (response: T) => Y;
export type IVmuzeyHttpEndpointProcessor<T = any, R = any> = (signal: T) => Promise<R>;

export interface IVmuzeyHttpEndpointPayload<T = any> {
  payload: T;
  trace: Tracing.TraceCarrier;
  session: any;
}

export interface IVmuzeyHttpEnpointConfig {
  method: 'get' | 'post' | 'put' | 'delete';
  path: string;
}

export interface EndpointSchemaInterface {
  validate(ctx: HttpRequestPayload): void;
  getSpecification(): OpenAPIV3.OperationObject;
}
