import { OpenAPIV3 } from 'openapi-types';
import { DispatcherInterface } from '@monki-island/transport';
import { Converter } from '@monki-island/common';
import { HttpRequestPayload, Middleware } from '../Interface';
import { Responder } from '../Responder';
import { EndpointInterface } from './EndpointInterface';
import { EndpointConfiguration, EndpointGuard, EndpointHandlerContext, EndpointSchemaInterface } from './Interface';
import { EndpointSpecificationInterface } from '../EndpointSpecification';
import { DependencyContainerCommonInterface } from '@monki-island/dependency-container';

export class Enpoint implements EndpointInterface {
  private dependencyContainer: DependencyContainerCommonInterface;
  protected dispatcher: DispatcherInterface;

  protected endpoint: EndpointConfiguration;
  protected specification: EndpointSpecificationInterface;
  protected middleware: Middleware[] = [];
  protected guard: EndpointGuard[] = [];

  private responder = new Responder(new Converter());

  getEndpoint(): EndpointConfiguration {
    return this.endpoint;
  }

  getHandler(): Middleware[] {
    return [this.handlerMiddleware()];
  }

  setDependencyContainer(dependencyContainer: DependencyContainerCommonInterface): void {
    this.dependencyContainer = dependencyContainer;
  }

  setDispatcher(dispatcher: DispatcherInterface): void {
    this.dispatcher = dispatcher;
  }

  getSpecification(): EndpointSpecificationInterface {
    return this.specification;
  }

  private handlerMiddleware(): Middleware {
    return async (ctx) => {
      this.specification.validate(ctx);

      const requestPayload = this.adaptInput(ctx);
      const response = await this.handleRequest(this.createHandlerContext(ctx, requestPayload));
      const output = this.adaptOutput(response);

      this.responder.reply(ctx, 200, output, {
        format: this.endpoint?.response?.format,
        withSuccess: this.endpoint?.response?.withSuccess,
      });
    };
  }

  protected adaptInput(input: HttpRequestPayload): any {
    return {};
  }

  protected handleRequest(context: EndpointHandlerContext<any>): any {
    return {};
  }

  protected adaptOutput(response: any): any {
    return response;
  }

  private createHandlerContext(ctx: HttpRequestPayload, payload: any): EndpointHandlerContext {
    const dispatchSignal = <T, Y = void>(signalName: string, signalPayload: T): Promise<Y> => {
      return this.dispatcher.dispatchSignal<T, Y>(signalName, {
        payload: signalPayload,
        trace: ctx.state.trace,
      } as any);
    };

    return {
      dispatchSignal,
      payload,
      getDependencyContainer: <T>(): T => {
        return this.dependencyContainer as any;
      },
    };
  }
}
