import { OpenAPIV3 } from 'openapi-types';
import { HttpRequestPayload } from '../Interface';
import { Validator } from '../Openapi';
import { EndpointSchemaInterface } from './Interface';

export class EndpointSchema implements EndpointSchemaInterface {
  protected request: OpenAPIV3.OperationObject;
  protected response: OpenAPIV3.ResponsesObject;

  validate(ctx: HttpRequestPayload): void {
    const openapiSchemaValidator = new Validator(this.request);

    openapiSchemaValidator.validate({
      query: ctx.request.query,
      params: ctx.params,
      body: ctx.request.body,
      headers: ctx.headers,
    });
  }

  getSpecification(): OpenAPIV3.OperationObject {
    return {
      ...this.request,
      responses: this.response,
    };
  }
}
