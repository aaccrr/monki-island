import { DispatcherInterface } from '@monki-island/transport';
import { Middleware } from '../Interface';
import { EndpointConfiguration } from './Interface';
import { EndpointSpecificationInterface } from '../EndpointSpecification';
import { DependencyContainerCommonInterface } from '@monki-island/dependency-container';

export interface EndpointInterface {
  getEndpoint(): EndpointConfiguration;
  getHandler(): Middleware[];

  setDispatcher(dispatcher: DispatcherInterface): void;
  setDependencyContainer(dependencyContainer: DependencyContainerCommonInterface): void;
  getSpecification(): EndpointSpecificationInterface;
}
