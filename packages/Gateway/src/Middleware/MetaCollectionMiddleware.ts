import { HttpRequestPayload, Middleware, MiddlewareInterface } from '../Interface';

export class MetaCollectionMiddleware implements MiddlewareInterface {
  middleware(): Middleware {
    return async (ctx: HttpRequestPayload, next) => {
      ctx.state.meta = {
        clientIp: ctx.headers['x-real-ip'] || ctx.headers['x-forwarded-for'] || ctx.headers['client-ip'],
      };

      await next();
    };
  }
}
