import { HttpRequestPayload } from '../Interface';

export class AuthMiddleware {
  middleware(): any {
    return async (ctx: HttpRequestPayload, next) => {
      const _auth = ctx.request.query._auth;

      if (_auth) {
        ctx.state._auth = _auth.split(',').reduce((acc, prop: any) => {
          let [key, value] = prop.split(':');
          if (key === 'm') value = value === 'all' ? value : value.split(';');
          acc[key] = value;
          return acc;
        }, {} as any);

        delete ctx.request.query._auth;
      }

      await next();
    };
  }
}
