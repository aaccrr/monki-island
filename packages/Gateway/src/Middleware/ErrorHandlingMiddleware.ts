import { BusinessError } from '@monki-island/error';
import { MiddlewareInterface, Middleware, HttpRequestPayload } from '../Interface';
import { ResponderInterface } from '../Responder';

export class ErrorHandlingMiddleware implements MiddlewareInterface {
  constructor(private responder: ResponderInterface) {}

  middleware(): Middleware {
    return async (ctx: HttpRequestPayload, next) => {
      try {
        await next();
      } catch (e) {
        if (e.type === 'BUSINESS_ERROR') {
          return this.responder.reply(ctx, e.httpCode, e.format());
        }

        if (e.type === 'VALIDATION_ERROR') {
          return this.responder.reply(ctx, e.httpCode, e.format());
        }

        const code = this.normalizeErrorCode(e.code);

        if (e.isJson) {
          const message = this.handleJsonError(e);
          return this.responder.reply(ctx, code, message);
        }

        if (code === 500) {
          const internallError = new BusinessError('ru', 'INTERNAL_ERROR');
          return this.responder.reply(ctx, internallError.httpCode, internallError.format());
        }

        this.responder.reply(ctx, code, e.message);
      }
    };
  }

  normalizeErrorCode(code: number | string): number {
    if (!code) return 500;
    if (typeof code === 'string') return 500;
    if ((typeof code === 'number' && code < 100) || code > 526) return 500;
    return code;
  }

  handleInternalError() {
    return {
      message: 'INTERNAL_ERROR',
    };
  }

  handleJsonError(e) {
    try {
      const parsedMessage = JSON.parse(e.message);

      return parsedMessage;
    } catch (err) {
      return e.message;
    }
  }
}
