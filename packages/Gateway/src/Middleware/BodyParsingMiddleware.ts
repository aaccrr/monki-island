import * as bodyParser from 'koa-body';
import { Middleware, MiddlewareInterface } from '../Interface';

export class BodyParsingMiddleware implements MiddlewareInterface {
  middleware(): Middleware {
    return bodyParser({ multipart: true });
  }
}
