import { Tracing } from '@monki-island/observability';
import { Middleware, MiddlewareInterface } from '../Interface';
import { EndpointConfiguration } from '../Endpoint';

export class TracingMiddleware implements MiddlewareInterface {
  constructor(private tracer: Tracing.TracerInterface, private endpoint: EndpointConfiguration) {}

  middleware(): Middleware {
    return async (ctx, next) => {
      const span = this.tracer.startSpan({
        name: `${this.endpoint.method} ${this.endpoint.path}`,
      });

      span.withPayload({
        body: ctx.request.body,
        query: ctx.request.query,
        params: ctx.params,
        auth: ctx.state._auth,
      });

      ctx.state.trace = span.extractContext();

      try {
        await next();
        span.withResponse({});
      } catch (e) {
        span.withError(e);
        throw e;
      }
    };
  }
}
