import { stripHtml } from 'string-strip-html';
import { Middleware, MiddlewareInterface } from '../Interface';

const isCuttedValue = (value) => typeof value === 'string';
const isNestedValue = (value) => typeof value === 'object' && value !== null;
const isArrayValue = (value) => Array.isArray(value);

const cutHtmlInString = (str) => stripHtml(str).result;

const cutPropOfAnyType = (obj, prop) => {
  if (isCuttedValue(obj[prop])) {
    obj[prop] = cutHtmlInString(obj[prop]);
    return;
  }

  if (isArrayValue(obj[prop])) {
    obj[prop].forEach((_, index) => cutPropOfAnyType(obj[prop], index));
    return;
  }

  if (isNestedValue(obj[prop])) {
    Object.keys(obj[prop]).forEach((nestedProp) => cutPropOfAnyType(obj[prop], nestedProp));
    return;
  }
};

const cutObjectType = (rootObject) => {
  if (typeof rootObject === 'undefined' || rootObject === null) return;
  return Object.keys(rootObject).forEach((rootObjKey) => cutPropOfAnyType(rootObject, rootObjKey));
};

export class HtmlSanitizationMiddleware implements MiddlewareInterface {
  middleware(): Middleware {
    return async (ctx, next) => {
      cutObjectType(ctx.request.body);
      cutObjectType(ctx.request.query);
      cutObjectType(ctx.params);
      cutObjectType(ctx.headers);
      await next();
    };
  }
}
