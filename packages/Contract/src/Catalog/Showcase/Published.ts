import * as DataModel from '@monki-island/data-model';

export interface Published {
  showcase: DataModel.Catalog.Showcase.Showcase;
}
