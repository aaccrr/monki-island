import * as DataModel from '@monki-island/data-model';

export interface Viewed {
  showcase: DataModel.Catalog.Showcase.Showcase;
  customerId: string;
}
