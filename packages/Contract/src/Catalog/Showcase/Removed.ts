import * as DataModel from '@monki-island/data-model';

export interface Removed {
  showcase: DataModel.Catalog.Showcase.Showcase;
}
