import * as DataModel from '@monki-island/data-model';

export interface Liked {
  showcase: DataModel.Catalog.Showcase.Showcase;
  customerId: string;
}
