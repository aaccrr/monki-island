import * as DataModel from '@monki-island/data-model';

export interface Disliked {
  showcase: DataModel.Catalog.Showcase.Showcase;
  customerId: string;
}
