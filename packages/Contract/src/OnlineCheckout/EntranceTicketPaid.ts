import { IItemModel, PaymentModel } from '@monki-island/data-model';

export interface EntranceTicketPaid {
  entranceTicket: IItemModel;
  payment: PaymentModel;
  timestamp: number;
}
