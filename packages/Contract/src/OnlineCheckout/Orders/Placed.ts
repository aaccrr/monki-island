import * as OnlineCheckout from '@data-model/online_checkout';
import * as DataModel from '@monki-island/data-model';

export interface Placed {
  order: OnlineCheckout.Order.Order;
  e_tickets: DataModel.IItemModel[];
}
