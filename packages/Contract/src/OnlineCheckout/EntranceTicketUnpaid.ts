import { IItemModel } from '@monki-island/data-model';

export interface EntranceTicketUnpaid {
  entranceTicket: IItemModel;
}
