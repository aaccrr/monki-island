import * as DataModel from '@data-model/online_checkout';

export interface Performed {
  refund: DataModel.Refund.Refund;
}
