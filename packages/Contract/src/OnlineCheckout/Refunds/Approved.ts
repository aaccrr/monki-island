import * as DataModel from '@data-model/online_checkout';

export interface Approved {
  refund: DataModel.Refund.Refund;
}
