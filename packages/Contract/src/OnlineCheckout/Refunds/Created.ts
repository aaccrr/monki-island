import * as DataModel from '@data-model/online_checkout';

export interface Created {
  refund: DataModel.Refund.Refund;
}
