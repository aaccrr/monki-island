import * as DataModel from '@data-model/online_checkout';

export interface Rejected {
  refund: DataModel.Refund.Refund;
}
