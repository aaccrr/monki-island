import { IItemModel } from '@monki-island/data-model';

export interface EntranceTicketRefunded {
  entranceTicket: IItemModel;
  refund: any;
  timestamp: number;
}
