import * as DataModel from '@data-model/online_checkout';

export interface Canceled {
  purchase: DataModel.Order.Order;
}
