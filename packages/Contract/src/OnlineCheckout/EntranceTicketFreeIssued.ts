import { IItemModel } from '@monki-island/data-model';

export interface EntranceTicketFreeIssued {
  entranceTicket: IItemModel;
  timestamp: number;
}
