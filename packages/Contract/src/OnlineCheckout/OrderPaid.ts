import { PaymentModel } from '@monki-island/data-model';

export interface OrderPaid {
  order: any;
  payment: PaymentModel;
}
