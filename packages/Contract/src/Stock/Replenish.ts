import { Nullable, Timezone } from '@monki-island/common';
import { VisitConditionLimitModel, VisitConditionTypeEnum, VisitConditionValueModel } from '@monki-island/data-model';

export interface Replenish {
  id: string;
  productId: string;
  nomenclatureId: string;
  type: VisitConditionTypeEnum;
  value: VisitConditionValueModel;
  limit: VisitConditionLimitModel;
  outDate: Nullable<string>;
  timezone: Timezone;
}
