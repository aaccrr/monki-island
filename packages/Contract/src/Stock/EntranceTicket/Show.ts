export interface Show {
  productId: string;
  from: string;
  to: string;
}
