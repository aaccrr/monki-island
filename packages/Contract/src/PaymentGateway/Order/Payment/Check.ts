export interface Check {
  orderId: string;
  paymentType: 'debet_card' | 'pushkin_card';
  amount: string;
}
