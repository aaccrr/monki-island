export interface Fulfilled {
  orderId: string;
  paymentId: string;
  amount: string;
  rrn: string;
  timestamp: number;
}
