export interface Refunded {
  refundId: string;
  orderId: string;
  paymentId: string;
}
