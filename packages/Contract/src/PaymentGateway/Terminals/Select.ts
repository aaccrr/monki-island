import * as OnlineCheckout from '@data-model/online_checkout';

export interface Select {
  payment_type: OnlineCheckout.PaymentType;
}
