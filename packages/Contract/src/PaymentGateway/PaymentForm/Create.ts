export interface Create {
  orderId: string;
  amount: string;
  customerEmail: string;
  customerPhone: string;
  paymentType: 'debet_card' | 'pushkin_card';
}
