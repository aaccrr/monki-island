export interface Process {
  orderId: string;
  paymentId: string;
  amount: string;
  customer: string;
  sign: string;
}
