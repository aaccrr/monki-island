export interface Process {
  orderId: string;
  status: 'authorized' | 'paid' | 'canceled' | 'party canceled' | 'waiting';
  rrn: string;
  amount: string;
  signature: string;
}
