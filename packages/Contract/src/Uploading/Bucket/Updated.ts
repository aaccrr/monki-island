import * as DataModel from '@monki-island/data-model';

export interface Updated {
  bucket: DataModel.Uploading.Bucket.Bucket;
  content: DataModel.Uploading.Content.Item[];
}
