import { Reporting } from '@monki-island/data-model';

export interface Print {
  contragent: Reporting.ServiceProvider;
  sheet: Reporting.Counterfoils.Sheet.Full;
}
