import { Reporting } from '@monki-island/data-model';

export interface Print {
  contragentId: string;
  detalization: Reporting.Sales.Detalization.Sheet;
  visitObject: Reporting.Balance.VisitObject;
}
