import { Reporting } from '@monki-island/data-model';

export interface Print {
  contragentId: string;
  reportingPeriod: Reporting.ReportingPeriod;
  report: Reporting.Sales.Report.Sheet;
}
