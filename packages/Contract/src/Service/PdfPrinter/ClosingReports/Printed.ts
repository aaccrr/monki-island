import { Reporting, Service } from '@monki-island/data-model';

export interface Printed {
  contragentId: string;
  reportingPeriod: Reporting.ReportingPeriod;
  reports: Service.PdfPrinter.PrintedReport[];
}
