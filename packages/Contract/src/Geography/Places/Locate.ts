import * as Geography from '@data-model/geography';

export interface Locate {
  places: Geography.Place[];
}
