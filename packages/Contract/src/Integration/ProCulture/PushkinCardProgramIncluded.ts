export interface PushkinCardProgramIncluded {
  eventId: string;
  proCultureEventId: number;
}
