import { PaginationInterface, Nullable } from '@monki-island/common';

export interface EventListPaginate {
  proCultureId: number;
  pagination: PaginationInterface;
  q: Nullable<string>;
  headId: Nullable<number>;
}
