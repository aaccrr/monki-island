export interface OrganizationAccepted {
  organizationId: string;
  proCultureId: number;
}
