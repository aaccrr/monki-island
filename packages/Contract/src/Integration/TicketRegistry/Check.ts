interface CheckWithRegistryPeriod {
  start: number;
  finish: number;
}

export interface Check {
  period: CheckWithRegistryPeriod;
}
