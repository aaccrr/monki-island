import { IItemModel } from '@monki-island/data-model';

export interface EntranceTicketUsed {
  entranceTicket: IItemModel;
  timestamp: number;
}
