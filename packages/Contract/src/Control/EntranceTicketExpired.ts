import { IItemModel } from '@monki-island/data-model';

export interface EntranceTicketExpired {
  entranceTicket: IItemModel;
  timestamp: number;
}
