import { IItemModel } from '@monki-island/data-model';

export interface EntranceTicketVisited {
  entranceTicket: IItemModel;
  timestamp: number;
}
