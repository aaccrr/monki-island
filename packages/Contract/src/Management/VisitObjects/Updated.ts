import * as Management from '@data-model/management';

export interface Updated {
  visitObject: Management.VisitObject.VisitObject;
}
