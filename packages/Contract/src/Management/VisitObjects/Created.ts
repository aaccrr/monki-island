import * as Management from '@data-model/management';

export interface Created {
  visitObject: Management.VisitObject.VisitObject;
}
