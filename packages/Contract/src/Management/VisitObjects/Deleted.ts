import * as Management from '@data-model/management';

export interface Deleted {
  visitObject: Management.VisitObject.VisitObject;
}
