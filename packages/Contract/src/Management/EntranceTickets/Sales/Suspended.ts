// import * as Management from '@data-model/management';
import * as Common from '@monki-island/common';
import * as DataModel from '@monki-island/data-model';

export interface Suspended {
  ticket: DataModel.EntranceTicketModel;
  renewalDate: Common.Nullable<Date>;
}
