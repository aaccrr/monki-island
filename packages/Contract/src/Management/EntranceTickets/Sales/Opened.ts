// import * as Management from '@data-model/management';
import * as DataModel from '@monki-island/data-model';

export interface Opened {
  ticket: DataModel.EntranceTicketModel;
}
