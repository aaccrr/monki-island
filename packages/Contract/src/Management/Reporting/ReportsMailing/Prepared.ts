import { Service } from '@monki-island/data-model';

export interface Prepared {
  recipients: string[];
  reports: Service.PdfPrinter.PrintedReport[];
}
