export interface Setup {
  contragentId: string;
  recipients: string[];
}
