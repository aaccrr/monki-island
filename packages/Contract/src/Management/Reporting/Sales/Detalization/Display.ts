export interface Display {
  contragentId: string;
  productId: string;
  visitPeriodStart: string;
  visitPeriodFinish: string;
  paymentType: 'any' | 'debet_card' | 'pushkin_card';
  showZeroes: boolean;
}
