import { Service } from '@monki-island/data-model';

export interface Requested {
  printedSalesDetalization: Service.PdfPrinter.PrintedReport;
  recipients: string[];
}
