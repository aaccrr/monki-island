export interface Generate {
  contragentId: string;
  reportingPeriodStart: string;
  reportingPeriodFinish: string;
  visitObjects: string[];
  nomenclatureTypes: string[];
  paymentType: 'any' | 'debet_card' | 'pushkin_card';
  showZeroes: boolean;
}
