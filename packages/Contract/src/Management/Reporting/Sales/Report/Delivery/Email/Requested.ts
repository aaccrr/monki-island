import { Service } from '@monki-island/data-model';

export interface Requested {
  recipients: string[];
  printedReport: Service.PdfPrinter.PrintedReport;
}
