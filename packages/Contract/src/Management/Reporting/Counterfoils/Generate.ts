export interface Generate {
  contragentId: string;
  from: string;
  to: string;
}
