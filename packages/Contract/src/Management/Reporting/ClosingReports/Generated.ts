import { Reporting } from '@monki-island/data-model';

export interface Generated {
  contragent: Reporting.ServiceProvider;
  reportingPeriod: Reporting.ReportingPeriod;
  performedDonatedServicesReport: Reporting.ClosingReports.Report.PerformedDonatedServices;
  pushkinCardProgramRealizationReport: Reporting.ClosingReports.Report.PushkinCardProgramRealization;
}
