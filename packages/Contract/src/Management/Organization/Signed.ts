import { OrganizationModel } from '@monki-island/data-model';

export interface Signed {
  organization: OrganizationModel;
  timestamp: number;
}
