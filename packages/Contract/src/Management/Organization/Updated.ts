import { OrganizationModel } from '@monki-island/data-model';

export interface Updated {
  organization: OrganizationModel;
  timestamp: number;
}
