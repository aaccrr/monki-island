import { OrganizationModel } from '@monki-island/data-model';

export interface Deleted {
  organization: OrganizationModel;
  timestamp: number;
}
