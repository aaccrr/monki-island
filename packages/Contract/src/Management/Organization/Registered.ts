import { OrganizationModel, OrganizationStaffPersonalInformationModel } from '@monki-island/data-model';

export interface Registered {
  organization: OrganizationModel;
  primaryManager: OrganizationStaffPersonalInformationModel;
  booker: OrganizationStaffPersonalInformationModel;
}
