import { EventPositionModel } from '@monki-island/data-model';

export interface Deleted {
  eventPosition: EventPositionModel;
  timestamp: number;
}
