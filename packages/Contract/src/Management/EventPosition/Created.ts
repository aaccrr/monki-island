import { EventPositionModel } from '@monki-island/data-model';

export interface Created {
  eventPosition: EventPositionModel;
  timestamp: number;
}
