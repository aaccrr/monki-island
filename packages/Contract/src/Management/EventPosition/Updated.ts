import { EventPositionModel } from '@monki-island/data-model';

export interface Updated {
  eventPosition: EventPositionModel;
  update: Partial<EventPositionModel>;
  timestamp: number;
}
