import * as DataModel from '@monki-island/data-model';

export interface Canceled {
  eventPosition: DataModel.EventPositionModel;
}
