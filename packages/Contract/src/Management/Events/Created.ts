// import * as Management from '@data-model/management';
import * as Integrations from '@data-model/integrations';
import * as DataModel from '@monki-island/data-model';

export interface Created {
  event: DataModel.EventModel;
  exchange: Integrations.Exchange.Events.Event;
}
