// import * as Management from '@data-model/management';
import * as DataModel from '@monki-island/data-model';
import * as Integrations from '@data-model/integrations';

export interface Updated {
  event: DataModel.EventModel;
  exchange: Integrations.Exchange.Events.Event;
}
