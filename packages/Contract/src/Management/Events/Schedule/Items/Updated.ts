import * as Management from '@data-model/management';

export interface Updated {
  item: Management.Event.Schedule.Item;
}
