import * as Management from '@data-model/management';

export interface Canceled {
  item: Management.Event.Schedule.Item;
}
