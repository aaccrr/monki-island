import * as Management from '@data-model/management';

export interface Deleted {
  item: Management.Event.Schedule.Item;
}
