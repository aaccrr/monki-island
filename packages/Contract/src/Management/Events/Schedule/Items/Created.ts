import * as Management from '@data-model/management';

export interface Created {
  item: Management.Event.Schedule.Item;
}
