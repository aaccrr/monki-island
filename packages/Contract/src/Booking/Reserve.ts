export interface Reserve {
  orderId: string;
  stockItemId: string;
  quantity: number;
}
