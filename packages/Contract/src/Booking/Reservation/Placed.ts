export interface Placed {
  id: string;
  placedAt: Date;
}
