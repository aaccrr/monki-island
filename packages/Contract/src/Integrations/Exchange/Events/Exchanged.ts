import * as DataModel from '@data-model/integrations';

export interface Exchanged {
  event: DataModel.Exchange.Events.Event;
}
