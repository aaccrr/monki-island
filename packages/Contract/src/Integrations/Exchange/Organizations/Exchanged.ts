import * as DataModel from '@data-model/integrations';

export interface Exchanged {
  organization: DataModel.Exchange.Organizations.Organization;
}
