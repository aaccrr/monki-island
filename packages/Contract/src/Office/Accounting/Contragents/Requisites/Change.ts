export interface Change {
  id: string;
  requisites: {
    fullName: string;
    address: string;
    director: string;
    checkingAccount: string;
    correspondentAccount: string;
    bank: string;
    bik: string;
    okpo: string;
    paymentRecipient: string;
    kbk: string;
    ogrn: string;
    inn: string;
    kpp: string;
    okato: string;
    signingDateOfPushkinCardContract: string;
    undisputedFeeRetention: boolean;
  };
}
