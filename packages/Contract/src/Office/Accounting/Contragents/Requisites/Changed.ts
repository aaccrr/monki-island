import { OrganizationModel } from '@monki-island/data-model';

export interface Changed {
  contragent: OrganizationModel;
}
