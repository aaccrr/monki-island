import * as DataModel from '@monki-island/data-model';

export interface Created {
  period: DataModel.Accounting.Period;
}
