import * as DataModel from '@data-model/accounting';

export interface Printed {
  closing_act: DataModel.ClosingActs.Act;
}
