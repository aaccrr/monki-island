import * as DataModel from '@data-model/accounting';

export interface Updated {
  counteragent: DataModel.Counteragents.Counteragent;
}
