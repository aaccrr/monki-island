import * as DataModel from '@data-model/accounting';

export interface Close {
  counteragent_id: string;
  period: DataModel.Period;
}
