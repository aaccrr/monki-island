import * as DataModel from '@data-model/accounting';

export interface Closed {
  period: DataModel.Period;
}
