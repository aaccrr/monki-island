export const Products = {
  EntranceTickets: {
    Count: 'catalog.products.entrance_tickets.count',
  },
  SeasonTickets: {
    Count: 'catalog.products.season_tickets.count',
  },
  List: 'catalog.products.list',
};
