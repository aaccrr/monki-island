export const Refunds = {
  Created: 'online_checkout.refunds.created',
  Approved: 'online_checkout.refunds.approved',
  Rejected: 'online_checkout.refunds.rejected',
  Performed: 'online_checkout.refunds.performed',
};
