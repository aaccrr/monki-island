export const Purchases = {
  Canceled: 'online_checkout.purchases.canceled',
  Refunded: 'online_checkout.purchases.refunded',
};
