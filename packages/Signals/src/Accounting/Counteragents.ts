export const Counteragents = {
  List: 'Accounting.Counteragents.List',
  Get: 'Accounting.Counteragents.Get',
  Requisites: {
    Updated: 'Accounting.Counteragents.Requisites.Updated',
  },
};
