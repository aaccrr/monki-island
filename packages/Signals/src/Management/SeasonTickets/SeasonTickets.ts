export const SeasonTickets = {
  Created: 'management.season_tickets.created',
  Updated: 'management.season_tickets.updated',
  Deleted: 'management.season_tickets.deleted',
  Canceled: 'management.season_tickets.canceled',
};
