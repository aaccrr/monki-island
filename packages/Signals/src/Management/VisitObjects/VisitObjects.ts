export const VisitObjects = {
  List: 'management.visit_objects.list',
  Created: 'management.visit_objects.created',
  Updated: 'management.visit_objects.updated',
  Deleted: 'management.visit_objects.deleted',
};
