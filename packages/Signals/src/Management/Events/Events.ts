export const Events = {
  Created: 'management.events.created',
  Updated: 'management.events.updated',
  Deleted: 'management.events.deleted',
  Canceled: 'management.events.canceled',
  Schedule: {
    Items: {
      Created: 'management.events.schedule.items.created',
      Updated: 'management.events.schedule.items.updated',
      Deleted: 'management.events.schedule.items.deleted',
      Canceled: 'management.events.schedule.items.canceled',
    },
  },
};
