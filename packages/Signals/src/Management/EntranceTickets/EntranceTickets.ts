export const EntranceTickets = {
  Created: 'management.entrance_tickets.created',
  Updated: 'management.entrance_tickets.updated',
  Deleted: 'management.entrance_tickets.deleted',
  Canceled: 'management.entrance_tickets.canceled',
  Sales: {
    Opened: 'management.entrance_tickets.sales.opened',
    Suspended: 'management.entrance_tickets.sales.suspended',
  },
};
