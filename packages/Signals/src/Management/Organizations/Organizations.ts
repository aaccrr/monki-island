export const Organizations = {
  Get: 'management.organizations.get',
  Registered: 'organization_management.organization.registered',
};
