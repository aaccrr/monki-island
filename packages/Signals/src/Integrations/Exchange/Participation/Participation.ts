export const Participation = {
  Admitted: 'integrations.exchange.participation.admitted',
  Excluded: 'integrations.exchange.participation.excluded',
};
