export const Events = {
  Get: 'integrations.exchange.events.get',
  Exchange: 'integrations.exchange.events.exchange',
  Exchanged: 'integrations.exchange.events.exchanged',
};
