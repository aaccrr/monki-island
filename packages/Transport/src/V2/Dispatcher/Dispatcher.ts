import { BusinessError, ValidationError } from '@monki-island/error';

import { TransporterInterface } from '../../Transporter';
import { SignalInterface } from '../../Signal';
import { DispatcherInterface } from './DispatcherInterface';
import { SignalTrace } from '.';

export class Dispatcher implements DispatcherInterface {
  constructor(private transporter: TransporterInterface) {}

  async dispatchSignal<T, Y = void>(name: string, signalPayload: T): Promise<Y> {
    // подменяется прокси
    const signal = signalPayload as any as SignalInterface<T>;

    if (name.endsWith('ed') || name.endsWith('id')) {
      await this.dispatchPublishSignal(name, signal);
      return;
    }

    return this.dispatchRequestSignal(name, signal);
  }

  async dispatchSignalWithTrace<T, Y = void>(name: string, signalPayload: SignalTrace<T>): Promise<Y> {
    if (name.endsWith('ed') || name.endsWith('id')) {
      await this.dispatchPublishSignal(name, signalPayload);
      return;
    }

    return this.dispatchRequestSignal(name, signalPayload);
  }

  private async dispatchPublishSignal(name: string, signal: SignalInterface): Promise<void> {
    try {
      await this.transporter.publish(name, signal);
    } catch (e) {
      throw e;
    }
  }

  private async dispatchRequestSignal(name: string, signal: SignalInterface): Promise<any> {
    try {
      const response = await this.transporter.request(name, signal);

      if (response.error) {
        this.throwResposeError(response.data);
      }

      return response.data;
    } catch (e) {
      throw e;
    }
  }

  private throwResposeError(responseError: any) {
    if (responseError.type === 'BUSINESS_ERROR') {
      const error = new BusinessError(responseError.locale, responseError.code);
      error.stack = responseError.stacktrace;
      throw error;
    }

    if (responseError.type === 'VALIDATION_ERROR' && responseError.isPublic) {
      const error = new ValidationError(responseError.locale, responseError.errors);
      error.stack = responseError.stacktrace;
      throw error;
    }

    throw new BusinessError('ru', 'INTERNAL_ERROR');
  }
}
