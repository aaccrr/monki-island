import { connect, JetStreamClient, JetStreamManager, consumerOpts, NatsConnection, ConsumerOptsBuilder, StreamConfig, createInbox, Msg } from 'nats';
import { RecieverInterface, RecieverFactoryInterface } from '../Reciever';
import { TransporterConfiguration } from './Interface';
import { TransporterInterface } from './TransporterInterface';

export class Transporter implements TransporterInterface {
  constructor(private config: TransporterConfiguration) {}

  private connection: NatsConnection;
  private streamingManager: JetStreamManager;
  private streamingConnection: JetStreamClient;

  async init(): Promise<void> {
    try {
      await this.initializeTransportSystemConnection();
    } catch (e) {
      console.log('nats connection error: ', e);
    }
  }

  disconnect(): void {}

  async publish<T>(signal: string, signalPayload: T): Promise<void> {
    await this.connection.publish(signal, this.serializeSignalPayload(signalPayload));
  }

  async request<T = any, Y = any>(signalName: string, signalPayload: T): Promise<Y> {
    const responseSignal = await this.connection.request(signalName, this.serializeSignalPayload(signalPayload), { timeout: 30000 });
    return this.parseSignalPayload(responseSignal.data);
  }

  async subscribe(signalName: string, recieverFactory: RecieverFactoryInterface): Promise<void> {
    this.connection.subscribe(signalName, {
      queue: this.config.serviceName,
      callback: async (err, signal) => {
        for (const reciever of recieverFactory.create()) {
          await this.processSignal(signal, reciever);
        }
      },
    });
  }

  private async processSignal(signal: Msg, reciever: RecieverInterface): Promise<void> {
    try {
      const signalPayload = this.parseSignalPayload(signal.data);
      const signalProcessingResult = await reciever.recieve(signal.subject, signalPayload);
      signal.respond(this.serializeSignalPayload({ error: false, data: signalProcessingResult }));
    } catch (e) {
      signal.respond(this.serializeSignalPayload({ error: true, data: this.serializeErrorData(e) }));
    }
  }

  private serializeErrorData(e: any): any {
    if (e.type === 'BUSINESS_ERROR' || e.type === 'VALIDATION_ERROR') {
      return e;
    }

    return {
      message: e.message,
    };
  }

  private async subscribeStream(signalName: string, reciever: RecieverInterface): Promise<void> {
    await this.ensureStream(signalName);

    try {
      const subscription = await this.streamingConnection.subscribe(signalName, this.getSubscriptionOption());

      for await (const signal of subscription) {
        const signalPayload = this.parseSignalPayload(signal.data);
        await reciever.recieve(signalName, signalPayload);
        signal.ack();
      }
    } catch (subSignalError) {
      console.log(`sub "${signalName}" signal error: `, subSignalError.message);
    }
  }

  private getSubscriptionOption(): ConsumerOptsBuilder {
    const subscriptionOption = consumerOpts();
    subscriptionOption.durable(this.config.serviceName);
    subscriptionOption.manualAck();
    subscriptionOption.ackExplicit();
    subscriptionOption.deliverTo(createInbox());
    return subscriptionOption;
  }

  private async ensureStream(signalName: string): Promise<void> {
    const streamName = signalName.split('.')[0];
    const streamConfig = await this.getStreamConfig(streamName);
    const consumers = await this.streamingManager.consumers.list(streamName).next();

    if (!streamConfig.subjects.includes(signalName)) {
      streamConfig.subjects.push(signalName);
      await this.streamingManager.streams.update(streamConfig);
    }
  }

  private async getStreamConfig(streamName: string): Promise<StreamConfig> {
    try {
      const stream = await this.streamingManager.streams.add({ name: streamName, subjects: [`${streamName}.*`] });
      return stream.config;
    } catch (addStreamError) {
      console.log(`add "${streamName}" stream error: `, addStreamError.message);

      if (addStreamError.message === 'stream name already in use') {
        try {
          const stream = await this.streamingManager.streams.info(streamName);
          return stream.config;
        } catch (findStreamError) {
          console.log(`find "${streamName}" stream error: `, findStreamError.message);
        }
      }
    }
  }

  private parseSignalPayload(signalPayload: Uint8Array): any {
    const signalPayloadString = Buffer.from(signalPayload).toString('utf8');
    return JSON.parse(signalPayloadString);
  }

  private serializeSignalPayload(signalPayload: any): Uint8Array {
    const signalPayloadString = JSON.stringify(signalPayload);
    return new Uint8Array(Buffer.from(signalPayloadString, 'utf8'));
  }

  private async initializeTransportSystemConnection(): Promise<void> {
    this.connection = await connect({
      servers: this.config.host,
      name: this.config.serviceName,
    });
  }

  private async initializeStreaming(): Promise<void> {
    this.streamingConnection = await this.connection.jetstream();
    this.streamingManager = await this.connection.jetstreamManager();
  }
}
