import { RecieverFactoryInterface } from '../Reciever';

export interface TransporterInterface {
  publish<T>(signalName: string, signalPayload: T): Promise<void>;
  request<T = any, Y = any>(signalName: string, signalPayload: T): Promise<Y>;
  subscribe(signalName: string, recieverFactory: RecieverFactoryInterface): Promise<void>;

  init(): Promise<void>;
  disconnect(): void;
}
