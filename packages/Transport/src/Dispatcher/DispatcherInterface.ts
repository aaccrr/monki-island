export interface SignalTrace<T> {
  trace: any;
  payload: T;
}

export interface DispatcherInterface {
  dispatchSignal<T, Y = void>(name: string, signalPayload: T): Promise<Y>;
  dispatchSignalWithTrace<T, Y = void>(name: string, signalPayload: SignalTrace<T>): Promise<Y>;
}
