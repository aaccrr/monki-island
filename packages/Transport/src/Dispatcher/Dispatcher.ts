import { Observer } from '@monki-island/observability';
import { BusinessError, ValidationError } from '@monki-island/error';

import { TransporterInterface } from '../Transporter';
import { SignalInterface } from '../Signal';
import { DispatcherInterface } from './DispatcherInterface';
import { SignalTrace } from '.';

export class Dispatcher implements DispatcherInterface {
  constructor(private transporter: TransporterInterface, private observer: Observer.Observer) {}

  async dispatchSignal<T, Y = void>(name: string, signalPayload: T): Promise<Y> {
    // подменяется прокси
    const signal = signalPayload as any as SignalInterface<T>;

    if (name.endsWith('ed') || name.endsWith('id')) {
      await this.dispatchPublishSignal(name, signal);
      return;
    }

    return this.dispatchRequestSignal(name, signal);
  }

  async dispatchSignalWithTrace<T, Y = void>(name: string, signalPayload: SignalTrace<T>): Promise<Y> {
    if (name.endsWith('ed') || name.endsWith('id')) {
      await this.dispatchPublishSignal(name, signalPayload);
      return;
    }

    return this.dispatchRequestSignal(name, signalPayload);
  }

  private async dispatchPublishSignal(name: string, signal: SignalInterface): Promise<void> {
    const spanContext = {
      name,
    };

    await this.observer.collector.collectMetric(name, signal.payload);
    const span = this.observer.tracer.linkSpan(spanContext, signal.trace);
    span.withPayload(signal.payload);

    try {
      await this.transporter.publish(name, signal);
      span.withResponse({});
    } catch (e) {
      span.withError(e);
    }
  }

  private async dispatchRequestSignal(name: string, signal: SignalInterface): Promise<any> {
    const spanContext = {
      name: `DISPATCHER ${name}`,
    };

    const span = this.observer.tracer.forkSpan(spanContext, signal.trace);
    span.withPayload(signal.payload);

    try {
      const response = await this.transporter.request(name, signal);

      if (response.error) {
        this.throwResposeError(response.data);
      }

      span.withResponse(response.data);
      return response.data;
    } catch (e) {
      span.withError(e);
      throw e;
    }
  }

  private throwResposeError(responseError: any) {
    if (responseError.type === 'BUSINESS_ERROR') {
      const error = new BusinessError(responseError.locale, responseError.code);
      error.stack = responseError.stacktrace;
      throw error;
    }

    if (responseError.type === 'VALIDATION_ERROR' && responseError.isPublic) {
      const error = new ValidationError(responseError.locale, responseError.errors);
      error.stack = responseError.stacktrace;
      throw error;
    }

    throw new BusinessError('ru', 'INTERNAL_ERROR');
  }
}
