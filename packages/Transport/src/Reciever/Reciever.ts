import { Tracing } from '@monki-island/observability';
import { RecieverInterface } from './RecieverInterface';

import { DispatcherInterface } from '../Dispatcher';
import { SignalInterface } from '../Signal';
import { RecieverAdapter } from './RecieverInterface';

export abstract class Reciever implements RecieverInterface {
  protected dispatcher: DispatcherInterface;
  protected signal: string[] = [];
  protected adapter: RecieverAdapter = {};

  private tracer: Tracing.TracerInterface;
  private span: Tracing.SpanInterface;
  protected trace: Tracing.TraceCarrier = null;

  protected abstract process(signalPayload: any): Promise<any>;

  protected dispatchSignal<T, Y = void>(signalName: string, signalPayload: T): Promise<Y> {
    return this.dispatcher.dispatchSignal<T, Y>(signalName, signalPayload);
  }

  getSignal(): string[] {
    return this.signal;
  }

  setDispatcher(dispatcher: DispatcherInterface): void {
    this.dispatcher = dispatcher;
  }

  setTracer(tracer: Tracing.TracerInterface): void {
    this.tracer = tracer;
  }

  async recieve(signalName: string, signalPayload: SignalInterface): Promise<any> {
    this.enableTracing(signalName, signalPayload.trace);

    try {
      this.trace = signalPayload.trace;
      const recieverPayload = this.adaptSignalPayload(signalName, signalPayload);
      this.span.withPayload(recieverPayload);

      const recieverResponse = await this.process.call(this.createHandlerContext(this.span.extractContext()), recieverPayload);
      this.span.withResponse(recieverResponse);

      return recieverResponse;
    } catch (e) {
      this.span.withError(e);
      throw e;
    } finally {
      this.trace = null;
    }
  }

  private enableTracing(signalName: string, trace: Tracing.TraceCarrier): void {
    this.span = this.tracer.forkSpan({ name: `RECIEVER ${signalName}` }, trace);
  }

  private adaptSignalPayload(signalName: string, signalPayload: SignalInterface): RecieverAdapter {
    const recieverAdapter = this.adapter[signalName];

    if (!recieverAdapter) {
      return signalPayload.payload || signalPayload;
    }

    return recieverAdapter(signalPayload.payload || signalPayload);
  }

  private createHandlerContext(trace: Tracing.TraceCarrier): this {
    const dispatchSignal = <T, Y = void>(signalName: string, signalPayload: T): Promise<Y> => {
      return this.dispatcher.dispatchSignal<T, Y>(signalName, {
        payload: signalPayload,
        trace,
      } as any);
    };

    return {
      ...this,
      dispatchSignal,
    };
  }
}
