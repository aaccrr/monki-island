import { Tracing } from '@monki-island/observability';
import { RecieverDependencyContainer } from './';
import { DispatcherInterface } from '../Dispatcher';
import { SignalInterface } from '../Signal';

export interface RecieverConstructor {
  new (...dep: any[]): RecieverInterface;
  create(dependencyContainer: RecieverDependencyContainer): RecieverInterface;
}

export interface RecieverInterface {
  getSignal(): string[];
  setDispatcher(dispatcher: DispatcherInterface): void;
  setTracer(tracer: Tracing.TracerInterface): void;
  recieve(signalName: string, signalPayload: SignalInterface): Promise<any>;
}

export type RecieverAdapter = {
  [key: string]: <S = any, P = any>(signalPayload: any) => any;
};
