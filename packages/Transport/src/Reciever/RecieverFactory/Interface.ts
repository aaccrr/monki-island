import { Observer } from '@monki-island/observability';
import { TransporterInterface } from '../../Transporter';
import { DispatcherInterface } from '../../Dispatcher';

export interface RecieverDependencyContainer {
  getTransporter(): TransporterInterface;
  getDispatcher(): DispatcherInterface;
  getObserver(): Observer.Observer;
}
