import { RecieverConstructor, RecieverInterface } from '../RecieverInterface';

export interface RecieverFactoryInterface {
  addReciever(reciever: RecieverConstructor): void;
  create(): RecieverInterface[];
}
