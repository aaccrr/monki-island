import { RecieverFactoryInterface } from './RecieverFactoryInterface';
import { RecieverInterface, RecieverConstructor } from '../RecieverInterface';
import { RecieverDependencyContainer } from './Interface';

export class RecieverFactory implements RecieverFactoryInterface {
  constructor(private dependencyContainer: RecieverDependencyContainer) {}

  private reciever: RecieverConstructor[] = [];

  addReciever(reciever: RecieverConstructor): void {
    this.reciever.push(reciever);
  }

  create(): RecieverInterface[] {
    return this.reciever.map((reciever) => {
      const initializedReciever = reciever.create(this.dependencyContainer);
      initializedReciever.setDispatcher(this.dependencyContainer.getDispatcher());
      initializedReciever.setTracer(this.dependencyContainer.getObserver().tracer);
      return initializedReciever;
    });
  }
}
