export type SignalName =
  | 'organization_management.account.delete'
  | 'organization_management.account.fetch'
  | 'organization_management.account.login'
  | 'organization_management.account.password.change'
  | 'organization_management.account.password.set'
  | 'organization_management.account.password.reset.request'
  | 'organization_management.account.password.reset.requested'
  | 'organization_management.account.primary.confirm'
  | 'organization_management.account.primary.confirmed'
  | 'organization_management.account.primary.confirm.request'
  | 'organization_management.account.primary.confirm.requested'
  | 'organization_management.account.primary.downgrade'
  | 'organization_management.account.primary.downgraded'
  | 'organization_management.account.subordinate.create'
  | 'organization_management.account.subordinate.upgrade'
  | 'organization_management.account.subordinate.upgraded'
  | 'organization_management.account.subordinate.list_paginate'
  | 'organization_management.account.update'
  | 'organization_management.account.primary.create'
  | 'organization_management.account.primary.created'
  | 'organization_management.account.primary.updated'
  | 'organization_management.account.primary.deleted'
  | 'organization_management.account.booking.created'
  | 'organization_management.account.booking.updated'
  | 'organization_management.account.booking.deleted'
  | 'organization_management.account.management.created'
  | 'organization_management.account.management.updated'
  | 'organization_management.account.management.deleted'
  | 'organization_management.account_session.validate'
  | 'organization_management.account_session.refresh'
  | 'organization_management.account_session.update'
  | 'organization_management.account_session.single.ensure'
  | 'organization_management.organization.load'
  | 'organization_management.organization.loaded'
  | 'organization_management.organization.register'
  | 'organization_management.organization.registered'
  | 'organization_management.organization.sign'
  | 'organization_management.organization.signed'
  | 'organization_management.organization.updated'
  | 'organization_management.organization.delete'
  | 'organization_management.organization.deleted'
  | 'organization_management.organization.fetch'
  | 'organization_management.organization.list_paginate'
  | 'organization_management.museum.fetch'
  | 'organization_management.museum.list_paginate'
  | 'organization_management.museum.loaded.list_paginate'
  | 'organization_management.museum.created.list_paginate'
  | 'organization_management.museum.load'
  | 'organization_management.museum.loaded'
  | 'organization_management.museum.create'
  | 'organization_management.museum.created'
  | 'organization_management.museum.update'
  | 'organization_management.museum.updated'
  | 'organization_management.museum.delete'
  | 'organization_management.museum.deleted'
  | 'organization_management.museum.card.request'
  | 'organization_management.museum.card.requested'
  | 'organization_management.museum.category.list'
  | 'organization_management.museum.service.list'
  | 'organization_management.ticket.fetch'
  | 'organization_management.ticket.list_paginate'
  | 'organization_management.ticket.create'
  | 'organization_management.ticket.created'
  | 'organization_management.ticket.update'
  | 'organization_management.ticket.updated'
  | 'organization_management.ticket.delete'
  | 'organization_management.ticket.deleted'
  | 'organization_management.season_ticket.fetch'
  | 'organization_management.season_ticket.list_paginate'
  | 'organization_management.season_ticket.create'
  | 'organization_management.season_ticket.created'
  | 'organization_management.season_ticket.update'
  | 'organization_management.season_ticket.updated'
  | 'organization_management.season_ticket.delete'
  | 'organization_management.season_ticket.deleted'
  | 'organization_management.event.fetch'
  | 'organization_management.event.create'
  | 'organization_management.event.created'
  | 'organization_management.event.update'
  | 'organization_management.event.updated'
  | 'organization_management.event.delete'
  | 'organization_management.event.deleted'
  | 'organization_management.event.list_paginate'
  | 'organization_management.event.category.list'
  | 'organization_management.event.position.created'
  | 'organization_management.event.position.updated'
  | 'organization_management.event.position.deleted'
  | 'organization_management.age_censor.list'
  | 'organization_management.client_category.list'
  | 'service.upload.contragent.document.uploaded'
  | 'service.upload.museum.content.uploaded'
  | 'service.crypto.code.validate'
  | 'service.geography.ip.locate'
  | 'service.geography.location.fetch'
  | 'service.geography.location.catalog.fetch'
  | 'catalog.service_provider.fetch'
  | 'catalog.catalog_museum.fetch'
  | 'catalog.catalog_museum.list'
  | 'catalog.catalog_museum.list_paginate'
  | 'catalog.catalog_museum.like'
  | 'catalog.catalog_museum.liked'
  | 'catalog.catalog_museum.unlike'
  | 'catalog.catalog_museum.unliked'
  | 'catalog.catalog_ticket.fetch'
  | 'catalog.catalog_ticket.list_paginate'
  | 'catalog.catalog_season_ticket.fetch'
  | 'catalog.catalog_season_ticket.list_paginate'
  | 'catalog.catalog_event.fetch'
  | 'catalog.catalog_event.list'
  | 'catalog.catalog_event.list_paginate'
  | 'catalog.catalog_event.like'
  | 'catalog.catalog_event.liked'
  | 'catalog.catalog_event.unlike'
  | 'catalog.catalog_event.unliked'
  | 'catalog.ticket.sale.opened'
  | 'catalog.ticket.sale.closed'
  | 'catalog.ticket.visit_condition.canceled'
  | 'catalog.season_ticket.sale.opened'
  | 'catalog.season_ticket.sale.closed'
  | 'catalog.season_ticket.canceled'
  | 'catalog.event.added'
  | 'catalog.event.position.added'
  | 'catalog.event.position.sale.opened'
  | 'catalog.event.position.sale.closed'
  | 'catalog.event.position.canceled'
  | 'catalog.event.position.finished'
  | 'catalog.like.exist'
  | 'pro_culture.integration.validate'
  | 'pro_culture.integration.bind'
  | 'pro_culture.integration.accepted'
  | 'pro_culture.integration.find'
  | 'pro_culture.event.list.paginate'
  | 'pro_culture.event.exchange'
  | 'pro_culture.event.pushkin_card.participation.validate'
  | 'online_checkout.order.paid'
  | 'online_checkout.entrance_ticket.unpaid'
  | 'online_checkout.entrance_ticket.paid'
  | 'online_checkout.entrance_ticket.refunded'
  | 'online_checkout.entrance_ticket.free.issued'
  | 'control.entrance_ticket.visited'
  | 'control.entrance_ticket.used'
  | 'control.entrance_ticket.expired'
  | 'integration.ticket_registry.payment.registered'
  | 'integration.ticket_registry.refund.registered'
  | 'integration.ticket_registry.usage.registered';
