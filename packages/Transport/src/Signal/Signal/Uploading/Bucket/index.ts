import { Account } from './Account';
import { Contragent } from './Contragent';
import { Event } from './Event';
import { VisitObject } from './VisitObject';
import { Employee } from './Employee';

export const Bucket = {
  Account,
  Contragent,
  Event,
  VisitObject,
  Employee,
};
