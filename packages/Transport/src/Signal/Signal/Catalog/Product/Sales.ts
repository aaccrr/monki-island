export const Sales = {
  Open: 'catalog.product.sales.open',
  Close: 'catalog.product.sales.close',
  Opened: 'catalog.product.sales.opened',
  Closed: 'catalog.product.sales.closed',
};
