export const Showcase = {
  Published: 'catalog.showcase.published',
  Removed: 'catalog.showcase.removed',
  Viewed: 'catalog.showcase.viewed',
  View: 'catalog.showcase.fetch',
  List: 'catalog.showcase.list',
  Like: 'catalog.showcase.like',
  Dislike: 'catalog.showcase.dislike',
  Liked: 'catalog.showcase.liked',
  Disliked: 'catalog.showcase.unliked',
};
