export enum Control {
  EntranceTicketVisited = 'control.entrance_ticket.visited',
  EntranceTicketUsed = 'control.entrance_ticket.used',
  EntranceTicketExpired = 'control.entrance_ticket.expired',
}
