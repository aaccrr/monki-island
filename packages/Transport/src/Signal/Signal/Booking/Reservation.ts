export const Reservation = {
  Placed: 'booking.reservation.placed',
  Cancel: 'booking.reservation.cancel',
  Confirmed: 'booking.reservation.confirmed',
};
