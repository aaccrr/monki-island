export enum Museum {
  Created = 'organization_management.museum.created',
  Updated = 'organization_management.museum.updated',
  Deleted = 'organization_management.museum.deleted',
}
