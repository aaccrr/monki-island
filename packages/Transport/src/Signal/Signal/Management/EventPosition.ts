export enum EventPosition {
  Created = 'organization_management.event.position.created',
  Updated = 'organization_management.event.position.updated',
  Deleted = 'organization_management.event.position.deleted',
  Canceled = 'organization_management.event.position.canceled',
}
