export enum Organization {
  Updated = 'organization_management.organization.updated',
  Registered = 'organization_management.organization.registered',
  RequisitesUpdate = 'organization_management.organization.requisites.update',
}
