export enum SeasonTicket {
  Created = 'organization_management.season_ticket.created',
  Updated = 'organization_management.season_ticket.updated',
  Deleted = 'organization_management.season_ticket.deleted',
  Canceled = 'organization_management.season_ticket.canceled',
}
