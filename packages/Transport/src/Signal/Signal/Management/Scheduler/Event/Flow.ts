export const Flow = {
  Started: 'management.scheduler.event.flow.started',
  Completed: 'management.scheduler.event.flow.completed',
};
