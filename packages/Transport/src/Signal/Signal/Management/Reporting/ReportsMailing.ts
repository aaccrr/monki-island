export const ReportsMailing = {
  List: 'management.reporting.reports_mailing.list',
  Setup: 'management.reporting.reports_mailing.setup',
  Prepared: 'management.reporting.reports_mailing.prepared',
};
