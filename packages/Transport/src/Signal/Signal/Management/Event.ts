export enum Event {
  Created = 'organization_management.event.created',
  Updated = 'organization_management.event.updated',
  Deleted = 'organization_management.event.deleted',
  Canceled = 'organization_management.event.canceled',
}
