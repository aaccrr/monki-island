export enum ProCulture {
  PushkinCardProgramParticipationCheck = 'integration.pro_culture.pushkin_card.participation.check',
  PushkinCardProgramIncluded = 'integration.pro_culture.pushkin_card.included',
  PushkinCardProgramExcluded = 'integration.pro_culture.pushkin_card.excluded',
  EventExchanged = 'integration.pro_culture.event.exchanged',
  EventExchange = 'integration.pro_culture.event.exchange',
  OrganizationValidate = 'integration.pro_culture.organization.validate',
  OrganizationBind = 'integration.pro_culture.organization.bind',
  OrganizationAccepted = 'integration.pro_culture.organization.accepted',
  OrganizationFind = 'integration.pro_culture.organization.find',
  EventListPaginate = 'integration.pro_culture.event.list_paginate',
}
