export const Ticket = {
  Queued: 'integration.pushkin_card.synchronization.ticket.queued',
  Perform: 'integration.pushkin_card.synchronization.ticket.perform',
};
