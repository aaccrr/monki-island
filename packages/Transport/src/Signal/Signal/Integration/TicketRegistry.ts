export enum TicketRegistry {
  TicketRegister = 'integration.ticket_registry.ticket.register',
  RefundRegister = 'integration.ticket_registry.refund.register',
  UsageRegister = 'integration.ticket_registry.usage.register',
  TicketRegistered = 'integration.ticket_registry.ticket.registered',
  RefundRegistered = 'integration.ticket_registry.refund.registered',
  UsageRegistered = 'integration.ticket_registry.usage.registered',
  Check = 'integration.registry.check',
  PushkinTicketSynchronize = 'integration.registry.pushkin_ticket.synchronize',
}
