import * as Catalog from './Catalog';
import * as Integration from './Integration';
import * as Management from './Management';
import * as Reporting from './Reporting';
import * as Service from './Service';
import * as Office from './Office';
import { Control } from './Control';
import { OnlineCheckout } from './OnlineCheckout';
import * as Stock from './Stock';
import * as Booking from './Booking';
import * as Users from './Users';
import * as PaymentGateway from './PaymentGateway';
import * as Uploading from './Uploading';

export * as Accounting from './Accounting';
export * as Sales from './Sales';

export { Catalog, Integration, Management, Reporting, Control, OnlineCheckout, Service, Office, Stock, Booking, Users, PaymentGateway, Uploading };
