export enum OnlineCheckout {
  OrderPaid = 'online_checkout.order.paid',
  EntranceTicketUnpaid = 'online_checkout.entrance_ticket.unpaid',
  EntranceTicketPaid = 'online_checkout.entrance_ticket.paid',
  EntranceTicketRefunded = 'online_checkout.entrance_ticket.refunded',
  EntranceTicketFreeIssued = 'online_checkout.entrance_ticket.free.issued',
}
