import { Tracing } from '@monki-island/observability';

export interface SignalInterface<T = any> {
  payload: T;
  trace: Tracing.TraceCarrier;
}
